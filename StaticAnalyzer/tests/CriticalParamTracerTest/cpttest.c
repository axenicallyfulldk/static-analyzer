int func1(int *b){
	return b[0]+b[1]+b[5];
}

struct S1{
	int f1;
	int f2;
	int f3;
};

int func2(struct S1* s1){
	int res = 0;
	for(int i = s1->f1; i<s1->f2; i+=s1->f3){
		res += i;
	}
	return res;
}

struct S2{
	int f1;
	int f2;
};

struct S3{
	int f1;
	struct S2* f2;
};

int func3(struct S3* s3){
	int res = s3->f1;
	for(int i = 0; i < s3->f2->f2; ++i){
		res += s3->f2->f1;
	}
	return res;
}

int func4(int a){
	struct S2 s2;
	struct S3 s3;
	s3.f2 = &s2;
	s3.f1 = a;
	return func3(&s3);
}

struct S2 gS2;

int func5(int a){
	struct S3 s3;
	s3.f2 = &gS2;
	s3.f1 = a;
	return func3(&s3);
}

int func6(){
	int res = 0;
	for(int i = gS2.f1; i < gS2.f2; ++i){
		res += 3;
	}
	return res;
}

int func7(){
	gS2.f2 = 10;
	return func6();
}

int func8(int flag){
	int res = 0;
	if(flag > 10) res = 15;
	else res = 20;
	return res + gS2.f2;
}
