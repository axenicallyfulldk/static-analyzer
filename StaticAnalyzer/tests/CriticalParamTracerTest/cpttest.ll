; ModuleID = 'cpttest.c'
source_filename = "cpttest.c"
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-pc-linux-gnu"

%struct.S2 = type { i32, i32 }
%struct.S1 = type { i32, i32, i32 }
%struct.S3 = type { i32, %struct.S2* }

@gS2 = common global %struct.S2 zeroinitializer, align 4

; Function Attrs: norecurse nounwind readonly uwtable
define i32 @func1(i32* nocapture readonly) local_unnamed_addr #0 {
  %2 = load i32, i32* %0, align 4, !tbaa !2
  %3 = getelementptr inbounds i32, i32* %0, i64 1
  %4 = load i32, i32* %3, align 4, !tbaa !2
  %5 = add nsw i32 %4, %2
  %6 = getelementptr inbounds i32, i32* %0, i64 5
  %7 = load i32, i32* %6, align 4, !tbaa !2
  %8 = add nsw i32 %5, %7
  ret i32 %8
}

; Function Attrs: norecurse nounwind readonly uwtable
define i32 @func2(%struct.S1* nocapture readonly) local_unnamed_addr #0 {
  %2 = getelementptr inbounds %struct.S1, %struct.S1* %0, i64 0, i32 0
  %3 = load i32, i32* %2, align 4, !tbaa !6
  %4 = getelementptr inbounds %struct.S1, %struct.S1* %0, i64 0, i32 1
  %5 = load i32, i32* %4, align 4, !tbaa !8
  %6 = icmp slt i32 %3, %5
  br i1 %6, label %7, label %11

; <label>:7:                                      ; preds = %1
  %8 = getelementptr inbounds %struct.S1, %struct.S1* %0, i64 0, i32 2
  %9 = load i32, i32* %8, align 4, !tbaa !9
  %10 = load i32, i32* %4, align 4, !tbaa !8
  br label %13

; <label>:11:                                     ; preds = %13, %1
  %12 = phi i32 [ 0, %1 ], [ %16, %13 ]
  ret i32 %12

; <label>:13:                                     ; preds = %7, %13
  %14 = phi i32 [ %3, %7 ], [ %17, %13 ]
  %15 = phi i32 [ 0, %7 ], [ %16, %13 ]
  %16 = add nsw i32 %14, %15
  %17 = add nsw i32 %9, %14
  %18 = icmp slt i32 %17, %10
  br i1 %18, label %13, label %11
}

; Function Attrs: argmemonly nounwind
declare void @llvm.lifetime.start.p0i8(i64, i8* nocapture) #1

; Function Attrs: argmemonly nounwind
declare void @llvm.lifetime.end.p0i8(i64, i8* nocapture) #1

; Function Attrs: norecurse nounwind readonly uwtable
define i32 @func3(%struct.S3* nocapture readonly) local_unnamed_addr #0 {
  %2 = getelementptr inbounds %struct.S3, %struct.S3* %0, i64 0, i32 0
  %3 = load i32, i32* %2, align 8, !tbaa !10
  %4 = getelementptr inbounds %struct.S3, %struct.S3* %0, i64 0, i32 1
  %5 = load %struct.S2*, %struct.S2** %4, align 8, !tbaa !13
  %6 = getelementptr inbounds %struct.S2, %struct.S2* %5, i64 0, i32 1
  %7 = load i32, i32* %6, align 4, !tbaa !14
  %8 = icmp sgt i32 %7, 0
  br i1 %8, label %9, label %13

; <label>:9:                                      ; preds = %1
  %10 = load %struct.S2*, %struct.S2** %4, align 8, !tbaa !13
  %11 = getelementptr inbounds %struct.S2, %struct.S2* %10, i64 0, i32 1
  %12 = load i32, i32* %11, align 4, !tbaa !14
  br label %15

; <label>:13:                                     ; preds = %15, %1
  %14 = phi i32 [ %3, %1 ], [ %21, %15 ]
  ret i32 %14

; <label>:15:                                     ; preds = %9, %15
  %16 = phi %struct.S2* [ %5, %9 ], [ %10, %15 ]
  %17 = phi i32 [ 0, %9 ], [ %22, %15 ]
  %18 = phi i32 [ %3, %9 ], [ %21, %15 ]
  %19 = getelementptr inbounds %struct.S2, %struct.S2* %16, i64 0, i32 0
  %20 = load i32, i32* %19, align 4, !tbaa !16
  %21 = add nsw i32 %20, %18
  %22 = add nuw nsw i32 %17, 1
  %23 = icmp slt i32 %22, %12
  br i1 %23, label %15, label %13
}

; Function Attrs: nounwind readonly uwtable
define i32 @func4(i32) local_unnamed_addr #2 {
  %2 = alloca %struct.S2, align 4
  %3 = alloca %struct.S3, align 8
  %4 = bitcast %struct.S2* %2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* nonnull %4) #4
  %5 = bitcast %struct.S3* %3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* nonnull %5) #4
  %6 = getelementptr inbounds %struct.S3, %struct.S3* %3, i64 0, i32 1
  store %struct.S2* %2, %struct.S2** %6, align 8, !tbaa !13
  %7 = getelementptr inbounds %struct.S3, %struct.S3* %3, i64 0, i32 0
  store i32 %0, i32* %7, align 8, !tbaa !10
  %8 = call i32 @func3(%struct.S3* nonnull %3)
  call void @llvm.lifetime.end.p0i8(i64 16, i8* nonnull %5) #4
  call void @llvm.lifetime.end.p0i8(i64 8, i8* nonnull %4) #4
  ret i32 %8
}

; Function Attrs: nounwind readonly uwtable
define i32 @func5(i32) local_unnamed_addr #2 {
  %2 = alloca %struct.S3, align 8
  %3 = bitcast %struct.S3* %2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* nonnull %3) #4
  %4 = getelementptr inbounds %struct.S3, %struct.S3* %2, i64 0, i32 1
  store %struct.S2* @gS2, %struct.S2** %4, align 8, !tbaa !13
  %5 = getelementptr inbounds %struct.S3, %struct.S3* %2, i64 0, i32 0
  store i32 %0, i32* %5, align 8, !tbaa !10
  %6 = call i32 @func3(%struct.S3* nonnull %2)
  call void @llvm.lifetime.end.p0i8(i64 16, i8* nonnull %3) #4
  ret i32 %6
}

; Function Attrs: norecurse nounwind readonly uwtable
define i32 @func6() local_unnamed_addr #0 {
  %1 = load i32, i32* getelementptr inbounds (%struct.S2, %struct.S2* @gS2, i64 0, i32 0), align 4, !tbaa !16
  %2 = load i32, i32* getelementptr inbounds (%struct.S2, %struct.S2* @gS2, i64 0, i32 1), align 4, !tbaa !14
  %3 = icmp slt i32 %1, %2
  br i1 %3, label %4, label %11

; <label>:4:                                      ; preds = %0
  %5 = load i32, i32* getelementptr inbounds (%struct.S2, %struct.S2* @gS2, i64 0, i32 1), align 4, !tbaa !14
  %6 = add i32 %1, 1
  %7 = icmp sgt i32 %5, %6
  %8 = select i1 %7, i32 %5, i32 %6
  %9 = sub i32 %8, %1
  %10 = mul i32 %9, 3
  br label %11

; <label>:11:                                     ; preds = %4, %0
  %12 = phi i32 [ 0, %0 ], [ %10, %4 ]
  ret i32 %12
}

; Function Attrs: norecurse nounwind uwtable
define i32 @func7() local_unnamed_addr #3 {
  store i32 10, i32* getelementptr inbounds (%struct.S2, %struct.S2* @gS2, i64 0, i32 1), align 4, !tbaa !14
  %1 = tail call i32 @func6()
  ret i32 %1
}

; Function Attrs: norecurse nounwind readonly uwtable
define i32 @func8(i32) local_unnamed_addr #0 {
  %2 = icmp sgt i32 %0, 10
  %3 = select i1 %2, i32 15, i32 20
  %4 = load i32, i32* getelementptr inbounds (%struct.S2, %struct.S2* @gS2, i64 0, i32 1), align 4, !tbaa !14
  %5 = add nsw i32 %4, %3
  ret i32 %5
}

attributes #0 = { norecurse nounwind readonly uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind }
attributes #2 = { nounwind readonly uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { norecurse nounwind uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 6.0.0-1ubuntu2 (tags/RELEASE_600/final)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"int", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !3, i64 0}
!7 = !{!"S1", !3, i64 0, !3, i64 4, !3, i64 8}
!8 = !{!7, !3, i64 4}
!9 = !{!7, !3, i64 8}
!10 = !{!11, !3, i64 0}
!11 = !{!"S3", !3, i64 0, !12, i64 8}
!12 = !{!"any pointer", !4, i64 0}
!13 = !{!11, !12, i64 8}
!14 = !{!15, !3, i64 4}
!15 = !{!"S2", !3, i64 0, !3, i64 4}
!16 = !{!15, !3, i64 0}
