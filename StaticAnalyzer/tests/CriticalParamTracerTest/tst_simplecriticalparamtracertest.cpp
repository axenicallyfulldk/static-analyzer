#include <QtTest>
#include <iostream>
#include <sstream>
#include <llvm/IR/LLVMContext.h> //LLVMContext
#include <llvm/Support/SourceMgr.h> //SMDiagnostic
#include <llvm/IRReader/IRReader.h> //parseIRFile
#include <llvm/IR/Module.h> //Module
#include <llvm/Support/MemoryBuffer.h>
#include <llvm/Support/raw_ostream.h>

#include "criticalparamtracer.h"

#include "easylogging++.h"
INITIALIZE_EASYLOGGINGPP

using namespace llvm;

llvm::LLVMContext context;

char *toString(const ComplexMemLocation &cml){
    std::stringstream ss;
    ss<<cml;
    std::string res = ss.str();
    char * str = new char[res.size()];
    strcpy(str, res.data());
    return str;
}

char *toString(const std::set<ComplexMemLocation> &s1){
    std::stringstream ss;
    for(auto& ml : s1){
        ss<<ml<<",";
    }
    std::string res = ss.str();
    if(res.size()>2) res = res.substr(0, res.size()-1);
    char * str = new char[res.size()];
    strcpy(str, res.data());
    return str;
}

char *toString(const std::set<const Value*> &s1){
    std::stringstream ss;
    for(auto& val : s1){
        if(val != nullptr){
            std::string str;
            raw_string_ostream rso(str); val->print(rso);
            ss<<str<<",";
        }else{
            ss<<"nullptr,";
        }
    }
    std::string res = ss.str();
    if(res.size()>2) res = res.substr(0, res.size()-1);
    char * str = new char[res.size()];
    strcpy(str, res.data());
    return str;
}

static const llvm::Argument* getArgByIndex(const llvm::Function* func, unsigned argIndex){
    unsigned index = 0;
    for(auto it = func->arg_begin(); it != func->arg_end(); ++it){
        if(index == argIndex) return &*it;
        ++index;
    }
    return nullptr;
}

bool operator== (const std::set<const Value*>& s1, const std::set<const Value*>& s2){
    if(s1.size() != s2.size()) return false;

    for(auto v : s1){
        if(s2.find(v) == s2.end()) return false;
    }

    return true;
}

bool operator== (const std::set<ComplexMemLocation> &c1, const std::set<ComplexMemLocation> &c2)
{
    if(c1.size() != c2.size()) return false;

    for(auto& ml : c1){
        if(c2.find(ml) == c2.end()) return false;
    }

    return true;
}

class SimpleCriticalParamTracerTest : public QObject
{
    Q_OBJECT

public:
    SimpleCriticalParamTracerTest();
    ~SimpleCriticalParamTracerTest();

    std::unique_ptr<llvm::Module> readModule(QString modulePath);

private slots:
    void test_case1();

};

SimpleCriticalParamTracerTest::SimpleCriticalParamTracerTest()
{

}

SimpleCriticalParamTracerTest::~SimpleCriticalParamTracerTest()
{

}


std::unique_ptr<llvm::Module> SimpleCriticalParamTracerTest::readModule(QString modulePath){
    QFile file(modulePath);
    if(!file.open(QFile::ReadOnly | QFile::Text))
    {
        qDebug() << " Could not open the file for reading";
        return nullptr;
    }

    llvm::SMDiagnostic err;

    QByteArray moduleText = file.readAll();
    file.close();

    llvm::MemoryBufferRef mbr(moduleText.data(), "ir file");

    return llvm::parseIR(mbr, err, context, true);
}


void SimpleCriticalParamTracerTest::test_case1()
{
    auto module = readModule("://cpttest.ll");

    struct TestCase{
        const Function* func;
        std::set<const Value*> critVals;
        std::set<ComplexMemLocation> critMemLocs;
    };

    std::vector<TestCase> testCases;
    {
        auto func = module->getFunction("func1");
        std::set<ComplexMemLocation> critMemLocs;

        ComplexMemLocation cml;
        cml.base = const_cast<Argument*>(getArgByIndex(func, 0));
        cml.size = 4;
        cml.shifts = {LocationShift(0)};
        cml.shiftValues.push_back(nullptr);
        critMemLocs.insert(cml);

        cml.shifts = {LocationShift(4)};
        critMemLocs.insert(cml);

        cml.shifts = {LocationShift(20)};
        critMemLocs.insert(cml);
        testCases.push_back(TestCase{func, {}, critMemLocs});
    }

    {
        auto func = module->getFunction("func2");
        std::set<ComplexMemLocation> critMemLocs;

        ComplexMemLocation cml;
        cml.base = const_cast<Argument*>(getArgByIndex(func, 0));
        cml.size = 4;
        cml.shifts = {LocationShift(0)};
        cml.shiftValues.push_back(nullptr);
        critMemLocs.insert(cml);

        cml.shifts = {LocationShift(4)};
        critMemLocs.insert(cml);

        cml.shifts = {LocationShift(8)};
        critMemLocs.insert(cml);
        testCases.push_back(TestCase{func, {}, critMemLocs});
    }

    {
        auto func = module->getFunction("func3");
        std::set<ComplexMemLocation> critMemLocs;

        ComplexMemLocation cml;
        cml.base = const_cast<Argument*>(getArgByIndex(func, 0));
        cml.size = 4;
        cml.shifts = {LocationShift(0)};
        cml.shiftValues.push_back(nullptr);
        critMemLocs.insert(cml);

        cml.shifts = {LocationShift(8), LocationShift(0)};
        cml.shiftValues = {nullptr,nullptr};
        critMemLocs.insert(cml);

        cml.shifts = {LocationShift(8), LocationShift(4)};
        cml.shiftValues = {nullptr,nullptr};
        critMemLocs.insert(cml);
        testCases.push_back(TestCase{func, {}, critMemLocs});
    }

    {
        auto func = module->getFunction("func4");
        testCases.push_back(TestCase{func, {getArgByIndex(func, 0)}, {}});
    }

    {
        auto func = module->getFunction("func5");
        std::set<ComplexMemLocation> critMemLocs;

        ComplexMemLocation cml;
        cml.base = module->getGlobalVariable("gS2");
        cml.size = 4;
        cml.shifts = {LocationShift(0)};
        cml.shiftValues.push_back(nullptr);
        critMemLocs.insert(cml);

        cml.shifts = {LocationShift(4)};
        cml.shiftValues = {nullptr};
        critMemLocs.insert(cml);
        testCases.push_back(TestCase{func, {getArgByIndex(func, 0)}, critMemLocs});
    }

    {
        auto func = module->getFunction("func6");
        std::set<ComplexMemLocation> critMemLocs;

        ComplexMemLocation cml;
        cml.base = module->getGlobalVariable("gS2");
        cml.size = 4;
        cml.shifts = {LocationShift(0)};
        cml.shiftValues.push_back(nullptr);
        critMemLocs.insert(cml);

        cml.shifts = {LocationShift(4)};
        cml.shiftValues = {nullptr};
        critMemLocs.insert(cml);
        testCases.push_back(TestCase{func, {}, critMemLocs});
    }

    {
        auto func = module->getFunction("func7");
        std::set<ComplexMemLocation> critMemLocs;

        ComplexMemLocation cml;
        cml.base = module->getGlobalVariable("gS2");
        cml.size = 4;
        cml.shifts = {LocationShift(0)};
        cml.shiftValues.push_back(nullptr);
        critMemLocs.insert(cml);
        testCases.push_back(TestCase{func, {}, critMemLocs});
    }

    {
        auto func = module->getFunction("func8");
        std::set<ComplexMemLocation> critMemLocs;

        ComplexMemLocation cml;
        cml.base = module->getGlobalVariable("gS2");
        cml.size = 4;
        cml.shifts = {LocationShift(4)};
        cml.shiftValues.push_back(nullptr);
        critMemLocs.insert(cml);
        testCases.push_back(TestCase{func, {getArgByIndex(func, 0)}, critMemLocs});
    }

    for(auto& tc : testCases){
        std::set<ComplexMemLocation> critMemLocs;
        std::set<const Value*> critVals;
        CriticalParamTracer cpt;
        cpt.getFuncCritParams(tc.func, critVals, critMemLocs);

        QCOMPARE(critVals, tc.critVals);
        QCOMPARE(critMemLocs, tc.critMemLocs);
    }
}

QTEST_APPLESS_MAIN(SimpleCriticalParamTracerTest)

#include "tst_simplecriticalparamtracertest.moc"
