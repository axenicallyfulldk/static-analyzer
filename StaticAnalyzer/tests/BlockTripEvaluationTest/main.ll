; ModuleID = 'main.c'
source_filename = "main.c"
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-pc-linux-gnu"

; Function Attrs: norecurse nounwind readnone uwtable
define double @test1(i32, i32, i1 zeroext) local_unnamed_addr #1 {
  %4 = icmp sgt i32 %0, 0
  %5 = icmp ult i32 %1, 1001
  %6 = and i1 %4, %5
  %7 = or i1 %6, %2
  br i1 %7, label %8, label %15

; <label>:8:                                      ; preds = %3
  %9 = mul i32 %1, %1
  %10 = uitofp i32 %9 to double
  %11 = sitofp i32 %0 to double
  %12 = fadd double %11, %10
  %13 = uitofp i32 %1 to double
  %14 = fadd double %12, %13
  br label %19

; <label>:15:                                     ; preds = %3
  %16 = sitofp i32 %0 to double
  %17 = uitofp i32 %1 to double
  %18 = fsub double %16, %17
  br label %19

; <label>:19:                                     ; preds = %15, %8
  %20 = phi double [ %14, %8 ], [ %18, %15 ]
  ret double %20
}

; Function Attrs: norecurse nounwind readonly uwtable
define double @test2(i32, double* nocapture readonly) local_unnamed_addr #2 {
entry:
  br label %loop_head

loop_head:                                    ; preds = %2
  %i = phi i32 [ 0, %entry], [%i_p1, %loop_body]
  %sum = phi double [0.0, %entry], [%sum_pel, %loop_body]
  %loop_cond = icmp ult i32 %i, %0
  br i1 %loop_cond, label %loop_body, label %exit
 
loop_body:
  %ai_addr = getelementptr inbounds double, double* %1, i32 %i
  %ai = load double, double* %ai_addr, align 8
  %sum_pel = fadd double %sum, %ai
  %i_p1 = add nuw nsw i32 %i, 1
  br label %loop_head

exit:
  ret double %sum
}


; Function Attrs: norecurse nounwind readonly uwtable
define double @test3(i32, double* nocapture readonly) local_unnamed_addr #0 {
entry:
  br label %loop_body

loop_body:
  %sum = phi double [ 0.0, %entry], [%sum_pai, %loop_latch]
  %i = phi i32 [0, %entry], [%i_p1, %loop_latch]
  %ai_addr = getelementptr inbounds double, double* %1, i32 %i
  %ai = load double, double* %ai_addr, align 8
  %sum_pai = fadd double %sum, %ai
  br label %loop_latch

loop_latch:
  %i_p1 = add nuw nsw i32 %i, 1
  %cond = icmp ult i32 %i, %0
  br i1 %cond, label %loop_body, label %exit

exit:
  ret double %sum
}


; Function Attrs: norecurse nounwind readnone uwtable
define double @test4(i32, i32, i8 signext) local_unnamed_addr #3 {
entry:
  %bc = icmp slt i32 %0, 10
  br i1 %bc, label %cond_a, label %cond_b

cond_a:
  %temp1 = sitofp i8 %2 to double
  %res_a = fmul double %temp1, 33.0
  br label %exit

cond_b:
  %res_b = sitofp i32 %1 to double
  br label %exit

exit:
  %res = phi double [%res_a, %cond_a], [%res_b, %cond_b]
  ret double %res
}

; Function Attrs: norecurse nounwind readnone uwtable
define double @test5(i32, i1, double* nocapture readonly) local_unnamed_addr #0 {
entry:
  br label %loop_head

loop_head:
  %sum = phi double [ 0.0, %entry], [%sum_pai, %loop_latch]
  %i = phi i32 [0, %entry], [%i_p1, %loop_latch]
  %ai_addr = getelementptr inbounds double, double* %2, i32 %i
  %ai = load double, double* %ai_addr, align 8
  br i1 %1, label %loop_a, label %loop_b

loop_a:
  %ai_a = fmul double %ai, 2.0
  br label %loop_latch

loop_b:
  %ai_b = fmul double %ai, 3.0
  br label %loop_latch


loop_latch:
  %ai_res = phi double [%ai_a, %loop_a], [%ai_b, %loop_b]
  %sum_pai = fadd double %sum, %ai_res
  %i_p1 = add nuw nsw i32 %i, 1
  %cond = icmp ult i32 %i, %0
  br i1 %cond, label %loop_head, label %exit

exit:
  ret double %sum
}

; Function Attrs: norecurse nounwind readnone uwtable
define double @test6(i32, i1, double* nocapture readonly) local_unnamed_addr #0 {
entry:
  br label %loop_head

loop_head:
  %sum = phi double [ 0.0, %entry], [%sum_pai, %loop_latch]
  %i = phi i32 [0, %entry], [%i_p1, %loop_latch]
  %ai_addr = getelementptr inbounds double, double* %2, i32 %i
  %ai = load double, double* %ai_addr, align 8
  %loop_cond = icmp slt i32 %i, %0
  br i1 %loop_cond, label %loop_body, label %exit

loop_body:
  br i1 %1, label %loop_body_a, label %loop_body_b

loop_body_a:
  %ai_a = fmul double %ai, 2.0
  br label %loop_latch

loop_body_b:
  %ai_b = fmul double %ai, 3.0
  br label %loop_latch

loop_latch:
  %ai_res = phi double [%ai_a, %loop_body_a], [%ai_b, %loop_body_b]
  %sum_pai = fadd double %sum, %ai_res
  %i_p1 = add nuw nsw i32 %i, 1
  br label %loop_head

exit:
  ret double %sum
}

; Function Attrs: norecurse nounwind readnone uwtable
define double @test11(i32) local_unnamed_addr #0 {
  %2 = icmp sgt i32 %0, 0
  br i1 %2, label %3, label %24

; <label>:3:                                      ; preds = %1
  %4 = add i32 %0, -1
  %5 = and i32 %0, 7
  %6 = icmp ult i32 %4, 7
  br i1 %6, label %9, label %7

; <label>:7:                                      ; preds = %3
  %8 = sub i32 %0, %5
  br label %26

; <label>:9:                                      ; preds = %26, %3
  %10 = phi double [ undef, %3 ], [ %52, %26 ]
  %11 = phi i32 [ 0, %3 ], [ %53, %26 ]
  %12 = phi double [ 0.000000e+00, %3 ], [ %52, %26 ]
  %13 = icmp eq i32 %5, 0
  br i1 %13, label %24, label %14

; <label>:14:                                     ; preds = %9
  br label %15

; <label>:15:                                     ; preds = %15, %14
  %16 = phi i32 [ %21, %15 ], [ %11, %14 ]
  %17 = phi double [ %20, %15 ], [ %12, %14 ]
  %18 = phi i32 [ %22, %15 ], [ %5, %14 ]
  %19 = sitofp i32 %16 to double
  %20 = fadd double %17, %19
  %21 = add nuw nsw i32 %16, 1
  %22 = add i32 %18, -1
  %23 = icmp eq i32 %22, 0
  br i1 %23, label %24, label %15, !llvm.loop !2

; <label>:24:                                     ; preds = %9, %15, %1
  %25 = phi double [ 0.000000e+00, %1 ], [ %10, %9 ], [ %20, %15 ]
  ret double %25

; <label>:26:                                     ; preds = %26, %7
  %27 = phi i32 [ 0, %7 ], [ %53, %26 ]
  %28 = phi double [ 0.000000e+00, %7 ], [ %52, %26 ]
  %29 = phi i32 [ %8, %7 ], [ %54, %26 ]
  %30 = sitofp i32 %27 to double
  %31 = fadd double %28, %30
  %32 = or i32 %27, 1
  %33 = sitofp i32 %32 to double
  %34 = fadd double %31, %33
  %35 = or i32 %27, 2
  %36 = sitofp i32 %35 to double
  %37 = fadd double %34, %36
  %38 = or i32 %27, 3
  %39 = sitofp i32 %38 to double
  %40 = fadd double %37, %39
  %41 = or i32 %27, 4
  %42 = sitofp i32 %41 to double
  %43 = fadd double %40, %42
  %44 = or i32 %27, 5
  %45 = sitofp i32 %44 to double
  %46 = fadd double %43, %45
  %47 = or i32 %27, 6
  %48 = sitofp i32 %47 to double
  %49 = fadd double %46, %48
  %50 = or i32 %27, 7
  %51 = sitofp i32 %50 to double
  %52 = fadd double %49, %51
  %53 = add nuw nsw i32 %27, 8
  %54 = add i32 %29, -8
  %55 = icmp eq i32 %54, 0
  br i1 %55, label %9, label %26
}

attributes #0 = { norecurse nounwind readnone uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }

attributes #2 = { norecurse nounwind readonly uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }

attributes #3 = { norecurse nounwind readnone uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 6.0.0-1ubuntu2 (tags/RELEASE_600/final)"}
!2 = distinct !{!2, !3}
!3 = !{!"llvm.loop.unroll.disable"}
