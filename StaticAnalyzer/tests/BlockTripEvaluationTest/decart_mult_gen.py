sets = [ [-10, 0, 100], [100, 999, 1000, 2000], [0, 1], ["%3", "%8", "%15", "%19"]]

vals = [0]*len(sets)

def printVal(val):
	print(",".join([str(i) for i in val]))

indexes = [0]*len(sets)
depth = 0
while indexes[0] < len(sets[0]) or depth != 0:
	curIndex = indexes[depth]
	if curIndex < len(sets[depth]):
		vals[depth] = sets[depth][curIndex]
		indexes[depth] += 1
		if depth == len(sets) - 1:
			printVal(vals)
		else:
			depth += 1
	elif depth > 0:
		indexes[depth] = 0
		depth -= 1
			
			
