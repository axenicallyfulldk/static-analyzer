#include <QtTest>
#include <iostream>
#include <sstream>
#include <llvm/IR/LLVMContext.h> //LLVMContext
#include <llvm/Support/SourceMgr.h> //SMDiagnostic
#include <llvm/IRReader/IRReader.h> //parseIRFile
#include <llvm/IR/Module.h> //Module
#include <llvm/Support/MemoryBuffer.h>
#include <llvm/Support/raw_ostream.h>

#include "blocktripevaluater.h"

#include "memloctypes.h"

INITIALIZE_EASYLOGGINGPP

class BlockTripEvaluation : public QObject
{
    Q_OBJECT

public:
    BlockTripEvaluation();
    ~BlockTripEvaluation();

private:
    llvm::SMDiagnostic err;
    llvm::LLVMContext context;

    std::unique_ptr<llvm::Module> module;

    struct TestInfo{
        std::string params;
        int result;
        TestInfo(const std::string& p, int res):params(p), result(res){}
    };

    std::multimap<const llvm::BasicBlock*, TestInfo> tests;

    void readModule(QString modulePath);
    void readTests(QString testsPath);

    const llvm::BasicBlock* findBasicBlockById(const llvm::Function* f, const std::string& id);

private slots:
    void test_case1();

};

static std::string getSimpleNodeLabel(const llvm::BasicBlock *Node,
                                      const llvm::Function *) {
    if (!Node->getName().empty())
        return Node->getName().str();

    std::string Str;
    llvm::raw_string_ostream OS(Str);

    Node->printAsOperand(OS, false);
    return OS.str();
}

BlockTripEvaluation::BlockTripEvaluation()
{
    readModule("://main.ll");
    readTests("://triptests.txt");
}

BlockTripEvaluation::~BlockTripEvaluation()
{

}

void BlockTripEvaluation::readModule(QString modulePath){
    QFile file(modulePath);
    if(!file.open(QFile::ReadOnly | QFile::Text))
    {
        qDebug() << " Could not open the file for reading";
        return;
    }

    QByteArray moduleText = file.readAll();
    file.close();

    llvm::MemoryBufferRef mbr(moduleText.data(), "ir file");

    module = llvm::parseIR(mbr, err, context, true);
}

void BlockTripEvaluation::readTests(QString testsPath){
    QFile file(testsPath);
    if(!file.open(QFile::ReadOnly | QFile::Text))
    {
        qDebug() << " Could not open the file for reading";
        return;
    }

    QTextStream in(&file);
    while (!in.atEnd())
    {
        QString line = in.readLine();
        line = line.trimmed();
        if(line.size()==0 || line.at(0)=='#') continue;
        auto vals = line.split(QRegExp("\\s+"));
        const llvm::Function* func = module->getFunction(vals[0].toLocal8Bit().data());
        if(func == nullptr){
            std::cout<<"Failed to find function "<<vals[0].toLocal8Bit().data()<<std::endl;
            continue;
        }
        const llvm::BasicBlock* bb = findBasicBlockById(func, vals[1].toLocal8Bit().data());
        std::string params = vals[2].toLocal8Bit().data();
        int result = vals[3].toInt();
        tests.insert(std::make_pair(bb, TestInfo(params, result)));
    }
    file.close();
}

const llvm::BasicBlock *BlockTripEvaluation::findBasicBlockById(
        const llvm::Function *f, const std::string &id){

    for(auto& bb:*f){
        if(getSimpleNodeLabel(&bb, f) == id) return &bb;
    }
    return 0;
}

static std::string buildCommand(const std::string& params, std::shared_ptr<const AST::Node> equation){
    std::ostringstream oss;
    size_t prevPos = 0, pos = 0;
    unsigned paramIndex = 0;
    do{
        pos = params.find(',', prevPos);
        oss<<"p"<<paramIndex<<"="<<params.substr(prevPos, pos-prevPos)<<"; ";
        prevPos = pos + 1;
        paramIndex += 1;
    }while(pos != std::string::npos);
    oss<<"print(int("<<equation->to_string()<<"))";
    return oss.str();
}

std::string runCommandAndGetOutput(const std::string& prog, QStringList args){
    QProcess process;
    process.start(prog.data(), args, QIODevice::ReadWrite);
    process.waitForFinished();

    QByteArray data = process.readAllStandardOutput();

    return data.data();
}

void BlockTripEvaluation::test_case1()
{
    for(auto& func: *module){
        BlockTripEvaluater bte(&func);
        for(auto& bb:func){
            auto range = tests.equal_range(&bb);
            for(auto it = range.first; it != range.second; ++it){
                std::string command = buildCommand(it->second.params, bte.getBasicBlockTripCount(&bb));

                QStringList args;
                args<<"-c"<<command.data();

                std::string resStr = runCommandAndGetOutput("python3", args);
                int predictedRes = atoi(resStr.data());
                int realRes = it->second.result;
                if(predictedRes != realRes){
                    std::ostringstream message;
                    message << func.getName().data()<<" ";
                    message << getSimpleNodeLabel(&bb, &func) << " ";
                    message << it->second.params <<" ";
                    message << predictedRes << " " << realRes;
                    QFAIL(message.str().c_str());
                }
            }
        }
    }
}

QTEST_APPLESS_MAIN(BlockTripEvaluation)

#include "tst_blocktripevaluation.moc"
