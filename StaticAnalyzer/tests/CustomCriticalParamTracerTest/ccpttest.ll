; ModuleID = 'ccpttest.c'
source_filename = "ccpttest.c"
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-pc-linux-gnu"

%struct.S2 = type { i32, i32 }
%struct.S3 = type { i32, %struct.S2* }
%struct.S1 = type { i32, i32, i32 }

@gS2 = common global %struct.S2 zeroinitializer, align 4
@gS3 = common local_unnamed_addr global %struct.S3 zeroinitializer, align 8

; Function Attrs: norecurse nounwind readonly uwtable
define i32 @func1(double* nocapture readonly) local_unnamed_addr #0 {
  %2 = load double, double* %0, align 8, !tbaa !2
  %3 = getelementptr inbounds double, double* %0, i64 1
  %4 = load double, double* %3, align 8, !tbaa !2
  %5 = fadd double %2, %4
  %6 = getelementptr inbounds double, double* %0, i64 5
  %7 = load double, double* %6, align 8, !tbaa !2
  %8 = fadd double %5, %7
  %9 = fptosi double %8 to i32
  ret i32 %9
}

; Function Attrs: norecurse nounwind readonly uwtable
define double @func2(%struct.S1* nocapture readonly) local_unnamed_addr #0 {
  %2 = getelementptr inbounds %struct.S1, %struct.S1* %0, i64 0, i32 0
  %3 = load i32, i32* %2, align 4, !tbaa !6
  %4 = getelementptr inbounds %struct.S1, %struct.S1* %0, i64 0, i32 1
  %5 = load i32, i32* %4, align 4, !tbaa !9
  %6 = icmp slt i32 %3, %5
  br i1 %6, label %7, label %11

; <label>:7:                                      ; preds = %1
  %8 = getelementptr inbounds %struct.S1, %struct.S1* %0, i64 0, i32 2
  %9 = load i32, i32* %8, align 4, !tbaa !10
  %10 = load i32, i32* %4, align 4, !tbaa !9
  br label %13

; <label>:11:                                     ; preds = %13, %1
  %12 = phi double [ 0.000000e+00, %1 ], [ %17, %13 ]
  ret double %12

; <label>:13:                                     ; preds = %7, %13
  %14 = phi i32 [ %3, %7 ], [ %18, %13 ]
  %15 = phi double [ 0.000000e+00, %7 ], [ %17, %13 ]
  %16 = sitofp i32 %14 to double
  %17 = fadd double %15, %16
  %18 = add nsw i32 %9, %14
  %19 = icmp slt i32 %18, %10
  br i1 %19, label %13, label %11
}

; Function Attrs: argmemonly nounwind
declare void @llvm.lifetime.start.p0i8(i64, i8* nocapture) #1

; Function Attrs: argmemonly nounwind
declare void @llvm.lifetime.end.p0i8(i64, i8* nocapture) #1

; Function Attrs: norecurse nounwind readonly uwtable
define double @func3(%struct.S3* nocapture readonly) local_unnamed_addr #0 {
  %2 = getelementptr inbounds %struct.S3, %struct.S3* %0, i64 0, i32 0
  %3 = load i32, i32* %2, align 8, !tbaa !11
  %4 = sitofp i32 %3 to double
  %5 = getelementptr inbounds %struct.S3, %struct.S3* %0, i64 0, i32 1
  %6 = load %struct.S2*, %struct.S2** %5, align 8, !tbaa !14
  %7 = getelementptr inbounds %struct.S2, %struct.S2* %6, i64 0, i32 1
  %8 = load i32, i32* %7, align 4, !tbaa !15
  %9 = icmp sgt i32 %8, 0
  br i1 %9, label %10, label %14

; <label>:10:                                     ; preds = %1
  %11 = load %struct.S2*, %struct.S2** %5, align 8, !tbaa !14
  %12 = getelementptr inbounds %struct.S2, %struct.S2* %11, i64 0, i32 1
  %13 = load i32, i32* %12, align 4, !tbaa !15
  br label %16

; <label>:14:                                     ; preds = %16, %1
  %15 = phi double [ %4, %1 ], [ %23, %16 ]
  ret double %15

; <label>:16:                                     ; preds = %10, %16
  %17 = phi %struct.S2* [ %6, %10 ], [ %11, %16 ]
  %18 = phi i32 [ 0, %10 ], [ %24, %16 ]
  %19 = phi double [ %4, %10 ], [ %23, %16 ]
  %20 = getelementptr inbounds %struct.S2, %struct.S2* %17, i64 0, i32 0
  %21 = load i32, i32* %20, align 4, !tbaa !17
  %22 = sitofp i32 %21 to double
  %23 = fadd double %19, %22
  %24 = add nuw nsw i32 %18, 1
  %25 = icmp slt i32 %24, %13
  br i1 %25, label %16, label %14
}

; Function Attrs: nounwind readonly uwtable
define double @func4(i32) local_unnamed_addr #2 {
  %2 = alloca %struct.S2, align 4
  %3 = alloca %struct.S3, align 8
  %4 = bitcast %struct.S2* %2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* nonnull %4) #4
  %5 = bitcast %struct.S3* %3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* nonnull %5) #4
  %6 = getelementptr inbounds %struct.S3, %struct.S3* %3, i64 0, i32 1
  store %struct.S2* %2, %struct.S2** %6, align 8, !tbaa !14
  %7 = getelementptr inbounds %struct.S2, %struct.S2* %2, i64 0, i32 1
  store i32 %0, i32* %7, align 4, !tbaa !15
  %8 = call double @func3(%struct.S3* nonnull %3)
  call void @llvm.lifetime.end.p0i8(i64 16, i8* nonnull %5) #4
  call void @llvm.lifetime.end.p0i8(i64 8, i8* nonnull %4) #4
  ret double %8
}

; Function Attrs: nounwind readonly uwtable
define double @func5(i32) local_unnamed_addr #2 {
  %2 = alloca %struct.S3, align 8
  %3 = bitcast %struct.S3* %2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* nonnull %3) #4
  %4 = getelementptr inbounds %struct.S3, %struct.S3* %2, i64 0, i32 1
  store %struct.S2* @gS2, %struct.S2** %4, align 8, !tbaa !14
  %5 = getelementptr inbounds %struct.S3, %struct.S3* %2, i64 0, i32 0
  store i32 %0, i32* %5, align 8, !tbaa !11
  %6 = call double @func3(%struct.S3* nonnull %2)
  call void @llvm.lifetime.end.p0i8(i64 16, i8* nonnull %3) #4
  ret double %6
}

; Function Attrs: norecurse nounwind readonly uwtable
define double @func6() local_unnamed_addr #0 {
  %1 = load i32, i32* getelementptr inbounds (%struct.S2, %struct.S2* @gS2, i64 0, i32 0), align 4, !tbaa !17
  %2 = load i32, i32* getelementptr inbounds (%struct.S2, %struct.S2* @gS2, i64 0, i32 1), align 4, !tbaa !15
  %3 = icmp slt i32 %1, %2
  br i1 %3, label %4, label %6

; <label>:4:                                      ; preds = %0
  %5 = load i32, i32* getelementptr inbounds (%struct.S2, %struct.S2* @gS2, i64 0, i32 1), align 4, !tbaa !15
  br label %8

; <label>:6:                                      ; preds = %8, %0
  %7 = phi double [ 0.000000e+00, %0 ], [ %11, %8 ]
  ret double %7

; <label>:8:                                      ; preds = %4, %8
  %9 = phi i32 [ %1, %4 ], [ %12, %8 ]
  %10 = phi double [ 0.000000e+00, %4 ], [ %11, %8 ]
  %11 = fadd double %10, 3.000000e+00
  %12 = add nsw i32 %9, 1
  %13 = icmp slt i32 %12, %5
  br i1 %13, label %8, label %6
}

; Function Attrs: norecurse nounwind uwtable
define double @func7() local_unnamed_addr #3 {
  store i32 10, i32* getelementptr inbounds (%struct.S2, %struct.S2* @gS2, i64 0, i32 1), align 4, !tbaa !15
  %1 = tail call double @func6()
  ret double %1
}

; Function Attrs: norecurse nounwind readonly uwtable
define double @func8(i32) local_unnamed_addr #0 {
  %2 = icmp sgt i32 %0, 10
  %3 = select i1 %2, double 1.500000e+01, double 2.000000e+01
  %4 = load i32, i32* getelementptr inbounds (%struct.S2, %struct.S2* @gS2, i64 0, i32 0), align 4
  %5 = icmp sgt i32 %4, 0
  br label %10

; <label>:6:                                      ; preds = %14
  %7 = load i32, i32* getelementptr inbounds (%struct.S2, %struct.S2* @gS2, i64 0, i32 1), align 4, !tbaa !15
  %8 = sitofp i32 %7 to double
  %9 = fadd double %15, %8
  ret double %9

; <label>:10:                                     ; preds = %1, %14
  %11 = phi i32 [ 0, %1 ], [ %16, %14 ]
  %12 = phi double [ %3, %1 ], [ %15, %14 ]
  br i1 %5, label %13, label %14

; <label>:13:                                     ; preds = %10
  br label %18

; <label>:14:                                     ; preds = %18, %10
  %15 = phi double [ %12, %10 ], [ %22, %18 ]
  %16 = add nsw i32 %11, %0
  %17 = icmp slt i32 %16, 100
  br i1 %17, label %10, label %6

; <label>:18:                                     ; preds = %13, %18
  %19 = phi i32 [ %23, %18 ], [ 0, %13 ]
  %20 = phi double [ %22, %18 ], [ %12, %13 ]
  %21 = fmul double %20, 2.000000e+00
  %22 = fadd double %20, %21
  %23 = add nuw nsw i32 %19, 1
  %24 = icmp eq i32 %23, %4
  br i1 %24, label %14, label %18
}

; Function Attrs: norecurse nounwind readonly uwtable
define double @func9(%struct.S3* nocapture readnone) local_unnamed_addr #0 {
  %2 = load i32, i32* getelementptr inbounds (%struct.S3, %struct.S3* @gS3, i64 0, i32 0), align 8, !tbaa !11
  %3 = sitofp i32 %2 to double
  %4 = load %struct.S2*, %struct.S2** getelementptr inbounds (%struct.S3, %struct.S3* @gS3, i64 0, i32 1), align 8, !tbaa !14
  %5 = getelementptr inbounds %struct.S2, %struct.S2* %4, i64 0, i32 1
  %6 = load i32, i32* %5, align 4, !tbaa !15
  %7 = icmp sgt i32 %6, 0
  br i1 %7, label %8, label %12

; <label>:8:                                      ; preds = %1
  %9 = load %struct.S2*, %struct.S2** getelementptr inbounds (%struct.S3, %struct.S3* @gS3, i64 0, i32 1), align 8, !tbaa !14
  %10 = getelementptr inbounds %struct.S2, %struct.S2* %9, i64 0, i32 1
  %11 = load i32, i32* %10, align 4, !tbaa !15
  br label %14

; <label>:12:                                     ; preds = %14, %1
  %13 = phi double [ %3, %1 ], [ %21, %14 ]
  ret double %13

; <label>:14:                                     ; preds = %8, %14
  %15 = phi %struct.S2* [ %4, %8 ], [ %9, %14 ]
  %16 = phi i32 [ 0, %8 ], [ %22, %14 ]
  %17 = phi double [ %3, %8 ], [ %21, %14 ]
  %18 = getelementptr inbounds %struct.S2, %struct.S2* %15, i64 0, i32 0
  %19 = load i32, i32* %18, align 4, !tbaa !17
  %20 = sitofp i32 %19 to double
  %21 = fadd double %17, %20
  %22 = add nuw nsw i32 %16, 1
  %23 = icmp slt i32 %22, %11
  br i1 %23, label %14, label %12
}

attributes #0 = { norecurse nounwind readonly uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind }
attributes #2 = { nounwind readonly uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { norecurse nounwind uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 6.0.0-1ubuntu2 (tags/RELEASE_600/final)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"double", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
!6 = !{!7, !8, i64 0}
!7 = !{!"S1", !8, i64 0, !8, i64 4, !8, i64 8}
!8 = !{!"int", !4, i64 0}
!9 = !{!7, !8, i64 4}
!10 = !{!7, !8, i64 8}
!11 = !{!12, !8, i64 0}
!12 = !{!"S3", !8, i64 0, !13, i64 8}
!13 = !{!"any pointer", !4, i64 0}
!14 = !{!12, !13, i64 8}
!15 = !{!16, !8, i64 4}
!16 = !{!"S2", !8, i64 0, !8, i64 4}
!17 = !{!16, !8, i64 0}
