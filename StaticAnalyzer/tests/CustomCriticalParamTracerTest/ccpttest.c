int func1(double* arr){
	return arr[0]+arr[1]+arr[5];
}

struct S1{
	int f1;
	int f2;
	int f3;
};

double func2(struct S1* s1){
	double res = 0;
	for(int i = s1->f1; i<s1->f2; i+=s1->f3){
		res += i;
	}
	return res;
}

struct S2{
	int f1;
	int f2;
};

struct S3{
	int f1;
	struct S2* f2;
};

double func3(struct S3* s3){
	double res = s3->f1;
	for(int i = 0; i < s3->f2->f2; ++i){
		res += s3->f2->f1;
	}
	return res;
}

double func4(int a){
	struct S2 s2;
	struct S3 s3;
	s3.f2 = &s2;
	s3.f2->f2 = a;//s2.f2 = a;
	return func3(&s3);
}

struct S2 gS2;

double func5(int a){
	struct S3 s3;
	s3.f2 = &gS2;
	s3.f1 = a;
	return func3(&s3);
}

double func6(){
	double res = 0;
	for(int i = gS2.f1; i < gS2.f2; ++i){
		res += 3.0;
	}
	return res;
}

double func7(){
	gS2.f2 = 10;
	return func6();
}

double func8(int flag){
	double res = .0;
	if(flag > 10) res = 15.0;
	else res = 20.0;

	for(int i=0; i< 100; i+= flag){
		for(int j = 0; j < gS2.f1; ++j){
			res+= 2*res;
		}
	}
	return res + gS2.f2;
}

