
struct S1{
	int f1;
	double f2;
	float f3;
};

struct S2{
	int f1;
	struct S1* f2;
};

char* gCharArr;
double* gDoubleArr;

struct S1 *gS1;
struct S2 gS2;

void testFunc1(char* charArr, double* doubleArr, struct S1* s1, struct S2* s2){
	doubleArr[10] = s2->f2->f1 + charArr[12] + gCharArr[9] + gDoubleArr[8] + gS1->f1 + gS2.f1;
// test array memory locations transfered by arguments and global
// test memory locations of simple structure fields
// test memory locations for nested structures
// test memory locations for array of nested structures
}

