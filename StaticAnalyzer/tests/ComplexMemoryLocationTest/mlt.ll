; ModuleID = 'memloctest.c'
source_filename = "memloctest.c"
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-pc-linux-gnu"

%struct.S1 = type { i32, double, float }
%struct.S2 = type { i32, %struct.S1* }

@gCharArr = common local_unnamed_addr global i8* null, align 8
@gDoubleArr = common local_unnamed_addr global double* null, align 8
@gS1 = common local_unnamed_addr global %struct.S1* null, align 8
@gS2 = common local_unnamed_addr global %struct.S2 zeroinitializer, align 8

; Function Attrs: norecurse nounwind uwtable
define void @testFunc1(i8* nocapture readonly, double* nocapture, %struct.S1* nocapture readnone, %struct.S2* nocapture readonly) local_unnamed_addr #0 {
  %charEl3Ptr = getelementptr inbounds i8, i8* %0, i64 3
  %charEl3 = load i8, i8* %charEl3Ptr, align 1

  %doubleEl10Ptr = getelementptr inbounds double, double* %1, i64 10
  %doubleEl10 = load double, double* %doubleEl10Ptr, align 8

  %gCharArrPtr = load i8*, i8** @gCharArr, align 8, !tbaa !13
  %gCharEl9Ptr = getelementptr inbounds i8, i8* %gCharArrPtr, i64 9
  %gCharEl9 = load i8, i8* %gCharEl9Ptr, align 1

  %gDoubleArrPtr = load double*, double** @gDoubleArr, align 8
  %gDoubleEl4Ptr = getelementptr inbounds double, double* %gDoubleArrPtr, i64 4
  %gDoubleEl4 = load double, double* %gDoubleEl4Ptr, align 8


  %s1F1Ptr = getelementptr inbounds %struct.S1, %struct.S1* %2, i64 0, i32 0
  %s1F1 = load i32, i32* %s1F1Ptr, align 8 ; It will be aligned by 8 bytes!
  %s1F2Ptr = getelementptr inbounds %struct.S1, %struct.S1* %2, i64 0, i32 1
  %s1F2 = load double, double* %s1F2Ptr, align 8

  %gS1 = load %struct.S1*, %struct.S1** @gS1, align 8
  %gS1F1Ptr = getelementptr inbounds %struct.S1, %struct.S1* %gS1, i64 0, i32 0
  %gS1F1 = load i32, i32* %gS1F1Ptr, align 8 ; It will be aligned by 8 bytes!

  %s2F2Ptr = getelementptr inbounds %struct.S2, %struct.S2* %3, i64 0, i32 1
  %s2F2 = load %struct.S1*, %struct.S1** %s2F2Ptr, align 8
  %s2F2F1Ptr = getelementptr inbounds %struct.S1, %struct.S1* %s2F2, i64 0, i32 0
  %s2F2F1 = load i32, i32* %s2F2F1Ptr, align 8 ; It will be aligned by 8 bytes!

  %gS2F2Ptr = getelementptr inbounds %struct.S2, %struct.S2* @gS2, i64 0, i32 1
  %gS2F2 = load %struct.S1*, %struct.S1** %gS2F2Ptr, align 8
  %gS2F2F1Ptr = getelementptr inbounds %struct.S1, %struct.S1* %gS2F2, i64 0, i32 0
  %gS2F2F1 = load i32, i32* %gS2F2F1Ptr, align 8 ; It will be aligned by 8 bytes!

  %s1_7_F1Ptr = getelementptr inbounds %struct.S1, %struct.S1* %2, i64 7, i32 0
  %s1_7_F1 = load i32, i32* %s1_7_F1Ptr, align 8 ; It will be aligned by 8 bytes!

  ret void
}

attributes #0 = { norecurse nounwind uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 6.0.0-1ubuntu2 (tags/RELEASE_600/final)"}
!2 = !{!3, !7, i64 8}
!3 = !{!"S2", !4, i64 0, !7, i64 8}
!4 = !{!"int", !5, i64 0}
!5 = !{!"omnipotent char", !6, i64 0}
!6 = !{!"Simple C/C++ TBAA"}
!7 = !{!"any pointer", !5, i64 0}
!8 = !{!9, !4, i64 0}
!9 = !{!"S1", !4, i64 0, !10, i64 8, !11, i64 16}
!10 = !{!"double", !5, i64 0}
!11 = !{!"float", !5, i64 0}
!12 = !{!5, !5, i64 0}
!13 = !{!7, !7, i64 0}
!14 = !{!10, !10, i64 0}
!15 = !{!3, !4, i64 0}
