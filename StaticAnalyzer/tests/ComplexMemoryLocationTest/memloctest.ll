; ModuleID = 'memloctest.c'
source_filename = "memloctest.c"
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-pc-linux-gnu"

%struct.S1 = type { i32, double, float }
%struct.S2 = type { i32, %struct.S1* }

@gCharArr = common local_unnamed_addr global i8* null, align 8
@gDoubleArr = common local_unnamed_addr global double* null, align 8
@gS1 = common local_unnamed_addr global %struct.S1* null, align 8
@gS2 = common local_unnamed_addr global %struct.S2 zeroinitializer, align 8

; Function Attrs: norecurse nounwind uwtable
define void @testFunc1(i8* nocapture readonly, double* nocapture, %struct.S1* nocapture readnone, %struct.S2* nocapture readonly) local_unnamed_addr #0 {
  %5 = getelementptr inbounds %struct.S2, %struct.S2* %3, i64 0, i32 1
  %6 = load %struct.S1*, %struct.S1** %5, align 8, !tbaa !2
  %7 = getelementptr inbounds %struct.S1, %struct.S1* %6, i64 0, i32 0
  %8 = load i32, i32* %7, align 8, !tbaa !8
  %9 = getelementptr inbounds i8, i8* %0, i64 12
  %10 = load i8, i8* %9, align 1, !tbaa !12
  %11 = sext i8 %10 to i32
  %12 = add nsw i32 %8, %11
  %13 = load i8*, i8** @gCharArr, align 8, !tbaa !13
  %14 = getelementptr inbounds i8, i8* %13, i64 9
  %15 = load i8, i8* %14, align 1, !tbaa !12
  %16 = sext i8 %15 to i32
  %17 = add nsw i32 %12, %16
  %18 = sitofp i32 %17 to double
  %19 = load double*, double** @gDoubleArr, align 8, !tbaa !13
  %20 = getelementptr inbounds double, double* %19, i64 8
  %21 = load double, double* %20, align 8, !tbaa !14
  %22 = fadd double %21, %18
  %23 = load %struct.S1*, %struct.S1** @gS1, align 8, !tbaa !13
  %24 = getelementptr inbounds %struct.S1, %struct.S1* %23, i64 0, i32 0
  %25 = load i32, i32* %24, align 8, !tbaa !8
  %26 = sitofp i32 %25 to double
  %27 = fadd double %22, %26
  %28 = load i32, i32* getelementptr inbounds (%struct.S2, %struct.S2* @gS2, i64 0, i32 0), align 8, !tbaa !15
  %29 = sitofp i32 %28 to double
  %30 = fadd double %27, %29
  %31 = getelementptr inbounds double, double* %1, i64 10
  store double %30, double* %31, align 8, !tbaa !14
  ret void
}

attributes #0 = { norecurse nounwind uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 6.0.0-1ubuntu2 (tags/RELEASE_600/final)"}
!2 = !{!3, !7, i64 8}
!3 = !{!"S2", !4, i64 0, !7, i64 8}
!4 = !{!"int", !5, i64 0}
!5 = !{!"omnipotent char", !6, i64 0}
!6 = !{!"Simple C/C++ TBAA"}
!7 = !{!"any pointer", !5, i64 0}
!8 = !{!9, !4, i64 0}
!9 = !{!"S1", !4, i64 0, !10, i64 8, !11, i64 16}
!10 = !{!"double", !5, i64 0}
!11 = !{!"float", !5, i64 0}
!12 = !{!5, !5, i64 0}
!13 = !{!7, !7, i64 0}
!14 = !{!10, !10, i64 0}
!15 = !{!3, !4, i64 0}
