#include <QtTest>
#include <iostream>
#include <sstream>
#include <llvm/IR/LLVMContext.h> //LLVMContext
#include <llvm/Support/SourceMgr.h> //SMDiagnostic
#include <llvm/IRReader/IRReader.h> //parseIRFile
#include <llvm/IR/Module.h> //Module
#include <llvm/Support/MemoryBuffer.h>
#include <llvm/Support/raw_ostream.h>

#include "memloctypes.h"

#include "easylogging++.h"
INITIALIZE_EASYLOGGINGPP

using namespace llvm;

llvm::LLVMContext context;

char *toString(const ComplexMemLocation &cml){
    std::stringstream ss;
    ss<<cml;
    std::string res = ss.str();
    char * str = new char[10000];
    //static char str[10000];
    strcpy(str, res.data());
    return str;
}

class BasicComplexMemoryLocationTest : public QObject
{
    Q_OBJECT

public:
    BasicComplexMemoryLocationTest();
    ~BasicComplexMemoryLocationTest();

    static std::unique_ptr<llvm::Module> readModule(QString modulePath);

private slots:
    void test_case1();

};

BasicComplexMemoryLocationTest::BasicComplexMemoryLocationTest()
{

}

BasicComplexMemoryLocationTest::~BasicComplexMemoryLocationTest()
{

}

std::unique_ptr<llvm::Module> BasicComplexMemoryLocationTest::readModule(QString modulePath){
    QFile file(modulePath);
    if(!file.open(QFile::ReadOnly | QFile::Text))
    {
        qDebug() << " Could not open the file for reading";
        return nullptr;
    }

    llvm::SMDiagnostic err;

    QByteArray moduleText = file.readAll();
    file.close();

    llvm::MemoryBufferRef mbr(moduleText.data(), "ir file");

    return llvm::parseIR(mbr, err, context, true);
}

static const llvm::Instruction* getInstByName(const llvm::Function* func, std::string valName){
    for(auto& bb : *func){
        for(auto& inst : bb){
            if(std::string(inst.getName().data())==valName){
                return &inst;
            }
        }
    }
    return nullptr;
}

static const llvm::Argument* getArgByIndex(const llvm::Function* func, unsigned argIndex){
    unsigned index = 0;
    for(auto it = func->arg_begin(); it != func->arg_end(); ++it){
        if(index == argIndex) return &*it;
        ++index;
    }
    return nullptr;
}



void BasicComplexMemoryLocationTest::test_case1()
{
    auto module = readModule("://mlt.ll");
    auto func = module->getFunction("testFunc1");

    struct TestCase{
        std::string valName;
        ComplexMemLocation ml;
    };

    std::vector<TestCase> testCases;
    {// Access to third element of char array passed by argument
        ComplexMemLocation cml;
        cml.base = const_cast<Argument*>(getArgByIndex(func, 0));
        cml.size = 1;
        LocationShift ls;
        ls.offset = 3;
        cml.shifts.push_back(ls);
        cml.shiftValues.push_back(nullptr);
        testCases.push_back(TestCase{"charEl3", cml});
    }

    {// Access to tenth element of char array passed by argument
        ComplexMemLocation cml;
        cml.base = const_cast<Argument*>(getArgByIndex(func, 1));
        cml.size = 8;
        LocationShift ls;
        ls.offset = 80;
        cml.shifts.push_back(ls);
        cml.shiftValues.push_back(nullptr);
        testCases.push_back(TestCase{"doubleEl10", cml});
    }

    {// Access to ninth element of char array passed as global value
        ComplexMemLocation cml;
        cml.base = const_cast<GlobalVariable*>(module->getGlobalVariable("gCharArr"));
        cml.size = 1;
        LocationShift ls;
        ls.offset = 0;
        cml.shifts.push_back(ls);
        ls.offset = 9;
        cml.shifts.push_back(ls);

        cml.shiftValues.push_back(nullptr);
        cml.shiftValues.push_back(nullptr);
        testCases.push_back(TestCase{"gCharEl9", cml});
    }

    {// Access to fourth element of double array passed as global value
        ComplexMemLocation cml;
        cml.base = const_cast<GlobalVariable*>(module->getGlobalVariable("gDoubleArr"));
        cml.size = 8;
        LocationShift ls;
        ls.offset = 0;
        cml.shifts.push_back(ls);
        ls.offset = 32;
        cml.shifts.push_back(ls);

        cml.shiftValues.push_back(nullptr);
        cml.shiftValues.push_back(nullptr);
        testCases.push_back(TestCase{"gDoubleEl4", cml});
    }

    {// Access to the first field of structure passed by pointer as argument
        ComplexMemLocation cml;
        cml.base = const_cast<Argument*>(getArgByIndex(func, 2));
        cml.size = 4;
        LocationShift ls;
        ls.offset = 0;
        cml.shifts.push_back(ls);
        cml.shiftValues.push_back(nullptr);
        testCases.push_back(TestCase{"s1F1", cml});
    }

    {// Access to the second field of structure passed by pointer as argument
        ComplexMemLocation cml;
        cml.base = const_cast<Argument*>(getArgByIndex(func, 2));
        cml.size = 8;
        LocationShift ls;
        ls.offset = 8;
        cml.shifts.push_back(ls);
        cml.shiftValues.push_back(nullptr);
        testCases.push_back(TestCase{"s1F2", cml});
    }

    {// Access to the s2->f2->f1, where s2 is passed as argument
        ComplexMemLocation cml;
        cml.base = const_cast<Argument*>(getArgByIndex(func, 3));
        cml.size = 4;
        LocationShift ls;
        ls.offset = 8;
        cml.shifts.push_back(ls);
        ls.offset = 0;
        cml.shifts.push_back(ls);

        cml.shiftValues.push_back(nullptr);
        cml.shiftValues.push_back(nullptr);
        testCases.push_back(TestCase{"s2F2F1", cml});
    }

    {// Access to the gS2->f2->f1, where gS2 is passed as global pointer
        ComplexMemLocation cml;
        cml.base = const_cast<GlobalVariable*>(module->getGlobalVariable("gS2"));
        cml.size = 4;
        LocationShift ls;
        ls.offset = 8;
        cml.shifts.push_back(ls);
        ls.offset = 0;
        cml.shifts.push_back(ls);

        cml.shiftValues.push_back(nullptr);
        cml.shiftValues.push_back(nullptr);
        testCases.push_back(TestCase{"gS2F2F1", cml});
    }

    {// Access to the s1[7].f1, where s1 is passed by argument
        ComplexMemLocation cml;
        cml.base = const_cast<Argument*>(getArgByIndex(func, 2));
        cml.size = 4;
        LocationShift ls;
        ls.offset = 7*24+0;
        cml.shifts.push_back(ls);
        cml.shiftValues.push_back(nullptr);
        testCases.push_back(TestCase{"s1_7_F1", cml});
    }

    for(auto tc : testCases){
        auto cml = ComplexMemLocation::get(getInstByName(func, tc.valName));
        QCOMPARE(cml, tc.ml);
    }
}

QTEST_APPLESS_MAIN(BasicComplexMemoryLocationTest)

#include "tst_basiccomplexmemorylocationtest.moc"
