TEMPLATE = subdirs

SUBDIRS += \
    BlockTripEvaluationTest \
    ComplexMemoryLocationTest \
    CriticalParamTracerTest \
    CustomCriticalParamTracerTest
