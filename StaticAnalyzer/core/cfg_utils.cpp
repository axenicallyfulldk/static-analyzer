#include "cfg_utils.h"
#include <llvm/IR/Function.h>
#include <llvm/IR/CFG.h>

using namespace llvm;


template<>
void findPathDecisionValues<llvm::BasicBlock>(const BasicBlock* from, const BasicBlock* to, std::set<Value*>& cps){

    // Algorithm:
    // 1. Build control flow SUBgraph (rfcfg), that include all edges and nodes
    // reachable from "from" node. Entry node in this CFG is the "from".
    // 2. Based on built subgraph, build another subgraph (rtcfg) that include
    // nodes and edges from which we can reach "to" node.
    // 3. Find all "exiting" nodes. Exiting nodes that exit in rtcfg and in
    // rfcfg have more successors.
    // 4. Conditions of "exiting" nodes are decision values.

    using FTCC = FunctionToCfgConverter<true>;
    using CfgType = Cfg<const BasicBlock*>;

    FTCC::Mapping orig2cfg;
    CfgType cfg = FTCC().convert(from->getParent(), &orig2cfg);

    findPathDecisionValues(orig2cfg[from], orig2cfg[to], cps);
}
