#ifndef VALUETOEXPRESSIONCONVERTER_H
#define VALUETOEXPRESSIONCONVERTER_H

#include "syntax_tree.h"
#include "cfg.h"
#include "cfg_utils.h"
#include "utils.h"

#include <llvm/Analysis/ScalarEvolution.h>
#include <llvm/Analysis/ScalarEvolutionExpressions.h>
#include <llvm/Analysis/TargetLibraryInfo.h>
#include <llvm/Analysis/AssumptionCache.h>
#include <llvm/Analysis/PostDominators.h>
#include <llvm/IR/Dominators.h>

/// Class for converting Value and SCEV to expression
class ValueToExpressionConverter
{
public:
    ValueToExpressionConverter(const llvm::Function* func, llvm::ScalarEvolution& se);

    std::shared_ptr<AST::Node> valueToExpr(const llvm::Value* value) const;

    std::shared_ptr<AST::Node> scevToExpr(const llvm::SCEV* scev) const;

    std::shared_ptr<AST::Node> memLocToExpr(AttachedComplexMemLocation acml) const;

    // Returns expression that evaluated as true if control jumps from "from" to "to" block
    std::shared_ptr<AST::Node> getCondExpr(const llvm::BasicBlock* from, const llvm::BasicBlock* to)const;

private:

    std::shared_ptr<AST::Node> scevUnknownToExpr(const llvm::SCEVUnknown* scev) const;

    std::shared_ptr<AST::Node> getPhiNodeExpr(const llvm::PHINode *phiNode) const;

    std::shared_ptr<AST::Node> getPostDomCondExpr(const llvm::BasicBlock* from, const llvm::BasicBlock* to) const;

    std::shared_ptr<AST::Node> getCondExprBr(const llvm::BranchInst* br, const llvm::BasicBlock* to) const;

    const llvm::BasicBlock* getNearestNotPostDom(const llvm::BasicBlock* bb) const;

    using BlockType = Cfg<const llvm::BasicBlock*>::Block;

    std::shared_ptr<AST::Node> getPathDecisionExpr(const BlockType* from, const BlockType* to) const;

    std::shared_ptr<AST::Node> getPathDecisionExpr(const llvm::BasicBlock* from, const llvm::BasicBlock* to)const;

    const llvm::Function* _func;
    llvm::DominatorTree _dt;
    llvm::PostDominatorTree _pdt;
    llvm::ScalarEvolution& _se;
};

#endif // VALUETOEXPRESSIONCONVERTER_H
