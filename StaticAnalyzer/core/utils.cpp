#include "utils.h"
#include "criticalparamtracer.h"
#include "flowsensitiveaa.h"

#include "easylogging++.h"

#include <iostream>
#include <llvm/IR/Dominators.h>
#include <llvm/IR/Operator.h>
#include <llvm/IR/CallSite.h>
#include <llvm/Analysis/InstructionSimplify.h>
#include <llvm/IR/GetElementPtrTypeIterator.h>
#include <llvm/Analysis/ValueTracking.h>
#include <llvm/Analysis/AssumptionCache.h>
#include <llvm/Analysis/BasicAliasAnalysis.h>
#include <llvm/Analysis/LoopAnalysisManager.h>
#include <llvm/Analysis/CFLAndersAliasAnalysis.h>

using namespace llvm;
using namespace std;


static bool init(FunctionAnalysisManager& FAM){
    FAM.registerPass([&] { return TargetLibraryAnalysis(); });
    FAM.registerPass([&] { return AssumptionAnalysis(); });
    FAM.registerPass([&] { return DominatorTreeAnalysis(); });
    FAM.registerPass([&] { return LoopAnalysis(); });
    FAM.registerPass([&] { return BasicAA(); });
    FAM.registerPass([&] { return ScalarEvolutionAnalysis(); });
    FAM.registerPass([&] { return SCEVAA(); });
    FAM.registerPass([&] { return CFLAndersAA(); });
    FAM.registerPass([&] { return FlowSensitiveAAWrap(); });

    return true;
}

string getSimpleBasicBlockLabel(const BasicBlock *Node) {
    if (!Node->getName().empty())
        return Node->getName().str();

    std::string Str;
    llvm::raw_string_ostream OS(Str);

    Node->printAsOperand(OS, false);
    return OS.str();
}

bool isValidMemLock(const ComplexMemLocation& cml){
    return cml.shifts.size() == cml.shiftValues.size();
}



bool doesInstrModifiesLocation(const Instruction *instr, ComplexMemLocation ml){
    if(auto callInst = dyn_cast<CallInst>(instr)){
        #pragma message "implement the stub"
        // Handle specific cases like MPI_rank, MPI_size, MPI_Dims_create
    }

    if(auto storeInst = dyn_cast<StoreInst>(instr)){
        auto sml = ComplexMemLocation::get(storeInst);
        bool valid = isValidMemLock(ml);
        if(!valid){
            LOG(WARNING)<<"Memory location "<< ml <<" is not valid! ";
            return false;
        }
        valid = valid & isValidMemLock(sml);
        if(!valid){
            std::string instStr; raw_string_ostream instStrem(instStr); instr->print(instStrem);
            LOG(WARNING)<<"Memory location "<< sml <<" calculated from "<< instStr <<" is not valid! ";
            return false;
        }

        if(sml.shiftValues.back() != nullptr){
            auto smlMem = sml.toLLVMMemoryLocation();
            bool locIsNotModified = true;

            FunctionAnalysisManager FAM;
            init(FAM);

            auto func = instr->getFunction();
            auto baa = &FAM.getResult<BasicAA>(*const_cast<Function*>(func));
            auto seaa = &FAM.getResult<SCEVAA>(*const_cast<Function*>(func));

            for(size_t i = ml.shifts.size(); i > 0 ; --i){
                auto subMl = ml.getSubLocation(i);

                bool dontModify = false;
                bool doesModify = false;
                if(subMl.shiftValues.back() != nullptr){
                    auto subMlMem = subMl.toLLVMMemoryLocation();
                    doesModify = seaa->alias(smlMem, subMlMem) == MustAlias ||
                            baa->alias(smlMem, subMlMem) == MustAlias;
                    dontModify = seaa->alias(smlMem, subMlMem) == NoAlias ||
                            baa->alias(smlMem, subMlMem) == NoAlias;
                }
                if(doesModify){
                    return true;
                }
                locIsNotModified &= dontModify;
            }

            if(locIsNotModified){
                return false;
            }
        }

        // If alias analysises can't or didn't help. Try to compare memory location
        // solely based on shifts.
        for(size_t i = ml.shifts.size(); i > 0 ; --i){
            auto subMl = ml.getSubLocation(i);
            if(subMl == sml){
                return true;
            }
        }
    }

    return false;
}

bool doesStoreInstModifiesLocation(const StoreInst* storeInst, AttachedComplexMemLocation ml);

bool doesInstModifiesLocation(const Instruction* inst, AttachedComplexMemLocation acml){
    if(auto callInst = dyn_cast<CallInst>(inst)){
        #pragma message "implement the stub"
        // Handle specific cases like MPI_rank, MPI_size, MPI_Dims_create
    }

    if(auto storeInst = dyn_cast<StoreInst>(inst)){
        return doesStoreInstModifiesLocation(storeInst, acml);
    }

    return false;
}

bool doesStoreInstModifiesLocation(const StoreInst* storeInst, AttachedComplexMemLocation ml){
    auto func = storeInst->getFunction();

    if(ml.shiftValues.size() != ml.shifts.size()){
        LOG(WARNING)<<"Memory location is incorrect";
    }

    // At first let's try the simplest way to check modification. Just compare
    // pointers on equality.
    for(int i = ml.shiftValues.size() - 1; i >= 0 ; --i){
        if(ml.shiftValues[i] == storeInst->getPointerOperand()){
            return true;
        }
    }

    // Now let's try llvm's alias analysises to check modification.
    FunctionAnalysisManager FAM;
    init(FAM);

    auto storeMemLoc = llvm::MemoryLocation::get(storeInst);

    auto baa = &FAM.getResult<BasicAA>(*const_cast<Function*>(func));
    auto seaa = &FAM.getResult<SCEVAA>(*const_cast<Function*>(func));

    unsigned mustNotCount = 0;

    for(int i = ml.shiftValues.size() - 1; i >= 0 ; --i){
        if(ml.shiftValues[i] == nullptr) continue;

        auto subCml = ml.getSubLocation(i+1);
        auto llvmMl = subCml.toLLVMMemoryLocation();

        auto seaaRes = seaa->alias(storeMemLoc, llvmMl);

        if(seaaRes == AliasResult::MustAlias){
            return true;
        }
        if(seaaRes == AliasResult::NoAlias){
            mustNotCount++;
            continue;
        }

        auto baaRes = baa->alias(storeMemLoc, llvmMl);

        if(baaRes == AliasResult::MustAlias){
            return true;
        }
        if(baaRes == AliasResult::NoAlias){
            mustNotCount++;
            continue;
        }
    }

    if(mustNotCount == ml.shiftValues.size()){
        return false;
    }

    if(ml.inst == nullptr){
        return false;
    }

    // if llvm's analysis didn't help than
    auto fsaa = &FAM.getResult<FlowSensitiveAAWrap>(*const_cast<Function*>(func));

    auto storeCml = ComplexMemLocation::get(storeInst);
    auto storeAcml = AttachedComplexMemLocation(storeCml, storeInst, true);

    for(int i = ml.shifts.size() - 1; i >= 0 ; --i){
        auto subCml = ml.getSubLocation(i+1);
        if(fsaa->alias(subCml, storeAcml)){
            return true;
        }
    }

    return false;
}

static bool isBasePointer(const Value* val){
    if(dyn_cast<Argument>(val)) return true;
    if(dyn_cast<GlobalVariable>(val)) return true;
    if(dyn_cast<AllocaInst>(val)) return true;
    return false;
}

static ComplexMemLocation replaceComplexMemLocationBaseWithValue(ComplexMemLocation ml, int offset, const llvm::Value* val){
    if(isBasePointer(val)){
        ComplexMemLocation sia = ml;
        sia.shifts.clear();
        sia.shiftValues.clear();
        sia.derefValues.clear();
        sia.base = const_cast<Value*>(val);
        for(size_t j = offset + 1 ; j < ml.shifts.size(); ++j){
            sia.shifts.push_back(ml.shifts[j]);
            // Old shiftValues are not valid now
            sia.shiftValues.push_back(nullptr);
            sia.derefValues.push_back(nullptr);
        }
        return sia;
    }else{
        auto sia = ComplexMemLocation::get(val);
        if(sia != NullComplexMemLocation){
            for(int j = offset + 1 ; j < ml.shifts.size(); ++j){
                sia.shifts.push_back(ml.shifts[j]);
                // Old shiftValues are not valid now
                sia.shiftValues.push_back(nullptr);
                sia.derefValues.push_back(nullptr);
            }
            sia.size = ml.size;
            sia.AATags = ml.AATags;
            return sia;
        }
    }

    return NullComplexMemLocation;
}

template<bool isConst>
void _deduceStoreInstCritParamsRespectToMemLoc(const StoreInst *storeInst, AttachedComplexMemLocation ml, std::set<std::conditional_t<isConst, const Value*, Value *>> &critVals, std::set<ComplexMemLocation> &critMemLocs){
    using ValType = std::conditional_t<isConst, const Value*, Value *>;

    auto sml = ComplexMemLocation::get(storeInst);
    bool valid = isValidMemLock(ml);
    if(!valid){
        LOG(WARNING)<<"Memory location "<< ml <<" is not valid! ";
        return;
    }
    valid = valid & isValidMemLock(sml);
    if(!valid){
        std::string instStr; raw_string_ostream instStrem(instStr); storeInst->print(instStrem);
        LOG(WARNING)<<"Memory location "<< sml <<" calculated from "<< instStr <<" is not valid! ";
        return;
    }

    for(int i = ml.shiftValues.size() - 1; i >= 0 ; --i){
        if(ml.shiftValues[i] == storeInst->getPointerOperand()){
            if(i + 1 == ml.shifts.size()){
                critVals.insert(const_cast<ValType>(storeInst->getValueOperand()));
                return;
            }

            auto calcCml = replaceComplexMemLocationBaseWithValue(ml, i, storeInst->getValueOperand());
            if(calcCml != NullComplexMemLocation){
                critMemLocs.insert(calcCml);
            }
            return;
        }
    }

    FunctionAnalysisManager FAM;
    init(FAM);

    auto baa = &FAM.getResult<BasicAA>(*const_cast<Function*>(storeInst->getFunction()));
    auto seaa = &FAM.getResult<SCEVAA>(*const_cast<Function*>(storeInst->getFunction()));

    unsigned mustNotCount = 0;

    auto storeMemLoc = llvm::MemoryLocation(storeInst);

    for(int i = ml.shiftValues.size() - 1; i >= 0 ; --i){
        if(ml.shiftValues[i] == nullptr) continue;

        auto subCml = ml.getSubLocation(i+1);
        auto llvmMl = subCml.toLLVMMemoryLocation();

        auto seaaRes = seaa->alias(storeMemLoc, llvmMl);

        if(seaaRes == AliasResult::MustAlias){
            if(i + 1 == ml.shifts.size()){
                critVals.insert(const_cast<ValType>(storeInst->getValueOperand()));
                return;
            }

            auto calcCml = replaceComplexMemLocationBaseWithValue(ml, i, storeInst->getValueOperand());
            if(calcCml != NullComplexMemLocation){
                critMemLocs.insert(calcCml);
            }
            return;
        }
        if(seaaRes == AliasResult::NoAlias){
            mustNotCount++;
            continue;
        }

        auto baaRes = baa->alias(storeMemLoc, llvmMl);

        if(baaRes == AliasResult::MustAlias){
            if(i + 1 == ml.shifts.size()){
                critVals.insert(const_cast<ValType>(storeInst->getValueOperand()));
                return;
            }

            auto calcCml = replaceComplexMemLocationBaseWithValue(ml, i, storeInst->getValueOperand());
            if(calcCml != NullComplexMemLocation){
                critMemLocs.insert(calcCml);
            }
            return;
        }
        if(baaRes == AliasResult::NoAlias){
            mustNotCount++;
            continue;
        }
    }

    if(mustNotCount == ml.shiftValues.size()){
        return;
    }

    if(ml.inst == nullptr){
        return;
    }

    auto fsaa = &FAM.getResult<FlowSensitiveAAWrap>(*const_cast<Function*>(storeInst->getFunction()));

    auto storeCml = ComplexMemLocation::get(storeInst);
    auto storeAcml = AttachedComplexMemLocation(storeCml, storeInst, true);

    for(int i = ml.shifts.size() - 1; i >= 0 ; --i){
        auto subCml = ml.getSubLocation(i+1);
        if(fsaa->alias(subCml, storeAcml)){
            if(i + 1 == ml.shifts.size()){
                critVals.insert(const_cast<ValType>(storeInst->getValueOperand()));
                return;
            }

            auto calcCml = replaceComplexMemLocationBaseWithValue(ml, i, storeInst->getValueOperand());
            if(calcCml != NullComplexMemLocation){
                critMemLocs.insert(calcCml);
            }
            return;
        }
    }
}


template<bool isConst>
void _deduceInstrCritParamsRespectToMemLoc(const Instruction *instr, ComplexMemLocation ml, std::set<std::conditional_t<isConst, const Value*, Value *>> &critVals, std::set<ComplexMemLocation> &critMemLocs){
    using ValType = std::conditional_t<isConst, const Value*, Value *>;

    if(auto callInst = dyn_cast<CallInst>(instr)){
        #pragma message "implement the stub"
        // Handle specific cases like MPI_rank, MPI_size, MPI_Dims_create
        return;
    }

    if(auto storeInst = dyn_cast<StoreInst>(instr)){
        auto sml = ComplexMemLocation::get(storeInst);
        bool valid = isValidMemLock(ml);
        if(!valid){
            LOG(WARNING)<<"Memory location "<< ml <<" is not valid! ";
            return;
        }
        valid = valid & isValidMemLock(sml);
        if(!valid){
            std::string instStr; raw_string_ostream instStrem(instStr); instr->print(instStrem);
            LOG(WARNING)<<"Memory location "<< sml <<" calculated from "<< instStr <<" is not valid! ";
            return;
        }

        // Let's try to use alias analysis. If memory location where store function
        // saves data intersects with target memory location than try to
        if(sml.shiftValues.back() != nullptr){
            auto smlMem = sml.toLLVMMemoryLocation();

            FunctionAnalysisManager FAM;
            init(FAM);

            auto baa = &FAM.getResult<BasicAA>(*const_cast<Function*>(instr->getFunction()));
            auto seaa = &FAM.getResult<SCEVAA>(*const_cast<Function*>(instr->getFunction()));
            for(size_t i = ml.shifts.size(); i > 0 ; --i){
                auto subMl = ml.getSubLocation(i);

                bool doesModify = false;
                if(subMl.shiftValues.back() != nullptr){
                    auto subMlMem = subMl.toLLVMMemoryLocation();
                    #pragma message "I'dont know why, but sometime basicAA produces error"
                    doesModify = seaa->alias(smlMem, subMlMem) == MustAlias ||
                            baa->alias(smlMem, subMlMem) == MustAlias;
                }

                if(doesModify){
                    // If we write certain value to the memory location, we have
                    // to return this value
                    if(i == ml.shifts.size()){
                        critVals.insert(const_cast<ValType>(storeInst->getValueOperand()));
                        return;
                    }
                    // If we modify reference in dereference chain of target memory
                    // location, we have to concatenate memory location of value
                    // with part of target memory location
                    if(isBasePointer(storeInst->getValueOperand())){
                        ComplexMemLocation sia = ml;
                        sia.shifts.clear();
                        sia.shiftValues.clear();
                        sia.derefValues.clear();
                        sia.base = const_cast<Value*>(storeInst->getValueOperand());
                        for(size_t j = i ; j < ml.shifts.size(); ++j){
                            sia.shifts.push_back(ml.shifts[j]);
                            // Old shiftValues are not valid now
                            sia.shiftValues.push_back(nullptr);
                            sia.derefValues.push_back(nullptr);
                        }
                        critMemLocs.insert(sia);
                    }else{
                        auto sia = ComplexMemLocation::get(storeInst->getValueOperand());
                        if(sia != NullComplexMemLocation){
                            for(size_t j = i ; j < ml.shifts.size(); ++j){
                                sia.shifts.push_back(ml.shifts[j]);
                                // Old shiftValues are not valid now
                                sia.shiftValues.push_back(nullptr);
                                sia.derefValues.push_back(nullptr);
                            }
                            sia.size = ml.size;
                            sia.AATags = ml.AATags;
                            critMemLocs.insert(sia);
                            return;
                        }
                    }
                }
            }
        }

        // If alias analysises can't help. Try to compare memory location
        // solely based on shifts.
        for(size_t i = ml.shifts.size(); i > 0 ; --i){
            auto subMl = ml.getSubLocation(i);
            if(subMl == sml){
                if(i == ml.shifts.size()){
                    critVals.insert(const_cast<ValType>(storeInst->getValueOperand()));
                    return;
                }

                if(isBasePointer(storeInst->getValueOperand())){
                    ComplexMemLocation sia = ml;
                    sia.shifts.clear();
                    sia.shiftValues.clear();
                    sia.derefValues.clear();
                    sia.base = const_cast<Value*>(storeInst->getValueOperand());
                    for(size_t j = i ; j < ml.shifts.size(); ++j){
                        sia.shifts.push_back(ml.shifts[j]);
                        // Old shiftValues are not valid now
                        sia.shiftValues.push_back(nullptr);
                        sia.derefValues.push_back(nullptr);
                    }
                    critMemLocs.insert(sia);
                }else{
                    auto sia = ComplexMemLocation::get(storeInst->getValueOperand());
                    if(sia != NullComplexMemLocation){
                        for(size_t j = i ; j < ml.shifts.size(); ++j){
                            sia.shifts.push_back(ml.shifts[j]);
                            // Old shiftValues are not valid now
                            sia.shiftValues.push_back(nullptr);
                            sia.derefValues.push_back(nullptr);
                        }
                        sia.size = ml.size;
                        sia.AATags = ml.AATags;
                        critMemLocs.insert(sia);
                        return;
                    }
                }
            }
        }
        return;
    }

    std::string instStr; raw_string_ostream instStrem(instStr); instr->print(instStrem);
    LOG(WARNING)<<"Can't deduce instruction's "<<instStr<<" ciritcal parameter, because this instruction is unsupported!";
}

template<bool isConst>
void _deduceInstCritParamsRespectToMemLoc(const Instruction *instr, AttachedComplexMemLocation ml, std::set<std::conditional_t<isConst, const Value*, Value *>> &critVals, std::set<ComplexMemLocation> &critMemLocs){

    if(auto callInst = dyn_cast<CallInst>(instr)){
        #pragma message "implement the stub"
        // Handle specific cases like MPI_rank, MPI_size, MPI_Dims_create
        return;
    }

    if(auto storeInst = dyn_cast<StoreInst>(instr)){
        _deduceStoreInstCritParamsRespectToMemLoc<isConst>(storeInst, ml, critVals, critMemLocs);
        return;
    }

    std::string instStr; raw_string_ostream instStrem(instStr); instr->print(instStrem);
    LOG(WARNING)<<"Can't deduce instruction's "<<instStr<<" ciritcal parameter, because this instruction is unsupported!";
}


void deduceInstCritParamsRespectToMemLoc(const Instruction *instr, AttachedComplexMemLocation ml, std::set<Value *> &critVals, std::set<ComplexMemLocation> &critMemLocs){
    _deduceInstCritParamsRespectToMemLoc<false>(instr, ml, critVals, critMemLocs);
}

void deduceInstCritParamsRespectToMemLoc(const Instruction *instr, AttachedComplexMemLocation ml, std::set<const Value *> &critVals, std::set<ComplexMemLocation> &critMemLocs){
    _deduceInstCritParamsRespectToMemLoc<true>(instr, ml, critVals, critMemLocs);
}

void deduceInstrCritParamsRespectToMemLoc(const Instruction *instr, ComplexMemLocation ml, std::set<Value *> &critVals, std::set<ComplexMemLocation> &critMemLocs){
    _deduceInstrCritParamsRespectToMemLoc<false>(instr, ml, critVals, critMemLocs);
}

void deduceInstrCritParamsRespectToMemLoc(const Instruction *instr, ComplexMemLocation ml, std::set<const Value *> &critVals, std::set<ComplexMemLocation> &critMemLocs){
    _deduceInstrCritParamsRespectToMemLoc<true>(instr, ml, critVals, critMemLocs);
}

template<bool isConst>
void _translateCallCritVals(const CallInst *callInst, const set<std::conditional_t<isConst, const Value*, Value *>> &callCritVals, set<std::conditional_t<isConst, const Value*, Value *>> &critVals, set<ComplexMemLocation> &critLocs){
    for(auto val : callCritVals){
        if(auto arg = dyn_cast<Argument>(val)){
            critVals.insert(callInst->getOperand(arg->getArgNo()));
        }else if(dyn_cast<GlobalVariable>(val)){
            critVals.insert(val);
        }else{
            string valStr; raw_string_ostream valStream(valStr); val->print(valStream);
            string callStr; raw_string_ostream callStream(callStr); callInst->print(callStream);

            LOG(WARNING)<<"Can't translate critical value "<<valStr<<" of call "<<callStr;
        }
    }
}

void translateCallCritVals(const CallInst *callInst, const set<Value *> &callCritVals, set<Value *> &critVals, set<ComplexMemLocation> &critLocs){
    _translateCallCritVals<false>(callInst, callCritVals, critVals, critLocs);
}

void translateCallCritVals(const CallInst *callInst, const set<const Value *> &callCritVals, set<const Value *> &critVals, set<ComplexMemLocation> &critLocs){
    _translateCallCritVals<true>(callInst, callCritVals, critVals, critLocs);
}

template<bool isConst>
void _translateCallCritMemLocs(const CallInst *callInst, const set<ComplexMemLocation> &callCritMemLocs, set<std::conditional_t<isConst, const Value*, Value *>> &critVals, set<ComplexMemLocation> &critLocs){
    for(auto ml : callCritMemLocs){
        if(auto arg = dyn_cast<Argument>(ml.base)){
            auto nml = ml;
            std::fill(nml.shiftValues.begin(), nml.shiftValues.end(), nullptr);
            std::fill(nml.derefValues.begin(), nml.derefValues.end(), nullptr);
            nml.base = callInst->getOperand(arg->getArgNo());
            callInst->getAAMetadata(nml.AATags);
            critLocs.insert(nml);
        }else if(dyn_cast<GlobalVariable>(ml.base)){
            critLocs.insert(ml);
        }else{
            string callStr; raw_string_ostream callStream(callStr); callInst->print(callStream);
            LOG(WARNING)<<"Can't translate critical memory location "<<ml<<" of call "<<callStr;
        }
    }
}

void translateCallCritMemLocs(const CallInst *callInst, const set<ComplexMemLocation> &callCritMemLocs, set<Value *> &critVals, set<ComplexMemLocation> &critLocs){
    _translateCallCritMemLocs<false>(callInst, callCritMemLocs, critVals, critLocs);
}

void translateCallCritMemLocs(const CallInst *callInst, const std::set<ComplexMemLocation> &callCritMemLocs, std::set<const Value *> &critVals, std::set<ComplexMemLocation> &critLocs){
    _translateCallCritMemLocs<true>(callInst, callCritMemLocs, critVals, critLocs);
}

template <bool isConst>
void _deduceCallCritParams(const CallInst *callInst, set<std::conditional_t<isConst, const Value*, Value *>> &callCritVals, set<ComplexMemLocation> &callCritLocs){
    using ValType = std::conditional_t<isConst, const Value*, Value *>;

    auto targetFunc = callInst->getCalledFunction();
    if(targetFunc == nullptr || targetFunc->isDeclaration()){
        #pragma message "It is controversial solution"
        std::string callStr;
        raw_string_ostream rso(callStr); callInst->print(rso);
        LOG(WARNING)<<"Critical parameters of "<<callStr<<" have to be deduced, but this functionality is not implemented yet";
        // If we can't access target function, let's think that there is no
        // critical values
        return;
    }

    set<ValType> funcCritVals;
    set<ComplexMemLocation> funcCritMemLocs;

    CriticalParamTracer fcpt;
    fcpt.getFuncCritParams(targetFunc, funcCritVals, funcCritMemLocs);

    translateCallCritVals(callInst, funcCritVals, callCritVals, callCritLocs);
    translateCallCritMemLocs(callInst, funcCritMemLocs, callCritVals, callCritLocs);
}

void deduceCallCritParams(const CallInst *callInst, set<Value *> &callCritVals, set<ComplexMemLocation> &callCritLocs){
    _deduceCallCritParams<false>(callInst, callCritVals, callCritLocs);
}

void deduceCallCritParams(const CallInst *callInst, set<const Value *> &callCritVals, set<ComplexMemLocation> &callCritLocs){
    _deduceCallCritParams<true>(callInst, callCritVals, callCritLocs);
}

string valToString(const Value *val){
    std::string valStr;
    raw_string_ostream rso(valStr);
    val->print(rso);
    return valStr;
}
