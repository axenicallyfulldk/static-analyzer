#ifndef CUSTOMCRITICALPARAMTRACER_H
#define CUSTOMCRITICALPARAMTRACER_H

#include <set>
#include <list>
#include <functional>

#include <llvm/IR/Function.h>
#include <llvm/IR/Dominators.h>

#include "livevariabletracer.h"
#include "memloctypes.h"

#include "blocktripclassifier.h"

using ParameterSelector = std::function<void(const llvm::Function*, std::set<const llvm::Value*>&, std::set<AttachedComplexMemLocation>&, std::set<const llvm::BasicBlock*>&)>;

/// Returns critical parameters respect to selected values, memory locations and
/// basic blocks (for basic block parameters that influence on their trip count
/// is deduced.
class CustomCriticalParamTracer: public LiveVariableTracer
{
public:
    CustomCriticalParamTracer(ParameterSelector selector);

    void getFuncCritParams(const llvm::Function* func, std::set<llvm::Value*>& vals, std::set<ComplexMemLocation>& memLocs);
    void getFuncCritParams(const llvm::Function* func, std::set<const llvm::Value*>& vals, std::set<ComplexMemLocation>& memLocs);

protected:
    ParameterSelector _selector;

    struct FuncInfo{
        std::set<const llvm::Value*> critVals; // Critical values of function
        std::set<ComplexMemLocation> critMemLocs; // Critical memory locations of function
        std::list<const llvm::CallInst*> critCalls; // Calls
        std::map<const llvm::BasicBlock*, std::set<const llvm::Value*>> depVals;
        std::map<const llvm::BasicBlock*, std::set<ComplexMemLocation>> depMemLocs;
        llvm::DominatorTree dt;
        FuncInfo(const llvm::Function* func):dt(const_cast<llvm::Function&>(*func)){}
    };

    std::map<const llvm::Function*, FuncInfo> funcInfo;
    std::map<const llvm::CallInst*, unsigned> callsCache;

    // Calculates function's critical parameters
    void calcCritParams(const llvm::Function* func);

    // Fetch data about critical parameters at entry block and add it to
    // function's critical parameters.
    void updateFuncCritParams(const llvm::Function* func);

    // Calculate critical parameters without taking into accout calls' critical
    // parameters.
    void calculatePrioriCritParams(const llvm::Function* func);

    static bool isSignificantFunction(const llvm::Function* func, std::set<const llvm::Function*>& visitedFuncs, ParameterSelector selector);

    bool isSignificantFunction(const llvm::Function* func);

    void traceGroupCritParams(unsigned groupIndex, const BlockTripClassifier& btc);

    // Trace call's critical parameters to caller critical parameters.
    void traceCallCritValues(const llvm::CallInst* callInst);

    // Returns dominator tree for given function
    virtual llvm::DominatorTree* getDominatorTree(const llvm::Function* f);

    // Returns count of critical parameters of function.
    static unsigned getFuncGist(const FuncInfo& fi);
};

#endif // CUSTOMCRITICALPARAMTRACER_H
