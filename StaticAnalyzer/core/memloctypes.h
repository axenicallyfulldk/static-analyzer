#ifndef MEMLOCTYPES_H
#define MEMLOCTYPES_H

#include <llvm/IR/Instructions.h>
#include <llvm/Support/raw_ostream.h>
#include <llvm/Analysis/MemoryLocation.h>

#include "easylogging++.h"

struct LinearOffset{
    const llvm::Value* val;
    int64_t scale;

    bool operator== (const LinearOffset& lo) const{
        return val == lo.val && scale == lo.scale;
    }

    bool operator!= (const LinearOffset& lo) const{
        return !(*this == lo);
    }

    bool operator< (const LinearOffset& lo) const{
        if(val < lo.val) return true;
        else if(val == lo.val && scale < lo.scale) return true;
        return false;
    }
};

std::ostream& operator<<(std::ostream& os, const LinearOffset& obj);

/// Structure that present address shift as offset + linearOffset[0].val * linearOffset[0].scale +
/// + ... + linearOffset[n-1].val * linearOffset[n-1].scale
struct LocationShift{
    int64_t offset;
    std::vector<LinearOffset> linearOffset;

    LocationShift():offset(0){}
    LocationShift(int64_t Offset):offset(Offset){}

    bool operator==(const LocationShift& lc) const{
        if (offset != lc.offset ||
                linearOffset.size() != lc.linearOffset.size()) return false;

        for(size_t i = 0; i < linearOffset.size(); ++i){
            if(linearOffset[i].val != lc.linearOffset[i].val ||
                    linearOffset[i].scale != lc.linearOffset[i].scale)
                return false;
        }
        return true;
    }

    bool operator!=(const LocationShift& lc) const{
        return !(*this == lc);
    }

    bool operator<(const LocationShift& lc) const{
        if(offset < lc.offset) return true;
        else if(lc.offset < offset) return false;

        if(linearOffset.size() < lc.linearOffset.size()) return true;
        else if (lc.linearOffset.size() < linearOffset.size()) return false;

        for(size_t i = 0; i < linearOffset.size(); ++i){
            if(linearOffset[i] < lc.linearOffset[i]) return true;
            else if(lc.linearOffset[i] < linearOffset[i]) return false;
        }
        return false;
    }

    static LocationShift getZeroShift(){
        LocationShift ls;
        ls.offset = 0;
        return ls;
    }
};

std::ostream& operator<<(std::ostream& os, const LocationShift& obj);

/// Memory location presented as base pointer, shift from this pointer, and
/// occupied size
struct MemLocation{
public:
    // Base address.
    llvm::Value* base;
    // Shift relative to the base
    LocationShift shift;
    // Shift value = base + shift (optional field)
    llvm::Value* shiftValue;
    // Deref value = *(base + shift) (optional field)
    llvm::Value *derefValue;
    // Occupied size
    uint64_t size;

    llvm::AAMDNodes AATags;

    MemLocation();

    static MemLocation get(const llvm::LoadInst* li);

    static MemLocation get(const llvm::StoreInst* si);

    static MemLocation get(const llvm::GetElementPtrInst* gep);

    static MemLocation get(const llvm::AllocaInst* ai);

    static MemLocation get(const llvm::Argument* arg);

    static MemLocation get(const llvm::GlobalVariable* gv);

    static MemLocation get(const llvm::Value* val);

    llvm::Value* getAddressValue();

    void clearOptionalFields();

    // Compares two memory locations. Doesn't take into account shift values.
    bool operator==(const MemLocation& cml) const;

    bool operator!=(const MemLocation& cml) const;

    bool operator<(const MemLocation& cml) const;

    llvm::MemoryLocation toLLVMMemoryLocation()const;
};

extern const MemLocation NullMemLocation;

std::ostream& operator<<(std::ostream& os, const MemLocation& obj);

/// Memory location presented as sequence of pointer dereferecing operations
/// For example: *(*(*(base + shifts[0]) + shifts[1]) + shifts[2])
class ComplexMemLocation{
public:
    // Base address. We have to try to deduce such base that would be a func argument or
    // global pointer or alloca instruction. It has to have DataLayout.getPointerSize() size.
    llvm::Value* base;
    // Shift relative to the base address
    std::vector<LocationShift> shifts;
    // Addresses obtained at different stages of pointer dereferencing sequence.
    // It is optional field (can be null).
    // shiftValues[0] = base + shifts[0]
    // shiftValues[1] = *(base + shifts[0]) + shifts[1]
    // and etc.
    std::vector<llvm::Value*> shiftValues;
    // Result of dereference
    // derefValues[0] = *(base + shifts[0])
    // derefValues[1] = *(*(base + shifts[0]) + shifts[1])
    // and etc.
    std::vector<llvm::Value*> derefValues;
    // Size of accessed data. For int32 it would be 4.
    uint64_t size;

    llvm::AAMDNodes AATags;

    ComplexMemLocation();

    static ComplexMemLocation get(const llvm::LoadInst* li);

    static ComplexMemLocation get(const llvm::StoreInst* si);

    static ComplexMemLocation get(const llvm::GetElementPtrInst* gep);

    static ComplexMemLocation get(const llvm::AllocaInst* ai);

    static ComplexMemLocation get(const llvm::Argument* arg);

    static ComplexMemLocation get(const llvm::GlobalVariable* gv);

    static ComplexMemLocation get(const llvm::Value* val);

    ComplexMemLocation getSubLocation(int offsetCount = -1) const;

    llvm::Value* getAddressValue();

    void clearOptionalFields();

    // Compares two memory locations. Doesn't take into account shift values.
    bool operator==(const ComplexMemLocation& cml) const;

    bool operator!=(const ComplexMemLocation& cml) const;

    bool operator<(const ComplexMemLocation& cml) const;

//    MemLocation toMemLocation()const{
//        MemLocation ml;
//    }

    llvm::MemoryLocation toLLVMMemoryLocation()const;
};

extern const ComplexMemLocation NullComplexMemLocation;

std::ostream& operator<<(std::ostream& os, const ComplexMemLocation& obj);


/// It often needed to know value of memory location before or after execution
/// of certain instruction
struct AttachedComplexMemLocation : public ComplexMemLocation{
    const llvm::Instruction* inst;
    bool beforeInst;

    AttachedComplexMemLocation():ComplexMemLocation(){}

    AttachedComplexMemLocation(const ComplexMemLocation& cml, const llvm::Instruction* inst, bool beforeInst){
        *static_cast<ComplexMemLocation*>(this) = cml;
        this->inst = inst;
        this->beforeInst = beforeInst;
    }

    AttachedComplexMemLocation getSubLocation(int offsetCount = -1) const{
        auto cml = this->ComplexMemLocation::getSubLocation(offsetCount);
        return AttachedComplexMemLocation(cml, inst, beforeInst);
    }
};



#endif // MEMLOCTYPES_H
