#include "blocktripevaluater.h"

#include <llvm/IR/InstrTypes.h>

// loop is rotated if there is no exit from loop head
static bool isLoopRotated(const llvm::Loop *loop){
    for(const auto& succ:llvm::successors(loop->getHeader())){
        if(!loop->contains(succ)) return false;
    }
    return true;
}

std::shared_ptr<AST::Node> BlockTripEvaluater::getRotatedLoopHeadTripCount(const llvm::Loop *loop){
    auto bePlusOne = std::make_shared<AST::SumOp>();
    bePlusOne->addOperand(std::make_shared<AST::LiteralInt>(1));
    bePlusOne->addOperand(_vtec.scevToExpr(_se.getBackedgeTakenCount(loop)));
    return bePlusOne;
}

std::shared_ptr<AST::Node> BlockTripEvaluater::getLoopBodyTripCount(const llvm::Loop *loop){
    // If loop rotated, loop head trip count equals body trip count
    if(isLoopRotated(loop)){
        return getRotatedLoopHeadTripCount(loop);
    // if loop is not rotated, body trip count equals backedge taken count
    }else{
        return _vtec.scevToExpr(_se.getBackedgeTakenCount(loop));
    }
}

std::shared_ptr<AST::Node> BlockTripEvaluater::getBranchCondition(const llvm::BasicBlock *bb){
    unsigned groupIndex = _btc.getBasicBlockGroup(bb);
    const llvm::BasicBlock* head = _btc.getGroupHead(groupIndex);
    const llvm::BasicBlock* bottom = _btc.getGroupBottom(groupIndex);

    if(head == nullptr){
        return std::make_shared<AST::LiteralInt>(1);
    }

    // Find all head successors that lead to bb
    std::set<const llvm::BasicBlock*> headSucc(llvm::succ_begin(head), llvm::succ_end(head));
    std::set<const llvm::BasicBlock*> postDomHeadSucc;
    std::copy_if(headSucc.begin(), headSucc.end(), std::inserter(postDomHeadSucc, postDomHeadSucc.begin()),
                 [&bottom, this](const llvm::BasicBlock* bb){return _pdt.dominates(bottom, bb);});

    // All paths from head lead to bb
    if(headSucc.size() == postDomHeadSucc.size()){
        return std::make_shared<AST::LiteralBool>(true);
    }

    llvm::Loop* loop = _li.getLoopFor(head);
    // If head is loop header, check case when all paths that don't lead to bb,
    // lead to exit from loop
    if(loop != nullptr && head == loop->getHeader()){
        std::set<const llvm::BasicBlock*> exitSucc;
        std::copy_if(headSucc.begin(), headSucc.end(), std::inserter(exitSucc, exitSucc.begin()),
                     [&loop](const llvm::BasicBlock* bb){return !loop->contains(bb);});

        if(headSucc.size() - exitSucc.size() == postDomHeadSucc.size()){
            return std::make_shared<AST::LiteralBool>(true);
        }
    }

    std::shared_ptr<AST::Node> condExpr;
    if(postDomHeadSucc.size()>1){
        auto multipleCondExpr=std::make_shared<AST::And>();
        for(auto& bb:postDomHeadSucc){
            multipleCondExpr->addOperand(_vtec.getCondExpr(head,bb));
        }
        condExpr = multipleCondExpr;
    }else{
        condExpr = _vtec.getCondExpr(head,*postDomHeadSucc.begin());
    }

    return condExpr;
}

BlockTripEvaluater::BlockTripEvaluater(llvm::Function *func):_tli(_tlii),_ac(*func),_dt(*func),_li(_dt),
    _se(*func, _tli, _ac, _dt, _li), _btc(func), _vtec(func, _se)
{
    _pdt.recalculate(*func);
}

std::shared_ptr<AST::Node> BlockTripEvaluater::getBasicBlockTripCount(const llvm::BasicBlock *bb, bool accountForLoop){
    if(bb == nullptr) return std::make_shared<AST::LiteralInt>(1);

    std::shared_ptr<AST::Node> res;

    unsigned groupIndex = _btc.getBasicBlockGroup(bb);
    const llvm::BasicBlock* top = _btc.getGroupTop(groupIndex);

    std::shared_ptr<AST::Node> loopExpr;

    llvm::Loop* loop = _li.getLoopFor(top);
    if(loop != nullptr && loop->getHeader() == top && accountForLoop){
        loopExpr = getRotatedLoopHeadTripCount(loop);
    }

    std::shared_ptr<AST::Node> condExpr = getBranchCondition(bb);
    std::shared_ptr<AST::Node> headTripCount;

    const llvm::BasicBlock* head = _btc.getGroupHead(groupIndex);

    if(head != nullptr){
        loop = _li.getLoopFor(head);
        //Case when bb is in the loop body
        if(loop != nullptr && head == loop->getHeader()){
            headTripCount = getLoopBodyTripCount(loop);
            // We have already taken loop trip count into account, so we will ignore it
            headTripCount = AST::createMulOp(getBasicBlockTripCount(head,false), headTripCount);

        }else{
            headTripCount = getBasicBlockTripCount(head);
        }
    }

    std::shared_ptr<AST::Node> headCondTripCount = AST::createMulOp(condExpr, headTripCount);

    return AST::createMulOp(headCondTripCount, loopExpr);
}
