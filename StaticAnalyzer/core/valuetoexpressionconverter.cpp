#include "valuetoexpressionconverter.h"
#include "easylogging++.h"

#include "memloctypes.h"

using namespace llvm;


static std::shared_ptr<AST::BooleanNode> createByPredicate(CmpInst::Predicate predicate){

    switch(predicate){
    case CmpInst::ICMP_EQ:
        return std::make_shared<AST::EqualTo>();
    case CmpInst::ICMP_NE:
        return std::make_shared<AST::NotEqualTo>();
    case CmpInst::ICMP_SGE:
    case CmpInst::ICMP_UGE:
        return std::make_shared<AST::GreaterOrEqual>();
    case CmpInst::ICMP_SGT:
    case CmpInst::ICMP_UGT:
        return std::make_shared<AST::GreaterThen>();
    case CmpInst::ICMP_SLE:
    case CmpInst::ICMP_ULE:
        return std::make_shared<AST::LessOrEqual>();
    case CmpInst::ICMP_SLT:
    case CmpInst::ICMP_ULT:
        return std::make_shared<AST::LessThen>();
    }

}

static std::shared_ptr<AST::Node> createSimilarToScev(SCEVTypes type){
    using namespace  llvm;
    switch(type){
    case scAddExpr:
        return std::make_shared<AST::SumOp>();
    case scMulExpr:
        return std::make_shared<AST::MulOp>();
    case scSMaxExpr:
    case scUMaxExpr:
        return std::make_shared<AST::MaxFunc>();
    }
}

ValueToExpressionConverter::ValueToExpressionConverter(const Function *func, ScalarEvolution &se):_se(se),_func(func),
    _dt(const_cast<Function&>(*func)){
    _pdt.recalculate(const_cast<Function&>(*func));
}

std::shared_ptr<AST::Node> ValueToExpressionConverter::valueToExpr(const Value *value) const{
    return scevToExpr(_se.getSCEV(const_cast<Value*>(value)));
}

std::shared_ptr<AST::Node> ValueToExpressionConverter::scevToExpr(const SCEV *scev) const{
    using namespace llvm;
    switch (scev->getSCEVType()) {
    case scConstant:{
        const auto* constInt = dyn_cast<const SCEVConstant>(scev)->getValue();
        assert(constInt != nullptr);
        return std::make_shared<AST::LiteralInt>(constInt);
    }
    case scTruncate:{
        auto truncExpr = dyn_cast<const SCEVTruncateExpr>(scev);
        int res = (1<<(truncExpr->getType()->getScalarSizeInBits())) - 1;
        auto binaryAnd = std::make_shared<AST::And>();
        binaryAnd->addOperand(scevToExpr(truncExpr->getOperand()));
        binaryAnd->addOperand(std::make_shared<AST::LiteralInt>(res));
        return binaryAnd;
    }
    case scZeroExtend:
    case scSignExtend:
        return scevToExpr(dyn_cast<const SCEVCastExpr>(scev)->getOperand());
    case scAddExpr:
    case scMulExpr:
    case scSMaxExpr:
    case scUMaxExpr:
    {
        auto naryScev = dyn_cast<const SCEVNAryExpr>(scev);
        auto expr = std::dynamic_pointer_cast<AST::ExprNode>(createSimilarToScev(static_cast<SCEVTypes>(scev->getSCEVType())));
        for(auto operand: naryScev->operands()){
            expr->addOperand(scevToExpr(operand));
        }
        return expr;
    }
    case scUDivExpr:
    {
        auto udivScev = dyn_cast<const SCEVUDivExpr>(scev);
        auto divExpr = std::make_shared<AST::DivOp>();
        divExpr->addOperand(scevToExpr(udivScev->getLHS()));
        divExpr->addOperand(scevToExpr(udivScev->getRHS()));
        return divExpr;
    }

    case scUnknown:
    {
//        scev->print(outs());
//        outs()<<"\n";
//        outs().flush();
        return scevUnknownToExpr(dyn_cast<const SCEVUnknown>(scev));
    }

    }

    return AST::FailNode;
}

static std::set<const Instruction*> getPrevInsts(const Instruction* inst){
    if(inst->getPrevNode()){
        return {inst->getPrevNode()};
    }

    std::set<const Instruction*> prevInsts;
    for(auto predBb : predecessors(inst->getParent())){
        prevInsts.insert(predBb->getTerminator());
    }
    return prevInsts;
}

static void traceMemLocToNearestModInst(const AttachedComplexMemLocation &acml, const Instruction *inst, std::set<const Instruction *> &nearestInst, std::set<const BasicBlock *> &visitedBbs){
    const BasicBlock* curBb = inst->getParent();
    do{
        if(doesInstModifiesLocation(inst, acml)){
            nearestInst.insert(inst);
            visitedBbs.insert(curBb);
            return;
        }
        inst = inst->getPrevNode();
    }while(inst);

    visitedBbs.insert(curBb);

    for(auto predBb : predecessors(curBb)){
        if(visitedBbs.find(predBb) == visitedBbs.end()){
            traceMemLocToNearestModInst(acml, predBb->getTerminator(), nearestInst, visitedBbs);
        }
    }
}

std::shared_ptr<AST::Node> ValueToExpressionConverter::memLocToExpr(AttachedComplexMemLocation acml) const{
    std::set<const Instruction*> nearestInsts;
    std::set<const BasicBlock*> visitedBbs;

    if(acml.beforeInst == false){
        traceMemLocToNearestModInst(acml, acml.inst, nearestInsts, visitedBbs);
    }else{
        std::set<const Instruction*> prevInsts = getPrevInsts(acml.inst);
        for(auto prevInst : prevInsts){
            traceMemLocToNearestModInst(acml, prevInst, nearestInsts, visitedBbs);
        }
    }

    if(nearestInsts.size() == 0){
        if(dyn_cast<Argument>(acml.base)){
            return std::shared_ptr<AST::ArgumentMemLocNode>(new AST::ArgumentMemLocNode(acml));
        }else if(dyn_cast<GlobalVariable>(acml.base)){
            return std::shared_ptr<AST::Node>(new AST::GlobalVarMemLocNode(acml));
        }else{
            LOG(WARNING)<<"Failed to calculate value of memory location "<<acml<< " because base is not argumnet and not a global variable";
            return AST::FailNode;
        }
    }

    const BasicBlock* headBb = acml.inst->getParent();

    for(auto nearestInst : nearestInsts){
        if(_dt.dominates(headBb, nearestInst->getParent())){
            LOG(WARNING)<<"Failed to calculate value of memory location "<<acml<< " because one of the nearest instructions is dominate by head bb";
            return AST::FailNode;
        }
    }

    std::shared_ptr<AST::Node> resExpr(new AST::LiteralInt(int64_t(0)));
    for(auto nearestInst : nearestInsts){
        auto condExpr = getPathDecisionExpr(nearestInst->getParent(), headBb);
        std::shared_ptr<AST::Node> valExpr;

        auto storeInst = dyn_cast<StoreInst>(nearestInst);
        if(storeInst == nullptr){
            LOG(WARNING)<<"Failed to calculate value of memory location "<<acml<<" because it depend's on instruction "<<valToString(nearestInst);
            valExpr = AST::FailNode;
        } else {
            std::set<const Value*> critVals;
            std::set<ComplexMemLocation> critMemLocs;
            deduceInstCritParamsRespectToMemLoc(nearestInst, acml, critVals, critMemLocs);

            if(critVals.size() + critMemLocs.size() != 1){
                LOG(WARNING)<<"Can't deduce value of memory location "<<acml<<" modified by "<<valToString(nearestInst);
                valExpr = AST::FailNode;
            }else{

                if(critVals.size()) valExpr = valueToExpr(*critVals.begin());
                else{
                    AttachedComplexMemLocation tacml(*critMemLocs.begin(), nearestInst, true);
                    valExpr = memLocToExpr(tacml);
                }
            }

        }

        resExpr = AST::createSumOp(resExpr, AST::createMulOp(condExpr, valExpr));
    }

    return resExpr;
}

std::shared_ptr<AST::Node> ValueToExpressionConverter::scevUnknownToExpr(const SCEVUnknown *scev) const{

    if(auto arg = dyn_cast<const Argument>(scev->getValue())){
        return std::make_shared<AST::ArgumentNode>(arg);
    }

    if(auto phi = dyn_cast<const PHINode>(scev->getValue())){
        return getPhiNodeExpr(phi);
    }

    if(auto li = dyn_cast<const LoadInst>(scev->getValue())){
        AttachedComplexMemLocation acml(ComplexMemLocation::get(li), li, true);
        return memLocToExpr(acml);
    }

    if(auto icmp = dyn_cast<const ICmpInst>(scev->getValue())){
        auto predicate = icmp->getPredicate();
        std::shared_ptr<AST::ExprNode> expr = createByPredicate(predicate);
        expr->addOperand(scevToExpr(_se.getSCEV(icmp->getOperand(0))));
        if(icmp->getNumOperands()>1)
            expr->addOperand(scevToExpr(_se.getSCEV(icmp->getOperand(1))));
        return expr;
    }

    if(auto binOp = dyn_cast<const BinaryOperator>(scev->getValue())){
        if(binOp->getOpcode() == BinaryOperator::Or){
            std::shared_ptr<AST::ExprNode> expr = std::make_shared<AST::Or>();
            assert(instr->getNumOperands()>1);
            expr->addOperand(scevToExpr(_se.getSCEV(binOp->getOperand(0))));
            for(unsigned i = 1; i < binOp->getNumOperands(); ++i){
                expr->addOperand(scevToExpr(_se.getSCEV(binOp->getOperand(i))));
            }
            return expr;
        }
        if(binOp->getOpcode() == BinaryOperator::And){
            std::shared_ptr<AST::ExprNode> expr = std::make_shared<AST::And>();
            assert(instr->getNumOperands()>1);
            expr->addOperand(scevToExpr(_se.getSCEV(binOp->getOperand(0))));
            for(unsigned i = 1; i < binOp->getNumOperands(); ++i){
                expr->addOperand(scevToExpr(_se.getSCEV(binOp->getOperand(i))));
            }
            return expr;
        }
    }

    return AST::FailNode;
}

std::shared_ptr<AST::Node> ValueToExpressionConverter::getPostDomCondExpr(const BasicBlock *from, const BasicBlock *to) const{
    std::set<const BasicBlock*> headSucc(succ_begin(from), succ_end(from));
    std::set<const BasicBlock*> postDomHeadSucc;
    std::copy_if(headSucc.begin(), headSucc.end(), std::inserter(postDomHeadSucc, postDomHeadSucc.begin()),
                 [&to, this](const BasicBlock* bb){return _pdt.dominates(to, bb);});

    std::shared_ptr<AST::Node> cond;
    for(auto succ: postDomHeadSucc){
        cond = AST::createOrOp(cond, getCondExpr(from, succ));
    }

    return cond;
}

std::shared_ptr<AST::Node> ValueToExpressionConverter::getPhiNodeExpr(const PHINode *phiNode) const{
    // Check that all associated blocks are not dominated by phi node's block

    auto phiBlock = const_cast<BasicBlock*>(phiNode->getParent());
    auto branchBb = _dt.getNode(phiBlock)->getIDom()->getBlock();
    std::shared_ptr<AST::Node> expr;
    for(unsigned i = 0; i < phiNode->getNumIncomingValues(); ++i){
        std::shared_ptr<AST::Node> cond;

        auto bb = phiNode->getIncomingBlock(i);
        auto val = phiNode->getIncomingValue(i);
        if(bb == branchBb){
            cond = getCondExpr(branchBb, phiBlock);
        }else{

            if(!_pdt.dominates(phiBlock, bb)){
                cond = getCondExpr(bb, phiBlock);
            }

            const BasicBlock* curBb = bb;
            auto detBb = getNearestNotPostDom(curBb);
            do{
                cond = AST::createAndOp(cond, getPostDomCondExpr(detBb, curBb));
                curBb = detBb;
                detBb = getNearestNotPostDom(curBb);
            }while(curBb != branchBb);
        }
        std::shared_ptr<AST::Node> condValExpr = AST::createMulOp(cond,scevToExpr(_se.getSCEV(val)));
        expr = AST::createSumOp(expr, condValExpr);
    }

    return expr;
}

std::shared_ptr<AST::Node> ValueToExpressionConverter::getCondExpr(const BasicBlock *from, const BasicBlock *to) const{
    using namespace llvm;
    const Instruction* termInstr = from->getTerminator();

    if(dyn_cast<const BranchInst>(termInstr)){
        const BranchInst* bi=dyn_cast<const BranchInst>(termInstr);
        return getCondExprBr(bi, to);
    }
}

std::shared_ptr<AST::Node> ValueToExpressionConverter::getCondExprBr(const BranchInst *br, const BasicBlock *to)const {
    if(br->getNumOperands() == 1) return std::make_shared<AST::LiteralBool>(true);

    const BasicBlock* tbb = br->getSuccessor(0); // Target block if condition is true
    const BasicBlock* fbb = br->getSuccessor(1); // Target block if condition is false

    // if branch is unconditional or both successors are the same return true
    if(br->isUnconditional() || ((tbb == fbb) && tbb == to)){
        return std::make_shared<AST::LiteralBool>(true);
    }

    // get expression for condition
    auto expr = valueToExpr(br->getCondition());

    // if
    if(tbb == to){
        return expr;
    }else if(fbb == to){
        auto notExpr = AST::createNotOp(expr);
        return notExpr;
    }

    return std::shared_ptr<AST::Node>();
}

const BasicBlock *ValueToExpressionConverter::getNearestNotPostDom(const BasicBlock *bb) const{
    const BasicBlock* head = nullptr;
    auto dtNode = _dt.getNode(const_cast<BasicBlock*>(bb));
    while((dtNode = dtNode->getIDom())!=nullptr){
        if(!_pdt.dominates(bb, dtNode->getBlock())){
            return dtNode->getBlock();
        }
    }
    return head;
}

std::shared_ptr<AST::Node> ValueToExpressionConverter::getPathDecisionExpr(const ValueToExpressionConverter::BlockType *from, const ValueToExpressionConverter::BlockType *to) const{
    using CfgType = typename BlockType::CfgType;

    CfgMapping<BlockType> orig2rf;
    CfgType rfcfg = getReachableFromSubgraph(*from, {to}, &orig2rf);

    //There is no path from "from" to "to"
    if(orig2rf.left.count(to) == 0){
        return std::shared_ptr<AST::Node>(new AST::LiteralBool(false));
    }

    CfgMapping<BlockType> rf2rt;
    CfgType rtcfg = getReachesToSubgraph(*orig2rf.left.at(to), &rf2rt);

    auto fromRes = rf2rt.left.at(orig2rf.left.at(from));

    std::set<const BlockType*> exitingBlocks;
    findExitingNodes(rtcfg, rf2rt, exitingBlocks);

    std::shared_ptr<AST::Node> resExpr(new AST::LiteralBool(true));

    for(auto exitingBlock : exitingBlocks){
        std::set<const BlockType*> exitBlocks;
        findExitNodes(exitingBlock, rf2rt, exitBlocks);

        std::shared_ptr<AST::Node> exitExpr(new AST::LiteralBool(false));
        for(auto exitBlock : exitBlocks){
            exitExpr = AST::createOrOp(exitExpr, getCondExpr(exitingBlock->getData(), exitBlock->getData()));
        }

        auto fromToExitingNodeExpr = getPathDecisionExpr(fromRes, exitingBlock);

        auto exitBlockExpr = AST::createNotOp(AST::createAndOp(exitExpr, fromToExitingNodeExpr));

        resExpr = AST::createAndOp(resExpr, exitBlockExpr);
    }

    return resExpr;
}

std::shared_ptr<AST::Node> ValueToExpressionConverter::getPathDecisionExpr(const BasicBlock *from, const BasicBlock *to) const{
    using FTCC = FunctionToCfgConverter<true>;
    using CfgType = Cfg<const BasicBlock*>;

    FTCC::Mapping orig2cfg;
    CfgType cfg = FTCC().convert(from->getParent(), &orig2cfg);

    return getPathDecisionExpr(orig2cfg[from], orig2cfg[to]);
}
