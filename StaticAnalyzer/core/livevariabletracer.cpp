#include "livevariabletracer.h"
#include "easylogging++.h"

using namespace std;
using namespace llvm;

LiveVariableTracer::LiveVariableTracer()
{

}

LiveVariableTracer::~LiveVariableTracer(){}

void LiveVariableTracer::traceValue(const Value *val, const BasicBlock *curBb, std::map<const BasicBlock *, std::set<const Value *> > &bbDepedentVals, map<const BasicBlock *, set<ComplexMemLocation> > &bbDependentLocs){
    assert(val != nullptr && curBB != nullptr);

    std::string valStr;
    raw_string_ostream rso(valStr);val->print(rso);
    LOG(DEBUG)<<"Traces value "<<valStr<<" in basic block "<<getSimpleBasicBlockLabel(curBb);

    if(bbDepedentVals[curBb].count(val)) return;
    bbDepedentVals[curBb].insert(val);

    const Instruction* instr = dyn_cast<Instruction>(val);

    // If val is not an instruction than it is constant, argument or global value.
    // In this case we just propagate this value further.
    // If val is an instruction than we have to propagate this value to basic block
    // where this instruction is executed.
    if(instr == nullptr || instr->getParent() != curBb){
        for(auto pred: predecessors(curBb)){
            traceValue(val, pred, bbDepedentVals, bbDependentLocs);
        }
        return;
    }

    if(auto phi = dyn_cast<PHINode>(val)){
        // At first we have to find values that decide from what predcessor
        // control flow comes to phi's bb. These values are used in
        // conditional branch instructions.
        std::set<Value*> controlDepedentVals;
        findPhiControlDecisionVals(phi, controlDepedentVals);

        for(auto cv: controlDepedentVals){
            if(auto cInstr = dyn_cast<Instruction>(cv)){
                traceValue(cInstr, cInstr->getParent(), bbDepedentVals, bbDependentLocs);
            } else {
                traceValue(cv, curBb, bbDepedentVals, bbDependentLocs);
            }
        }

        for(unsigned i = 0; i < phi->getNumIncomingValues(); ++i){
            auto inVal = phi->getIncomingValue(i);
            auto inBlock = phi->getIncomingBlock(i);

            traceValue(inVal, inBlock, bbDepedentVals, bbDependentLocs);
        }

        return;
    }

    if(auto loadInst = dyn_cast<LoadInst>(val)){
        auto memLoc = ComplexMemLocation::get(loadInst);
        AttachedComplexMemLocation acml(memLoc, loadInst, true);
        traceMemLocationBeforeInstr(acml, loadInst, bbDepedentVals, bbDependentLocs);
        return;
    }

    if(auto callInst = dyn_cast<CallInst>(val)){
        // Usually all call's arguments are critical parameters respect to
        // return value, but we would like to foresee all cases
        traceCallInst(callInst, curBb, bbDepedentVals, bbDependentLocs);
        return;
    }

    // In case of other instructions, trace all operands
    for(auto opI = instr->op_begin(); opI != instr->op_end(); ++opI){
        traceValue(opI->get(), curBb, bbDepedentVals, bbDependentLocs);
    }
}

void LiveVariableTracer::traceCallInst(const CallInst *callInst, const BasicBlock *curBb, map<const BasicBlock *, set<const Value *> > &bbDepedentVals, map<const BasicBlock *, set<ComplexMemLocation> > &bbDependentLocs){
    std::set<Value*> callCritVals;
    std::set<ComplexMemLocation> callCritLocs;
    deduceCallCritParams(callInst, callCritVals, callCritLocs);

    for(auto critVal: callCritVals){
        traceValue(critVal, curBb, bbDepedentVals, bbDependentLocs);
    }

    for(auto critLoc: callCritLocs){
        AttachedComplexMemLocation acml(critLoc, callInst, true);
        traceMemLocationBeforeInstr(acml, callInst, bbDepedentVals, bbDependentLocs);
    }

    return;
}

void LiveVariableTracer::traceMemLocationInclBeforeInstr(AttachedComplexMemLocation ml, const Instruction *instr, map<const BasicBlock *, set<const Value *> > &bbDepedentVals, map<const BasicBlock *, set<ComplexMemLocation> > &bbDependentLocs){
    assert(instr != nullptr);

    std::string instrStr;
    raw_string_ostream rso(instrStr);instr->print(rso);
    LOG(DEBUG)<<"Traces memory location "<<ml<<" including and before instruction "<<instrStr;

    auto curBb = instr->getParent();
    do{
        //if(instr->mayWriteToMemory() && doesInstrModifiesLocation(instr, ml)){
        if(instr->mayWriteToMemory() && doesInstModifiesLocation(instr, ml)){
            std::string instrStr;
            raw_string_ostream rso(instrStr);instr->print(rso);
            LOG(DEBUG)<<"Instruction "<<instrStr<<" modifies location "<<ml<<" try to calculate critical parameters";

            set<Value*> critVals;
            set<ComplexMemLocation> critMemLocs;
            //deduceInstrCritParamsRespectToMemLoc(instr, ml, critVals, critMemLocs);
            deduceInstCritParamsRespectToMemLoc(instr, ml, critVals, critMemLocs);

            for(auto val : critVals){
                traceValue(val, curBb, bbDepedentVals, bbDependentLocs);
            }

            for(auto ml : critMemLocs){
                AttachedComplexMemLocation acml(ml, instr, true);

                traceMemLocationBeforeInstr(acml, instr, bbDepedentVals, bbDependentLocs);
            }
            // Since location has been modified there is no point to trace it
            // further.
            break;
        }
        instr = instr->getPrevNode();
    }while(instr);

    auto& bbMemLocs = bbDependentLocs[curBb];
    if(instr == nullptr && bbMemLocs.find(ml) == bbMemLocs.end()){
        // If instr == nullptr it means that in current block there is no instruction
        // that modifies target memory location. In this case we have to propagate
        // this memloc to predecessors.
        bbMemLocs.insert(ml);
        for(auto pred : predecessors(curBb)){
            traceMemLocationInclBeforeInstr(ml, &pred->back(), bbDepedentVals, bbDependentLocs);
        }
    }
}

void LiveVariableTracer::traceMemLocationBeforeInstr(AttachedComplexMemLocation ml, const Instruction *instr, map<const BasicBlock *, set< const Value *> > &bbDepedentVals, map<const BasicBlock *, set<ComplexMemLocation> > &bbDependentLocs){
    auto curBb = instr->getParent();

    if(instr == &curBb->front()){
        // if it is the first instruction in the block we have to check that we have
        // not propagated this value already, and if we haven't than we propagate
        // to all predcessors. In other case we do nothing.
        auto& bbMemLocs = bbDependentLocs[curBb];
        if(bbMemLocs.find(ml) == bbMemLocs.end()){
            bbMemLocs.insert(ml);
            for(auto pred: predecessors(curBb)){
                traceMemLocationInclBeforeInstr(ml, &pred->back(), bbDepedentVals, bbDependentLocs);
            }
        }
    }else{
        traceMemLocationInclBeforeInstr(ml, instr->getPrevNode(), bbDepedentVals, bbDependentLocs);
    }
}

void LiveVariableTracer::findPhiControlDecisionVals(const PHINode *phi, std::set<Value *> &controlDecisionVals){
    using FTCC = FunctionToCfgConverter<true>;
    using CfgType = Cfg<const BasicBlock*>;

    auto phiBb = const_cast<BasicBlock*>(phi->getParent());
    auto func = phiBb->getParent();

    FTCC::Mapping orig2cfg;
    CfgType cfg = FTCC().convert(func, &orig2cfg);

    auto dt = getDominatorTree(phi->getParent()->getParent());

    auto ncdOrig = phiBb != &func->getEntryBlock() ?
                dt->getNode(phiBb)->getIDom()->getBlock() :
                phiBb;

    std::set<const CfgType::Block*> stopBlocks;
    for(auto& pb : phi->blocks()) stopBlocks.insert(orig2cfg[pb]);

    CfgMapping<CfgType::Block> cfg2rfcfg;
    CfgType scfg = getReachableFromSubgraph(*orig2cfg[ncdOrig], stopBlocks, &cfg2rfcfg);

    auto ncd = cfg2rfcfg.left.at(orig2cfg[ncdOrig]);
    for(BasicBlock* bb : phi->blocks()){
        // Basic block can miss in the graph, if it is dominated by phi node
        if(cfg2rfcfg.left.count(orig2cfg[bb])){
            auto pb = cfg2rfcfg.left.at(orig2cfg[bb]);
            findPathDecisionValues(ncd, pb, controlDecisionVals);
        }
    }

    // What if we can go from phi predecessor to the node, from which we can never
    // achieve phi block again.
    for(BasicBlock* bb : phi->blocks()){
        auto ppStopBlocks = stopBlocks;
        ppStopBlocks.insert(orig2cfg[phiBb]);
        ppStopBlocks.erase(orig2cfg[bb]);

        CfgMapping<CfgType::Block> cfg2pcfg;
        CfgType pcfg = getReachableFromSubgraph(*orig2cfg[bb], ppStopBlocks, &cfg2pcfg);

        auto phiBbb = cfg2pcfg.left.at(orig2cfg[phiBb]);
        auto phiPred = cfg2pcfg.left.at(orig2cfg[bb]);

        findPathDecisionValues(phiPred, phiBbb, controlDecisionVals);
    }
}
