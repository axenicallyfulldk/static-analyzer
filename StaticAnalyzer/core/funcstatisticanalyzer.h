#ifndef FUNCSTATISTICANALYZER_H
#define FUNCSTATISTICANALYZER_H

#include <llvm/IR/Function.h>

#include <string>

#include "blocktripevaluater.h"
#include "blocktripclassifier.h"
#include "valuetoexpressionconverter.h"
#include "syntax_tree.h"

#include "criteria.h"

class FuncStatisticAnalyzer
{
public:
    typedef std::vector<std::shared_ptr<AST::ValueNode>> ValueVector;

    template<class T>
    struct OccurenceInfo{
        std::set<T> instances;
        std::set<const llvm::CallInst*> significantCalls;
    };

    static bool criticalParamSort(std::shared_ptr<AST::ValueNode> a, std::shared_ptr<AST::ValueNode> b){
        static std::map<AST::NodeType, int> priority = {
            {AST::NodeType::Argument, 0},
            {AST::NodeType::ArgMemLoc, 1},
            {AST::NodeType::GlobalVar, 2},
            {AST::NodeType::GlobalVarMemLoc, 3}
        };

        if(priority[a->getType()] < priority[b->getType()]){
            return true;
        }

        if(AST::ArgumentNode::classof(a.get()) && AST::ArgumentNode::classof(b.get())){
            auto aArg = std::dynamic_pointer_cast<AST::ArgumentNode>(a);
            auto bArg = std::dynamic_pointer_cast<AST::ArgumentNode>(b);
            if(aArg->getIrArgument() == nullptr || bArg->getIrArgument() == nullptr){
                return aArg->to_string()<bArg->to_string();
            }else{
                return aArg->getIrArgument()->getArgNo() < bArg->getIrArgument()->getArgNo();
            }
        }

        if(AST::ArgumentMemLocNode::classof(a.get()) && AST::ArgumentMemLocNode::classof(b.get())){
            auto aArgNode = std::dynamic_pointer_cast<AST::ArgumentMemLocNode>(a);
            auto bArgNode = std::dynamic_pointer_cast<AST::ArgumentMemLocNode>(b);
            auto aMemLoc = aArgNode->getMemLoc();
            auto bMemLoc = bArgNode->getMemLoc();
            if(aMemLoc == NullComplexMemLocation || bMemLoc == NullComplexMemLocation){
                return aArgNode->to_string()<bArgNode->to_string();
            }else{
                auto aArg = llvm::dyn_cast<llvm::Argument>(aMemLoc.base);
                auto bArg = llvm::dyn_cast<llvm::Argument>(bMemLoc.base);
                if(aArg->getArgNo() < bArg->getArgNo()) return true;
                if(aMemLoc < bMemLoc) return true;
            }
        }

        if(AST::GlobalVarNode::classof(a.get()) && AST::GlobalVarNode::classof(b.get())){
            auto aArg = std::dynamic_pointer_cast<AST::GlobalVarNode>(a);
            auto bArg = std::dynamic_pointer_cast<AST::GlobalVarNode>(b);
            return aArg->to_string()<bArg->to_string();
        }

        if(AST::GlobalVarMemLocNode::classof(a.get()) && AST::GlobalVarMemLocNode::classof(b.get())){
            auto aArgNode = std::dynamic_pointer_cast<AST::GlobalVarMemLocNode>(a);
            auto bArgNode = std::dynamic_pointer_cast<AST::GlobalVarMemLocNode>(b);
            auto aMemLoc = aArgNode->getMemLoc();
            auto bMemLoc = bArgNode->getMemLoc();
            if(aMemLoc == NullComplexMemLocation || bMemLoc == NullComplexMemLocation){
                return aArgNode->to_string()<bArgNode->to_string();
            }else{
                auto aArg = llvm::dyn_cast<llvm::GlobalVariable>(aMemLoc.base);
                auto bArg = llvm::dyn_cast<llvm::GlobalVariable>(bMemLoc.base);
                if(aArg->getName() < bArg->getName()) return true;
                if(aMemLoc < bMemLoc) return true;
            }
        }

        return false;
    }

private:
    BlockTripClassifier _btc;
    BlockTripEvaluater _bte;

    llvm::TargetLibraryInfoImpl _tlii;
    llvm::TargetLibraryInfo _tli;
    llvm::AssumptionCache _ac;
    llvm::DominatorTree _dt;
    llvm::PostDominatorTree _pdt;
    llvm::LoopInfo _li;
    llvm::ScalarEvolution _se;
    ValueToExpressionConverter _vtec;

    const llvm::Function* _func;

    std::set<MathFuncType> _usedMathFuncs;

    std::set<MpiFuncType> _usedMpiFuncs;

    std::map<const llvm::BasicBlock*, OccurenceInfo<const llvm::LoadInst*>> _loads;
    std::map<const llvm::BasicBlock*, OccurenceInfo<const llvm::StoreInst*>> _stores;
    std::map<const llvm::BasicBlock*, OccurenceInfo<const llvm::Instruction*>> _flops4;
    std::map<const llvm::BasicBlock*, OccurenceInfo<const llvm::Instruction*>> _flops8;

    std::map<const llvm::BasicBlock*, std::map<MathFuncType, OccurenceInfo<const llvm::CallInst*>>> _mathCalls;

    std::map<const llvm::BasicBlock*, std::map<MpiFuncType, OccurenceInfo<const llvm::CallInst*>>> _mpiCalls;

    std::map<const llvm::BasicBlock*, OccurenceInfo<const llvm::CallInst*>> _mpiSendCalls;
    std::map<const llvm::BasicBlock*, OccurenceInfo<const llvm::CallInst*>> _mpiRecvCalls;

    void addInstance(CriteriaType criteria, const llvm::BasicBlock* bb, const llvm::Instruction* instr);

    void addSignificanFunction(CriteriaType criteria, const llvm::BasicBlock* bb, const llvm::CallInst* call);

    void countInstructions(CriteriaType criteria);

    void countAllInstructions();

    static bool isCriticalParam(std::shared_ptr<AST::Node> expr){
        return AST::ArgumentNode::classof(expr.get()) ||
                AST::GlobalVarNode::classof(expr.get()) ||
                AST::ArgumentMemLocNode::classof(expr.get()) ||
                AST::GlobalVarMemLocNode::classof(expr.get());
    }


    typedef std::set<std::shared_ptr<AST::ValueNode>, decltype (&criticalParamSort)> SortedParamSet;

    static void extractCriticalParams(std::shared_ptr<AST::Node> node, SortedParamSet& critParams){
        if(AST::ExprNode::classof(node.get())){
            auto expr = std::dynamic_pointer_cast<AST::ExprNode>(node);
            for(unsigned i = 0; i < expr->getOperandsCount(); ++i){
                extractCriticalParams(expr->getOperand(i), critParams);
            }
        }

        if(isCriticalParam(node)){
            critParams.insert(std::dynamic_pointer_cast<AST::ValueNode>(node));
        }
    }



public:
    FuncStatisticAnalyzer(llvm::Function* func);

    std::shared_ptr<AST::Node> getFlopExpr();

    std::shared_ptr<AST::Node> getFlop4Expr();

    std::shared_ptr<AST::Node> getFlop8Expr();

    std::shared_ptr<AST::Node> getWriteAccessExpr();

    std::shared_ptr<AST::Node> getReadAccessExpr();

    std::shared_ptr<AST::Node> getStoreSizeExpr();

    std::shared_ptr<AST::Node> getLoadSizeExpr();

    std::vector<MathFuncType> getUsedMathFuncs(){
        return std::vector<MathFuncType>(_usedMathFuncs.begin(),_usedMathFuncs.end());
    }

    std::shared_ptr<AST::Node> getMathFuncExpr(MathFuncType type);

    std::vector<MpiFuncType> getUsedMpiFuncs(){
        return std::vector<MpiFuncType>(_usedMpiFuncs.begin(),_usedMpiFuncs.end());
    }

    std::shared_ptr<AST::Node> getMpiFuncExpr(MpiFuncType type);

    bool doesSendData(){
        return _mpiSendCalls.size();
    }
    std::shared_ptr<AST::Node> getSendedByteExpr();

    bool doesRecvData(){
        return _mpiRecvCalls.size();
    }
    std::shared_ptr<AST::Node> getRecievedByteExpr();


    static ValueVector extractSortedCriticalParams(std::shared_ptr<AST::Node> expr){
        SortedParamSet critParams(&criticalParamSort);
        extractCriticalParams(expr, critParams);

        ValueVector cp(critParams.begin(), critParams.end());
        std::sort(cp.begin(), cp.end(), criticalParamSort);
        return cp;
    }

};

#endif // FUNCSTATISTICANALYZER_H
