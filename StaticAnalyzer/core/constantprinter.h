#ifndef CONSTANTPRINTER_H
#define CONSTANTPRINTER_H

#include <string>
#include <sstream>
#include <llvm/IR/Constants.h>
#include <llvm/ADT/SmallString.h>
#include <llvm/Support/raw_ostream.h>
#include <llvm/Support/Format.h>

using namespace llvm;

class ConstantPrinter{
public:
    static std::string to_string(const ConstantData* constValue){
        std::stringstream out;
        if (const ConstantInt *CI = dyn_cast<const ConstantInt>(constValue)) {
          if (CI->getType()->isIntegerTy(1)) {
            out << (CI->getZExtValue() ? "true" : "false");
            return out.str();
          }

          std::string hmm;
          raw_string_ostream rso(hmm);
          rso << CI->getValue();
          out<<rso.str();
          return out.str();
        }

        if (const ConstantFP *CFP = dyn_cast<const ConstantFP>(constValue)) {
          const APFloat &APF = CFP->getValueAPF();
          if (&APF.getSemantics() == &APFloat::IEEEsingle() ||
              &APF.getSemantics() == &APFloat::IEEEdouble()) {
            // We would like to output the FP constant value in exponential notation,
            // but we cannot do this if doing so will lose precision.  Check here to
            // make sure that we only output it in exponential format if we can parse
            // the value back and get the same value.
            //
            bool ignored;
            bool isDouble = &APF.getSemantics() == &APFloat::IEEEdouble();
            bool isInf = APF.isInfinity();
            bool isNaN = APF.isNaN();
            if (!isInf && !isNaN) {
              double Val = isDouble ? APF.convertToDouble() : APF.convertToFloat();
              SmallString<128> StrVal;
              APF.toString(StrVal, 6, 0, false);
              // Check to make sure that the stringized number is not some string like
              // "Inf" or NaN, that atof will accept, but the lexer will not.  Check
              // that the string matches the "[-+]?[0-9]" regex.
              //
              assert(((StrVal[0] >= '0' && StrVal[0] <= '9') ||
                      ((StrVal[0] == '-' || StrVal[0] == '+') &&
                       (StrVal[1] >= '0' && StrVal[1] <= '9'))) &&
                     "[-+]?[0-9] regex does not match!");
              // Reparse stringized version!
              if (APFloat(APFloat::IEEEdouble(), StrVal).convertToDouble() == Val) {
                out << StrVal.data();
                return out.str();
              }
            }
            // Otherwise we could not reparse it to exactly the same value, so we must
            // output the string in hexadecimal format!  Note that loading and storing
            // floating point types changes the bits of NaNs on some hosts, notably
            // x86, so we must not use these types.
            static_assert(sizeof(double) == sizeof(uint64_t),
                          "assuming that double is 64 bits!");
            APFloat apf = APF;
            // Floats are represented in ASCII IR as double, convert.
            if (!isDouble)
              apf.convert(APFloat::IEEEdouble(), APFloat::rmNearestTiesToEven,
                                &ignored);

            std::string hmm;
            raw_string_ostream rso(hmm);
            rso << format_hex(apf.bitcastToAPInt().getZExtValue(), 0, /*Upper=*/true);
            out<<rso.str();
            return out.str();
          }

          // Either half, or some form of long double.
          // These appear as a magic letter identifying the type, then a
          // fixed number of hex digits.
          out << "0x";
          APInt API = APF.bitcastToAPInt();
          if (&APF.getSemantics() == &APFloat::x87DoubleExtended()) {
              std::string hmm;
              raw_string_ostream rso(hmm);
            rso << 'K';
            rso << format_hex_no_prefix(API.getHiBits(16).getZExtValue(), 4,
                                        /*Upper=*/true);
            rso << format_hex_no_prefix(API.getLoBits(64).getZExtValue(), 16,
                                        /*Upper=*/true);
            return rso.str();
          } else if (&APF.getSemantics() == &APFloat::IEEEquad()) {
              std::string hmm;
              raw_string_ostream rso(hmm);
            rso << 'L';
            rso << format_hex_no_prefix(API.getLoBits(64).getZExtValue(), 16,
                                        /*Upper=*/true);
            rso << format_hex_no_prefix(API.getHiBits(64).getZExtValue(), 16,
                                        /*Upper=*/true);
            return rso.str();
          } else if (&APF.getSemantics() == &APFloat::PPCDoubleDouble()) {
              std::string hmm;
              raw_string_ostream rso(hmm);
            rso << 'M';
            rso << format_hex_no_prefix(API.getLoBits(64).getZExtValue(), 16,
                                        /*Upper=*/true);
            rso << format_hex_no_prefix(API.getHiBits(64).getZExtValue(), 16,
                                        /*Upper=*/true);
            return rso.str();
          } else if (&APF.getSemantics() == &APFloat::IEEEhalf()) {
              std::string hmm;
              raw_string_ostream rso(hmm);
            rso << 'H';
            rso << format_hex_no_prefix(API.getZExtValue(), 4,
                                        /*Upper=*/true);
            return rso.str();
          } else
            llvm_unreachable("Unsupported floating point type");
          return out.str();
        }
    }
};

#endif // CONSTANTPRINTER_H
