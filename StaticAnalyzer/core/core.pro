QT -= gui

TEMPLATE = lib
CONFIG += staticlib c++17
CONFIG -= qt

#LIBS += $$system('llvm-config --ldflags')
#LIBS += $$system('llvm-config --libs core')

INCLUDEPATH += $$system('llvm-config --includedir')
QMAKE_CXXFLAGS += $$system('llvm-config --cxxflags')
QMAKE_CXXFLAGS -= -fno-exceptions

SOURCES += \
        blocktripclassifier.cpp \
        blocktripevaluater.cpp \
        cfg.cpp \
        cfg_utils.cpp \
        criteria.cpp \
        criticalparamtracer.cpp \
        customcriticalparamtracer.cpp \
        easylogging++.cc \
        flowsensitiveaa.cpp \
        funcstatisticanalyzer.cpp \
        livevariabletracer.cpp \
        memloctypes.cpp \
        syntax_tree.cpp \
        utils.cpp \
        valuetoexpressionconverter.cpp

HEADERS += \
    blocktripclassifier.h \
    blocktripevaluater.h \
    cfg.h \
    cfg_utils.h \
    constantprinter.h \
    criteria.h \
    criticalparamanalyzer.h \
    criticalparamtracer.h \
    customcriticalparamtracer.h \
    easylogging++.h \
    flowsensitiveaa.h \
    funcstatisticanalyzer.h \
    livevariabletracer.h \
    memloctypes.h \
    syntax_tree.h \
    utils.h \
    valuetoexpressionconverter.h
