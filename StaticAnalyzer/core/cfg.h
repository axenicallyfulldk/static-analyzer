#ifndef CFG_H
#define CFG_H

#include <iostream>
#include <string>
#include <type_traits>
#include <variant>

#include <boost/graph/graph_traits.hpp>
#include <boost/graph/dominator_tree.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/graph_traits.hpp>

#define BOOST_NO_EXCEPTIONS
#include <boost/throw_exception.hpp>


template<class T>
class Cfg{
protected:
    using Graph = boost::adjacency_list<boost::setS, boost::listS, boost::bidirectionalS, T>;

    using out_edge_iterator = typename boost::graph_traits<Graph>::out_edge_iterator;
    using in_edge_iterator = typename boost::graph_traits<Graph>::in_edge_iterator;
    using vertex_iterator = typename boost::graph_traits<Graph>::vertex_iterator;
    using edge_iterator = typename boost::graph_traits<Graph>::edge_iterator;

public:
    using BlockId = typename boost::graph_traits<Graph>::vertex_descriptor;
    using EdgeId = typename boost::graph_traits<Graph>::edge_descriptor;

    class Block;

    template <bool isConstant>
    class BlockIterator{
    private:
        friend class Cfg;
        friend class Block;

        using inner_iterator = std::variant<out_edge_iterator, in_edge_iterator, vertex_iterator>;

        Cfg& _cfg;
        inner_iterator _iter;

        BlockIterator(Cfg& graph, inner_iterator iter):_cfg(graph),_iter(iter){}

        struct TargetBlockVisitor{
            BlockId& nodeId;
            TargetBlockVisitor(BlockId& node):nodeId(node){}
            void operator()(out_edge_iterator oei){nodeId = oei->m_target;}
            void operator()(in_edge_iterator iei){nodeId = iei->m_source;}
            void operator()(vertex_iterator vi){nodeId = *vi;}
        };

    public:
        using iterator_category = std::forward_iterator_tag;
        using difference_type = ptrdiff_t;
        using value_type = T;
        using pointer = std::conditional_t<isConstant, const Block*, Block*>;
        using reference = std::conditional_t<isConstant, const Block&, Block&>;

        BlockIterator<isConstant>& operator=(BlockIterator<isConstant>& it){
            _iter = it._iter;
            return *this;
        }

        /// pre-increment
        BlockIterator<isConstant>& operator++(){
            std::visit([](auto&& b){++b;}, _iter);
            return *this;
        }

        /// post-increment
        BlockIterator<isConstant> operator++(int){
            BlockIterator<isConstant> old(_cfg, _iter);
            ++(*this);
            return old;
        }

        reference operator*()const{
            BlockId nodeId;
            std::visit(TargetBlockVisitor(nodeId), _iter);
            return _cfg.operator[](nodeId);
        }

        pointer operator->()const{
            BlockId nodeId;
            std::visit(TargetBlockVisitor(nodeId), _iter);
            return &_cfg.operator[](nodeId);
        }

        operator BlockIterator<true>()const{
            return BlockIterator<true>(_cfg, _iter);
        }

        template<bool R>
        bool operator==(const BlockIterator<R>& it) const{
            return _iter == it._iter;
        }

        template<bool R>
        bool operator!=(const BlockIterator<R>& it) const{
            return !(*this == it);
        }
    };

    template<bool isConstant>
    class BlockRange{
    private:
        BlockIterator<isConstant> _begin, _end;
    public:
        BlockRange(BlockIterator<isConstant> begin, BlockIterator<isConstant> end):_begin(begin), _end(end){
        }
        BlockIterator<isConstant> begin(){ return _begin;}
        BlockIterator<isConstant> end(){ return _end;}

        BlockIterator<true> begin() const{ return _begin;}
        BlockIterator<true> end() const{ return _end;}

        operator BlockRange<true>(){
            return BlockRange<true>(_begin, _end);
        }
    };

    class Block{
    private:
        Cfg& _cfg;
        BlockId _id;

        Block(Cfg& g, BlockId id):_cfg(g), _id(id){}

        friend class BlockIterator<true>;
        friend class BlockIterator<false>;
        friend class Cfg;

    public:

        Block(const Block& b) = delete;
        Block& operator=(const Block& b) = delete;

        using CfgType = Cfg;

        const T& getData(){
            return _cfg._graph[_id];
        }

        T& getData()const{
            return _cfg._graph[_id];
        }

        BlockId getId()const{
            return _id;
        }

        const Cfg& getGraph()const{
            return _cfg;
        }

        Cfg& getGraph(){
            return _cfg;
        }

        bool operator==(const Block& block)const{
            return &_cfg == &block._cfg && _id == block._id;
        }

        bool operator<(const Block& block)const{
            if(&_cfg < &block._cfg) return true;
            else if(&block._cfg < &_cfg) return false;

            return _id < block._id;
        }

        size_t succCount()const{
            return boost::out_degree(_id, _cfg._graph);
        }

        BlockRange<false> successors(){
            auto oei = boost::out_edges(_id, _cfg._graph);
            return BlockRange<false>(BlockIterator<false>(_cfg, oei.first),
                                     BlockIterator<false>(_cfg, oei.second));
        }

        BlockRange<true> successors() const{
            auto oei = boost::out_edges(_id, _cfg._graph);
            return BlockRange<true>(BlockIterator<true>(_cfg, oei.first),
                                     BlockIterator<true>(_cfg, oei.second));
        }

        size_t predCount() const{
            return boost::in_degree(_id, _cfg._graph);
        }

        BlockRange<false> predecessors(){
            auto iei = boost::in_edges(_id, _cfg._graph);
            return BlockRange<false>(BlockIterator<false>(_cfg, iei.first),
                                     BlockIterator<false>(_cfg, iei.second));
        }

        BlockRange<true> predecessors()const{
            auto iei = boost::in_edges(_id, _cfg._graph);
            return BlockRange<true>(BlockIterator<true>(_cfg, iei.first),
                                    BlockIterator<true>(_cfg, iei.second));
        }
    };

    class Edge{
    private:

        Cfg& _cfg;
        EdgeId _edgeId;

        Edge(Cfg& g, EdgeId e):_cfg(g),_edgeId(e){}

        friend class Cfg;
    public:
        Block getSource(){
            return _cfg[boost::source(_edgeId, _cfg._graph)];
        }

        const Block getSource()const{
            return _cfg[boost::source(_edgeId, _cfg._graph)];
        }

        Block getDest(){
            return _cfg[boost::target(_edgeId, _cfg._graph)];
        }

        const Block getDest()const{
            return _cfg[boost::target(_edgeId, _cfg._graph)];
        }
    };


    Cfg(){}
    Cfg(const Cfg<T>& cfg){
        std::map<BlockId, BlockId> blockMapping;
        // Copying graph
        for(auto vi = boost::vertices(cfg._graph); vi.first != vi.second; ++vi.first){
            auto vid = boost::add_vertex(cfg._graph[*vi.first], _graph);
            blockMapping[*vi.first] = vid;
        }

        for(auto vi = boost::vertices(cfg._graph); vi.first != vi.second; ++vi.first){
            for(auto ei = boost::out_edges(*vi.first, cfg._graph); ei.first != ei.second; ++ei.first){
                boost::add_edge(blockMapping[ei.first->m_source],
                        blockMapping[ei.first->m_target], _graph);
            }
        }

        for(auto& iter : blockMapping){
            _blocks[iter.second] = new Block(*this, iter.second);
        }

        setEntryBlock(blockMapping[cfg._entryBlock]);
    }

    ~Cfg(){
        for(auto b : _blocks){
            delete b.second;
        }
    }

    BlockRange<false> blocks(){
        auto bi = boost::vertices(_graph);
        return BlockRange<false>(BlockIterator<false>(*this, bi.first),
                                 BlockIterator<false>(*this, bi.second));
    }

    BlockRange<true> blocks() const{
        auto bi = boost::vertices(_graph);
        return BlockRange<true>(BlockIterator<true>(const_cast<Cfg&>(*this), bi.first),
                                BlockIterator<true>(const_cast<Cfg&>(*this), bi.second));
    }

    size_t getBlockCount() const{
        return boost::num_vertices(_graph);
    }

    Block& addBlock(const T& data){
        auto nodeId = boost::add_vertex(data, _graph);
        Block* block = new Block(*this, nodeId);
        _blocks.emplace(nodeId, block);
        if(getBlockCount() == 1) setEntryBlock(nodeId);
        return *block;
    }

    Edge addEdge(BlockId from, BlockId to){
        auto edge = boost::add_edge(from, to, _graph);
        return Edge(_graph, edge.first);
    }

    Edge addEdge(const Block& from, const Block& to){
        auto edge = boost::add_edge(from.getId(), to.getId(), _graph);
        return Edge(*this, edge.first);
    }

    void removeBlock(BlockId blockId){
        boost::clear_vertex(blockId, _graph);
        boost::remove_vertex(blockId, _graph);
    }

    void removeEdge(EdgeId edgeId){
        boost::remove_edge(edgeId, _graph);
    }

    void setEntryBlock(BlockId entry){
        _entryBlock = entry;
    }

    void setEntryBlock(const Block& block){
        _entryBlock = block.getId();
    }

    Block& getEntryBlock(){
        return (*this)[_entryBlock];
    }

    const Block& getEntryBlock()const{
        return (*this)[_entryBlock];
    }

    Block& operator[](BlockId nodeId){
        return *_blocks.find(nodeId)->second;
    }

    const Block& operator[](BlockId nodeId) const{
        return *_blocks.find(nodeId)->second;
    }

protected:
    std::map<BlockId, Block*> _blocks;
    Graph _graph;
    BlockId _entryBlock;

};


#endif // CFG_H
