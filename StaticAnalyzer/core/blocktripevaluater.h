#ifndef BLOCKTRIPEVALUATER_H
#define BLOCKTRIPEVALUATER_H

#include "blocktripclassifier.h"
#include "syntax_tree.h"
#include "valuetoexpressionconverter.h"

#include <llvm/Analysis/TargetLibraryInfo.h>
#include <llvm/Analysis/AssumptionCache.h>
#include <llvm/Analysis/LoopInfo.h>
#include <llvm/Analysis/ScalarEvolution.h>
#include <llvm/Analysis/ScalarEvolutionExpressions.h>

#include <algorithm>

#include <iostream>

class BlockTripEvaluater
{
private:
    const llvm::Function* _func;

    llvm::TargetLibraryInfoImpl _tlii;
    llvm::TargetLibraryInfo _tli;
    llvm::AssumptionCache _ac;
    llvm::DominatorTree _dt;
    llvm::PostDominatorTree _pdt;
    llvm::LoopInfo _li;
    llvm::ScalarEvolution _se;

    BlockTripClassifier _btc;

    ValueToExpressionConverter _vtec;


    /// Return expression of loop head trip count
    std::shared_ptr<AST::Node> getRotatedLoopHeadTripCount(const llvm::Loop* loop);

    /// Return expression of loop body trip count
    std::shared_ptr<AST::Node> getLoopBodyTripCount(const llvm::Loop* loop);

    /// Return expression for control flow condition from bb's head to bb
    std::shared_ptr<AST::Node> getBranchCondition(const llvm::BasicBlock* bb);

public:
    BlockTripEvaluater(llvm::Function* func);

    /// Return expression of basic block trip count
    std::shared_ptr<AST::Node> getBasicBlockTripCount(const llvm::BasicBlock* bb, bool accountForLoop=true);
};

#endif // BLOCKTRIPEVALUATER_H
