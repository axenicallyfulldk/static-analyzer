#include "syntax_tree.h"

#include "constantprinter.h"

#include <iostream>

using namespace AST;

#define DEF_TYPE_FUNC_OP(ClassName)\
constexpr char ClassName::_typeStr[];\
constexpr char ClassName::_funcNameStr[];\
constexpr char ClassName::_opNameStr[];

#define DEF_TYPE_FUNC(ClassName)\
constexpr char ClassName::_typeStr[];\
constexpr char ClassName::_funcNameStr[];


constexpr char UnknownNode::_typeStr[];

DEF_TYPE_FUNC_OP(LessThen);

DEF_TYPE_FUNC_OP(LessOrEqual);

DEF_TYPE_FUNC_OP(GreaterThen);

DEF_TYPE_FUNC_OP(GreaterOrEqual);

DEF_TYPE_FUNC_OP(EqualTo);

DEF_TYPE_FUNC_OP(NotEqualTo);

DEF_TYPE_FUNC_OP(Not);

DEF_TYPE_FUNC(MaxFunc);

DEF_TYPE_FUNC(MinFunc);

DEF_TYPE_FUNC_OP(SumOp);

DEF_TYPE_FUNC_OP(SubOp);

DEF_TYPE_FUNC_OP(MulOp);

DEF_TYPE_FUNC_OP(DivOp);

DEF_TYPE_FUNC(CustomFunc);

constexpr char ArgumentNode::_typeStr[];

constexpr char GlobalVarNode::_typeStr[];

constexpr char ArgumentMemLocNode::_typeStr[];

constexpr char LiteralFloat::_typeStr[];

constexpr char LiteralInt::_typeStr[];

constexpr char LiteralBool::_typeStr[];

DEF_TYPE_FUNC_OP(And);

DEF_TYPE_FUNC_OP(Or);


static std::string to_string(const ComplexMemLocation& cml){
    std::string res;
    if(cml.base->hasName()){
        res = cml.base->getName();
    } else {
        if(auto arg = dyn_cast<Argument>(cml.base)){
            res = std::to_string(arg->getArgNo());
        }
    }
    for(auto& shift : cml.shifts){
        res += "_";
        res += std::to_string(shift.offset);
    }
    return res;
}


Node::Node(NodeType type, const char *typeStr):_type(type),
    _typeStr(typeStr){

}

Node::~Node(){}

bool Node::hasParent() const{
    return !_parent.expired();
}

std::shared_ptr<Node> Node::getParent() const{
    return _parent.lock();
}

NodeType Node::getType() const {
    return _type;
}

const char *Node::getTypeString() const{
    return _typeStr;
}

ExprNode::ExprNode(bool isOp, bool isComm, bool isUnary, NodeType type,
                   const char *typeStr):Node(type, typeStr), _isOperator(isOp),
    _isCommutative(isComm), _isUnary(isUnary), _funcName(nullptr),
    _opName(nullptr){
}

ExprNode::ExprNode(bool isOp, bool isComm, bool isUnary, const char *funcName,
                        const char *opName, NodeType type, const char *typeStr):
    Node(type, typeStr), _isOperator(isOp), _isCommutative(isComm),
    _isUnary(isUnary),  _funcName(funcName), _opName(opName){
}

void ExprNode::setOpName(const char *opName){
    _opName = opName;
}

void ExprNode::setFuncName(const char *funcName){
    _funcName = funcName;
}

const char *ExprNode::getFuncName() const{
    return _funcName;
}

const char *ExprNode::getOpName() const{
    return _opName;
}

bool ExprNode::isOperator() const{
    return _isOperator;
}

bool ExprNode::isUnaryOpeator() const{
    return _isUnary;
}

bool ExprNode::isCommutative() const{
    return _isCommutative;
}

unsigned ExprNode::getOperandsCount() const{
    return _childs.size();
}

std::shared_ptr<Node> ExprNode::getOperand(unsigned i) const{
    return _childs[i];
}

void ExprNode::addOperand(std::shared_ptr<Node> op){
    _childs.push_back(op);
}

void ExprNode::setOperand(unsigned i, std::shared_ptr<Node> op){
    _childs[i] = op;
}

std::shared_ptr<Node> ExprNode::removeOperand(unsigned i){
    std::shared_ptr<Node> op = _childs[i];
    _childs.erase(_childs.begin() + i);
    return op;
}

inline std::string wrapNode(const Node* node){
    if(ExprNode::classof(node)){
        const ExprNode *exprNode = dynamic_cast<const ExprNode *>(node);
        if(exprNode->isOperator()){
            return std::string("(") + exprNode->to_string() + ")";
        }
    }

    return node->to_string();
}

std::string ExprNode::to_string() const{
    std::string res;
    if(isOperator()){
        if(isUnaryOpeator()){
            assert(_childs.size()==1 && "Unary operator has to have one operand");
            res = std::string(getOpName()) + " (" + _childs[0].get()->to_string() + ")";
        }else{
            assert(_childs.size()>=2 && "Operator has to have at least two operands");
            res = wrapNode(_childs[0].get());
            for(unsigned i=1; i < _childs.size(); ++i){
                res += std::string(" ") + getOpName() + " " + wrapNode(_childs[i].get());
            }
        }
    } else{
        res = std::string(getFuncName()) + "(";
        if(getOperandsCount()>0){
            res += _childs[0]->to_string();
            for(unsigned i=1; i < _childs.size(); ++i){
                res += ", " + _childs[i]->to_string();
            }
        }
        res += ")";
    }

    return res;
}

bool ExprNode::classof(const Node *node){
    NodeType type = node->getType();

    if(BooleanNode::classof(node) ||
            type == NodeType::MaxFunc ||
            type == NodeType::MinFunc ||
            type == NodeType::SumOp ||
            type == NodeType::SubOp ||
            type == NodeType::MulOp ||
            type == NodeType::DivOp ||
            type == NodeType::CustomFunc)
        return true;

    return false;
}

BooleanNode::BooleanNode(bool isOp, bool isComm, bool isUnary,
                         const char *funcName, const char *opName,
                         NodeType type, const char *typeStr):
    ExprNode(isOp, isComm, isUnary, funcName, opName, type, typeStr){
}

bool BooleanNode::classof(const Node *node){
    NodeType type = node->getType();

    if(type == NodeType::LessThan ||
            type == NodeType::LessOrEqual ||
            type == NodeType::GreaterThan ||
            type == NodeType::GreaterOrEqual ||
            type == NodeType::EqualTo ||
            type == NodeType::NotEqualTo ||
            type == NodeType::Not ||
            type == NodeType::And ||
            type == NodeType::Or)
        return true;
    return false;
}

LessThen::LessThen():BooleanNode (true, false, false, _funcNameStr, _opNameStr,
                                       NodeType::LessThan, _typeStr){
}

bool LessThen::classof(const Node *node){
    return node->getType() == NodeType::LessThan;
}

LessOrEqual::LessOrEqual():BooleanNode (true, false, false, _funcNameStr,
                                        _opNameStr, NodeType::LessOrEqual,
                                        _typeStr){
}

bool LessOrEqual::classof(const Node *node){
    return node->getType() == NodeType::LessOrEqual;
}

GreaterThen::GreaterThen():BooleanNode (true, false, false, _funcNameStr,
                                        _opNameStr, NodeType::GreaterThan,
                                        _typeStr){
}

bool GreaterThen::classof(const Node *node){
    return node->getType() == NodeType::GreaterThan;
}

GreaterOrEqual::GreaterOrEqual():BooleanNode (true, false, false, _funcNameStr,
                                              _opNameStr, NodeType::GreaterOrEqual,
                                              _typeStr){
}

bool GreaterOrEqual::classof(const Node *node){
    return node->getType() == NodeType::GreaterOrEqual;
}

EqualTo::EqualTo():BooleanNode (true, true, false, _funcNameStr, _opNameStr,
                                     NodeType::EqualTo, _typeStr){
}

bool EqualTo::classof(const Node *node){
    return node->getType() == NodeType::EqualTo;
}

Not::Not():BooleanNode (true, false, true, _funcNameStr, _opNameStr,
                        NodeType::GreaterOrEqual, _typeStr){
}

bool Not::classof(const Node *node){
    return node->getType() == NodeType::Not;
}

NotEqualTo::NotEqualTo():BooleanNode (true, true, false, _funcNameStr, _opNameStr,
                                           NodeType::NotEqualTo, _typeStr){
}

bool NotEqualTo::classof(const Node *node){
    return node->getType() == NodeType::NotEqualTo;
}

MaxFunc::MaxFunc():ExprNode(false, true, false,_funcNameStr, nullptr,
                            NodeType::MaxFunc ,_typeStr){
}

bool MaxFunc::classof(const Node *node){
    return node->getType() == NodeType::MaxFunc;
}

MinFunc::MinFunc():ExprNode(false, true, false, _funcNameStr, nullptr,
                            NodeType::MinFunc ,_typeStr){
}

bool MinFunc::classof(const Node *node){
    return node->getType() == NodeType::MinFunc;
}

SumOp::SumOp():ExprNode(true, true, false, _funcNameStr, _opNameStr,
                        NodeType::SumOp ,_typeStr){
}

bool SumOp::classof(const Node *node){
    return node->getType() == NodeType::SumOp;
}

SubOp::SubOp():ExprNode(true, false, false, _funcNameStr, _opNameStr,
                        NodeType::SubOp ,_typeStr){
}

bool SubOp::classof(const Node *node){
    return node->getType() == NodeType::SubOp;
}

MulOp::MulOp():ExprNode(true, true, false, _funcNameStr, _opNameStr,
                        NodeType::MulOp ,_typeStr){
}

bool MulOp::classof(const Node *node){
    return node->getType() == NodeType::MulOp;
}

DivOp::DivOp():ExprNode(true, false, false, _funcNameStr, _opNameStr,
                        NodeType::DivOp ,_typeStr){
}

bool DivOp::classof(const Node *node){
    return node->getType() == NodeType::DivOp;
}

ValueNode::ValueNode(NodeType type, const char *typeStr):Node(type, typeStr){}

bool ValueNode::classof(const Node *node){
    NodeType type = node->getType();
    return type == NodeType::Argument || type == NodeType::GlobalVar ||
            type==NodeType::ArgMemLoc || type == NodeType::GlobalVarMemLoc ||
            LiteralNode::classof(node);
}

ArgumentNode::ArgumentNode(const char *argName):ValueNode(NodeType::Argument, _typeStr),
    _argName(argName){
    _arg = nullptr;
}

ArgumentNode::ArgumentNode(const llvm::Argument *arg):ValueNode(NodeType::Argument, _typeStr),
    _arg(arg){
    _argName=std::to_string(_arg->getArgNo());
}

bool ArgumentNode::classof(const Node *node){
    return node->getType() == NodeType::Argument;
}

GlobalVarNode::GlobalVarNode(const char *gvName):ValueNode(NodeType::GlobalVar, _typeStr),
    _globalVarName(gvName){
}

GlobalVarNode::GlobalVarNode(const llvm::GlobalVariable *gv):ValueNode(NodeType::GlobalVar, _typeStr),
    _globalVar(gv){
    _globalVarName = gv->getName();
}

bool GlobalVarNode::classof(const Node *node){
    return node->getType() == NodeType::GlobalVar;
}

ArgumentMemLocNode::ArgumentMemLocNode(const char *argMemLocName):ValueNode(NodeType::ArgMemLoc, _typeStr),
    _argMemLocName(argMemLocName){
    _memLoc = NullComplexMemLocation;
}

ArgumentMemLocNode::ArgumentMemLocNode(ComplexMemLocation memLoc):ValueNode(NodeType::ArgMemLoc, _typeStr),
    _memLoc(memLoc){
    _argMemLocName = ::to_string(_memLoc);
}

bool ArgumentMemLocNode::classof(const Node *node){
    return node->getType() == NodeType::ArgMemLoc;
}

GlobalVarMemLocNode::GlobalVarMemLocNode(const char *gvMemLocName):ValueNode(NodeType::GlobalVarMemLoc, _typeStr),
    _gvMemLocName(gvMemLocName){
    _memLoc = NullComplexMemLocation;
}

GlobalVarMemLocNode::GlobalVarMemLocNode(ComplexMemLocation memLoc):ValueNode(NodeType::GlobalVarMemLoc, _typeStr),
    _memLoc(memLoc){
    _gvMemLocName = ::to_string(_memLoc);
}

bool GlobalVarMemLocNode::classof(const Node *node){
    return node->getType() == NodeType::GlobalVarMemLoc;
}

LiteralNode::LiteralNode(NodeType type, const char *typeStr):ValueNode (type, typeStr){
}

LiteralNode::LiteralNode(const ConstantData *cd, NodeType type, const char *typeStr):ValueNode (type, typeStr),_literal(cd){
    _literalStr = ConstantPrinter::to_string(cd);
}

bool LiteralNode::classof(const Node *node){
    return node->getType() == NodeType::LiteralBool ||
            node->getType() == NodeType::LiteralInt ||
            node->getType() == NodeType::LiteralFloat;
}

std::string LiteralNode::to_string() const {
    return _literalStr;
}

LiteralFloat::LiteralFloat(const ConstantFP *ci):LiteralNode(ci, NodeType::LiteralFloat, _typeStr){
    _literalFloat = ci->getValueAPF().convertToDouble();
}

LiteralFloat::LiteralFloat(double value):LiteralNode(NodeType::LiteralFloat, _typeStr), _literalFloat(value){
    _literalStr = std::to_string(value);
}

bool LiteralFloat::classof(const Node *node){
    return node->getType() == NodeType::LiteralFloat;
}

LiteralInt::LiteralInt(const ConstantInt *ci):LiteralNode(ci, NodeType::LiteralInt, _typeStr){
    assert(ci->getType()->getBitWidth() != 1);

    _literalInt = ci->getSExtValue();
}

LiteralInt::LiteralInt(int64_t value):LiteralNode(NodeType::LiteralInt, _typeStr), _literalInt(value){
    _literalStr = std::to_string(value);
}

bool LiteralInt::classof(const Node *node){
    return node->getType() == NodeType::LiteralInt;
}

LiteralBool::LiteralBool(const ConstantInt *ci):LiteralNode(ci, NodeType::LiteralBool, _typeStr){
    assert(ci->getType()->getBitWidth() == 1);

    _literalBool = ci->getZExtValue() != 0 ? true : false;
}

LiteralBool::LiteralBool(bool value):LiteralNode(NodeType::LiteralBool, _typeStr),_literalBool(value){
    _literalStr = value ? "1" :"0";
}

bool LiteralBool::classof(const Node *node){
    return node->getType() == NodeType::LiteralBool;
}


CustomFunc::CustomFunc():ExprNode(false, true, false,_funcNameStr, nullptr, NodeType::CustomFunc ,_typeStr){
    _customFuncName = "FUNC";
}

CustomFunc::CustomFunc(const char *funcName):ExprNode(false, true, false, _funcNameStr, nullptr, NodeType::CustomFunc ,_typeStr){
    _customFuncName = funcName;
}

void CustomFunc::setCustomFuncName(const char *name){
    _customFuncName = name;
}

std::string CustomFunc::getCustomFuncName() const {
    return _customFuncName;
}

std::string CustomFunc::to_string() const{
    std::string res;

    res = std::string(getCustomFuncName()) + "(";
    if(getOperandsCount()>0){
        if(_values.size()>0) res += _values[0]->to_string() + "=";
        res += getOperand(0)->to_string();
        for(unsigned i=1; i < getOperandsCount(); ++i){
            res += ", ";
            if(_values.size()>i) res += _values[i]->to_string() + "=";
            res+= getOperand(i)->to_string();
        }
    }
    res += ")";

    return res;
}

bool CustomFunc::classof(const Node *node){
    return node->getType() == NodeType::CustomFunc;
}

static const char FailString[] = "FAIL";
std::shared_ptr<AST::Node> AST::FailNode = std::make_shared<AST::UnknownNode>(FailString);


bool AST::isZero(std::shared_ptr<Node> val){
    if(auto boolVal = dynamic_cast<LiteralBool*>(val.get())){
        return !boolVal->getValue();
    }

    if(auto intVal = dynamic_cast<LiteralInt*>(val.get())){
        return intVal->getValue() == 0;
    }

    return false;
}

bool AST::isOne(std::shared_ptr<Node> val){
    if(auto boolVal = dynamic_cast<LiteralBool*>(val.get())){
        return boolVal->getValue();
    }

    if(auto intVal = dynamic_cast<LiteralInt*>(val.get())){
        return intVal->getValue() == 1;
    }

    return false;
}

std::shared_ptr<Node> AST::createSumOp(std::vector<std::shared_ptr<Node> > &values, std::shared_ptr<Node> defVal){

    if(values.size() == 0) return std::make_shared<LiteralInt>(0);

    std::vector<std::shared_ptr<Node>> vals(values);
    for(auto &val: vals){
        if(!val){
            val = defVal;
        }
    }

    std::vector<std::shared_ptr<Node>> nonZeroVals;

    std::copy_if(vals.begin(), vals.end(), std::back_inserter(nonZeroVals),
                 [](std::shared_ptr<Node>& val){return !isZero(val);});

    if(nonZeroVals.size()==0){
        return vals[0];
    }
    if(nonZeroVals.size()==1){
        return *nonZeroVals.begin();
    }

    auto res = std::make_shared<SumOp>();
    for(auto val: nonZeroVals) res->addOperand(val);
    return res;
}

std::shared_ptr<Node> AST::createSumOp(std::shared_ptr<Node> f, std::shared_ptr<Node> s, std::shared_ptr<Node> defVal){
    if(!f){
        f = defVal;
    }
    if(!s){
        s = defVal;
    }

    if (isZero(f) && isZero(s)){
        return std::make_shared<LiteralInt>(0);
    }

    if(isZero(f)) return s;
    else if(isZero(s)) return f;

    if(SumOp::classof(f.get()) && SumOp::classof(s.get())){
        auto res = std::make_shared<SumOp>();
        auto sumF = std::dynamic_pointer_cast<SumOp>(f);
        auto sumS = std::dynamic_pointer_cast<SumOp>(s);
        for(unsigned i = 0; i < sumF->getOperandsCount(); ++i){
            res->addOperand(sumF->getOperand(i));
        }
        for(unsigned i = 0; i < sumS->getOperandsCount(); ++i){
            res->addOperand(sumS->getOperand(i));
        }
        return res;
//    }else if(SumOp::classof(f.get()) || SumOp::classof(s.get())){
//        auto res = std::dynamic_pointer_cast<MulOp>(MulOp::classof(f.get()) ? f : s);
//        auto nonMul = MulOp::classof(f.get()) ? s : f;

//        res->addOperand(nonMul);
//        return res;
    }else{
        auto res = std::make_shared<SumOp>();
        res->addOperand(f);
        res->addOperand(s);
        return res;
    }

//    auto res = std::make_shared<SumOp>();
//    res->addOperand(f);
//    res->addOperand(s);
    //return res;
}

std::shared_ptr<Node> AST::createSumOp(std::shared_ptr<Node> f, std::shared_ptr<Node> s, std::shared_ptr<Node> t, std::shared_ptr<Node> defVal){
    std::vector<std::shared_ptr<Node>> vals({f,s,t});
    return createSumOp(vals, defVal);
}

std::shared_ptr<Node> AST::createOrOp(std::vector<std::shared_ptr<Node> > &values, std::shared_ptr<Node> defVal){

    if(values.size() == 0) return std::make_shared<LiteralBool>(false);

    std::vector<std::shared_ptr<Node>> vals(values);
    for(auto &val: vals){
        if(!val){
            val = defVal;
        }
    }

    std::vector<std::shared_ptr<Node>> nonFalseVals;

    std::copy_if(vals.begin(), vals.end(), std::back_inserter(nonFalseVals),
                 [](std::shared_ptr<Node>& val){return !isZero(val);});

    if(nonFalseVals.size()==0){
        return vals[0];
    }
    if(nonFalseVals.size()==1){
        return *nonFalseVals.begin();
    }

    auto res = std::make_shared<Or>();
    for(auto val: nonFalseVals) res->addOperand(val);
    return res;
}

std::shared_ptr<Node> AST::createOrOp(std::shared_ptr<Node> f, std::shared_ptr<Node> s, std::shared_ptr<Node> defVal){
    if(!f){
        f = defVal;
    }
    if(!s){
        s = defVal;
    }

    if (isZero(f) && isZero(s)){
        return std::make_shared<LiteralBool>(false);
    }

    if(isZero(f)) return s;
    else if(isZero(s)) return f;

    auto res = std::make_shared<Or>();
    res->addOperand(f);
    res->addOperand(s);
    return res;
}

std::shared_ptr<Node> AST::createOrOp(std::shared_ptr<Node> f, std::shared_ptr<Node> s, std::shared_ptr<Node> t, std::shared_ptr<Node> defVal){
    std::vector<std::shared_ptr<Node>> vals({f,s,t});
    return createOrOp(vals, defVal);
}

std::shared_ptr<Node> AST::createMulOp(std::vector<std::shared_ptr<Node> > &values, std::shared_ptr<Node> defVal){

    if(values.size() == 0) return std::make_shared<LiteralInt>(0);

    std::vector<std::shared_ptr<Node>> vals(values);
    for(auto &val: vals){
        if(!val){
            val = defVal;
        }
    }

    for(auto val: vals){
        if(isZero(val)){
            return std::make_shared<LiteralInt>(0);
        }
    }

    std::vector<std::shared_ptr<Node>> nonOneVals;

    std::copy_if(vals.begin(), vals.end(), std::back_inserter(nonOneVals),
                 [](std::shared_ptr<Node>& val){return !isOne(val);});

    if(nonOneVals.size()==0){
        return vals[0];
    }
    if(nonOneVals.size()==1){
        return *nonOneVals.begin();
    }

    auto res = std::make_shared<MulOp>();
    for(auto val: nonOneVals) res->addOperand(val);
    return res;
}

std::shared_ptr<Node> AST::createMulOp(std::shared_ptr<Node> f, std::shared_ptr<Node> s, std::shared_ptr<Node> defVal){
    if(!f){
        f = defVal;
    }
    if(!s){
        s = defVal;
    }

    if (isZero(f) || isZero(s)){
        return std::make_shared<LiteralInt>(0);
    }

    if(isOne(f)) return s;
    else if(isOne(s)) return f;


    if(MulOp::classof(f.get()) && MulOp::classof(s.get())){
        auto res = std::make_shared<MulOp>();
        auto mulF = std::dynamic_pointer_cast<MulOp>(f);
        auto mulS = std::dynamic_pointer_cast<MulOp>(s);
        for(unsigned i = 0; i < mulF->getOperandsCount(); ++i){
            res->addOperand(mulF->getOperand(i));
        }
        for(unsigned i = 0; i < mulS->getOperandsCount(); ++i){
            res->addOperand(mulS->getOperand(i));
        }
        return res;
    }else if(MulOp::classof(f.get()) || MulOp::classof(s.get())){
        auto res = std::dynamic_pointer_cast<MulOp>(MulOp::classof(f.get()) ? f : s);
        auto nonMul = MulOp::classof(f.get()) ? s : f;

        res->addOperand(nonMul);
        return res;
    }else{
        auto res = std::make_shared<MulOp>();
        res->addOperand(f);
        res->addOperand(s);
        return res;
    }
}

std::shared_ptr<Node> AST::createMulOp(std::shared_ptr<Node> f, std::shared_ptr<Node> s, std::shared_ptr<Node> t, std::shared_ptr<Node> defVal){
    std::vector<std::shared_ptr<Node>> vals({f,s,t});
    return createMulOp(vals, defVal);
}

std::shared_ptr<Node> AST::createAndOp(std::vector<std::shared_ptr<Node> > &values, std::shared_ptr<Node> defVal){

    if(values.size() == 0) return std::make_shared<LiteralBool>(false);

    std::vector<std::shared_ptr<Node>> vals(values);
    for(auto &val: vals){
        if(!val){
            val = defVal;
        }
    }

    for(auto val: vals){
        if(isZero(val)){
            return std::make_shared<LiteralBool>(false);
        }
    }

    std::vector<std::shared_ptr<Node>> nonTrueVals;

    std::copy_if(vals.begin(), vals.end(), std::back_inserter(nonTrueVals),
                 [](std::shared_ptr<Node>& val){return !isOne(val);});

    if(nonTrueVals.size()==0){
        return vals[0];
    }
    if(nonTrueVals.size()==1){
        return *nonTrueVals.begin();
    }

    auto res = std::make_shared<And>();
    for(auto val: nonTrueVals) res->addOperand(val);
    return res;
}

std::shared_ptr<Node> AST::createAndOp(std::shared_ptr<Node> f, std::shared_ptr<Node> s, std::shared_ptr<Node> defVal){
    if(!f){
        f = defVal;
    }
    if(!s){
        s = defVal;
    }

    if (isZero(f) || isZero(s)){
        return std::make_shared<LiteralBool>(false);
    }

    if(isOne(f)) return s;
    else if(isOne(s)) return f;

    auto res = std::make_shared<And>();
    res->addOperand(f);
    res->addOperand(s);
    return res;
}

std::shared_ptr<Node> AST::createAndOp(std::shared_ptr<Node> f, std::shared_ptr<Node> s, std::shared_ptr<Node> t, std::shared_ptr<Node> defVal){
    std::vector<std::shared_ptr<Node>> vals({f,s,t});
    return createAndOp(vals, defVal);
}

std::shared_ptr<Node> AST::createNotOp(std::shared_ptr<Node> f){
    if(isOne(f)) return std::make_shared<AST::LiteralBool>(false);
    if(isZero(f)) return std::make_shared<AST::LiteralBool>(true);

    if(AST::Not::classof(f.get())){
        auto notNode = std::dynamic_pointer_cast<AST::Not>(f);
        if(notNode->getOperandsCount() == 1){
            return notNode->getOperand(0);
        }
    }

    auto notOp = std::make_shared<AST::Not>();
    notOp->addOperand(f);
    return notOp;
}


