#include "criticalparamtracer.h"

#include "easylogging++.h"

using namespace std;
using namespace llvm;

CriticalParamTracer::CriticalParamTracer()
{

}

void CriticalParamTracer::getFuncCritParams(const Function *func, std::set<Value *> &vals, std::set<ComplexMemLocation> &memLocs){
    calcCritParams(func);

    auto fiIt = funcInfo.find(func);
    assert(fiIt != funcInfo.end());

    auto& critVals = fiIt->second.critVals;
    for(auto val : critVals) vals.insert(const_cast<Value*>(val));

    auto& critMls = fiIt->second.critMemLocs;
    std::copy(critMls.begin(), critMls.end(), std::inserter(memLocs, memLocs.begin()));
}

void CriticalParamTracer::getFuncCritParams(const Function *func, std::set<const Value *> &vals, std::set<ComplexMemLocation> &memLocs){
    calcCritParams(func);

    auto fiIt = funcInfo.find(func);
    assert(fiIt != funcInfo.end());

    auto& critVals = fiIt->second.critVals;
    std::copy(critVals.begin(), critVals.end(), std::inserter(vals, vals.begin()));

    auto& critMls = fiIt->second.critMemLocs;
    std::copy(critMls.begin(), critMls.end(), std::inserter(memLocs, memLocs.begin()));
}

void CriticalParamTracer::calcCritParams(const Function *func){
    LOG(DEBUG)<<"Start to calculate critical parameters of "<<func->getName().data()<<" function";

    auto funcInfoIt = funcInfo.find(func);
    if(funcInfoIt == funcInfo.end()){
        calculatePrioriCritParams(func);
        funcInfoIt = funcInfo.find(func);
    }

    // Iterate through critical calls.
    for(auto call:funcInfoIt->second.critCalls){
        const llvm::Function* callFunc = call->getCalledFunction();

        auto callFuncIt = funcInfo.find(callFunc);

        if(callsCache.find(call) != callsCache.end() && callsCache[call] == getFuncGist(callFuncIt->second)) continue;

        if(callFuncIt != funcInfo.end()){
            traceCallCritValues(call);
            callsCache[call] = getFuncGist(callFuncIt->second);
        }else{
            callsCache[call] = 0;
        }

        calcCritParams(callFunc);

        callFuncIt = funcInfo.find(callFunc);
        if(callsCache[call] != getFuncGist(callFuncIt->second)){
            traceCallCritValues(call);
            callsCache[call] = getFuncGist(callFuncIt->second);
        }
    }

    LOG(DEBUG)<<"Finished to calculate critical parameters of "<<func->getName().data()<<" function";
}

static std::set<BasicBlock *> findExitBlocks(const Function *func){
    std::set<BasicBlock*> exitBlocks;
    for(auto& bb : *func){
        if(dyn_cast<ReturnInst>(bb.getTerminator())){
            exitBlocks.insert(const_cast<llvm::BasicBlock*>(&bb));
        }
    }
    return exitBlocks;
}

void CriticalParamTracer::findControlDecisionVals(const std::set<BasicBlock *> &bbs, std::set<Value *> &controlDecisionVals){
    // Find nearest common dominator
    auto it = bbs.begin();
    auto func = (*it)->getParent();
    auto ncd = *it;

    auto dt = getDominatorTree(func);

    ++it;
    while(it != bbs.end()){
        ncd = dt->findNearestCommonDominator(ncd, *it);
        ++it;
    }

    // Build subgraph from ncd to all exits
    using FTCC = FunctionToCfgConverter<true>;
    using CfgType = Cfg<const BasicBlock*>;

    FTCC::Mapping orig2cfg;
    CfgType cfg = FTCC().convert(func, &orig2cfg);

    std::set<const CfgType::Block*> stopBlocks;
    for(auto& bb : bbs) stopBlocks.insert(orig2cfg[bb]);

    CfgMapping<CfgType::Block> cfg2rfcfg;
    CfgType scfg = getReachableFromSubgraph(*orig2cfg[ncd], stopBlocks, &cfg2rfcfg);

    // Find control decision values
    auto ncdS = cfg2rfcfg.left.at(orig2cfg[ncd]);
    for(BasicBlock* bb : bbs){
        auto pb = cfg2rfcfg.left.at(orig2cfg[bb]);
        findPathDecisionValues(ncdS, pb, controlDecisionVals);
    }
}

void CriticalParamTracer::updateFuncCritParams(const Function *func){
    auto ebb = &func->getEntryBlock();
    auto fiIt = funcInfo.find(func);
    assert(fiIt != funcInfo.end());

    auto& fi = fiIt->second;

    for(auto val : fi.depVals[ebb]){
        if(dyn_cast<Argument>(val)){
            fi.critVals.insert(val);
        } else if(dyn_cast<GlobalVariable>(val)){
            fi.critVals.insert(val);
        } else{
            string valStr;
            raw_string_ostream rso(valStr);val->print(rso);
            LOG(WARNING)<<"Can't set value "<<valStr<<" as "<<func->getName().data()<<" critical value";
        }
    }

    for(auto ml : fi.depMemLocs[ebb]){
        if(dyn_cast<Argument>(ml.base)){
            fi.critMemLocs.insert(ml);
        }else if(dyn_cast<GlobalVariable>(ml.base)){
            fi.critMemLocs.insert(ml);
        }else{
            LOG(WARNING)<<"Can't set memory location "<<ml<<" as "<<func->getName().data()<<" critical memory location";
        }
    }
}

void CriticalParamTracer::calculatePrioriCritParams(const Function *func){

    LOG(DEBUG)<<"Start to calculate priori critical parameters of "<<func->getName().data()<<" function";

    auto fiIt = funcInfo.find(func);

    if(fiIt == funcInfo.end()){
        funcInfo.emplace(make_pair(func, FuncInfo(func)));
        fiIt = funcInfo.find(func);
    }

    FuncInfo& fi = fiIt->second;

    std::set<BasicBlock*> exitBlocks = findExitBlocks(func);
    assert(exitBlocks.size()>0);

    // If there are several exit block then we have to trace variables
    // that defines which exit will we come to.
    if(exitBlocks.size() > 1){
        std::set<Value*> cdv;
        findControlDecisionVals(exitBlocks, cdv);

        for(auto v : cdv){
            if(auto inst = dyn_cast<Instruction>(v)){
                traceValue(inst, inst->getParent(), fi.depVals, fi.depMemLocs);
            }else{
                traceValue(v, const_cast<llvm::BasicBlock*>(&func->getEntryBlock()), fi.depVals, fi.depMemLocs);
            }
        }
    }

    for(auto eb : exitBlocks){
        auto retInst = dyn_cast<llvm::ReturnInst>(eb->getTerminator());
        assert(retInst->getNumOperands() == 1);

        traceValue(retInst->getOperand(0), eb, fi.depVals, fi.depMemLocs);
    }

    updateFuncCritParams(func);

    LOG(DEBUG)<<"Finished to calculate priori critical parameters of "<<func->getName().data()<<" function";
}

void CriticalParamTracer::traceCallInst(CallInst *callInst, BasicBlock *curBb, map<const BasicBlock *, set<Value *> > &bbDepedentVals, map<const BasicBlock *, set<ComplexMemLocation> > &bbDependentLocs){
    auto func = curBb->getParent();

    auto fiIt = funcInfo.find(func);
    assert(fiIt != funcInfo.end());

    auto& fInfo = fiIt->second;

    auto callInstIt = std::find(fInfo.critCalls.begin(), fInfo.critCalls.end(), callInst);

    if(callInstIt == fInfo.critCalls.end()){
        fInfo.critCalls.push_back(callInst);
    }
}

void CriticalParamTracer::traceCallCritValues(const CallInst *callInst){
    auto calleeFunc = callInst->getCalledFunction();
    auto& calleeFuncInfo = funcInfo.find(calleeFunc)->second;

    auto callerFunc = callInst->getFunction();
    auto& callerFuncInfo = funcInfo.find(callerFunc)->second;

    std::set<const Value*> callCritVals;
    std::set<ComplexMemLocation> callCritMemLocs;

    translateCallCritVals(callInst, calleeFuncInfo.critVals, callCritVals, callCritMemLocs);
    translateCallCritMemLocs(callInst, calleeFuncInfo.critMemLocs, callCritVals, callCritMemLocs);

    auto callBb = callInst->getParent();
    for(auto val : callCritVals){
        traceValue(val, callBb, callerFuncInfo.depVals, callerFuncInfo.depMemLocs);
    }

    for(auto memLoc : callCritMemLocs){
        AttachedComplexMemLocation acml;
        *static_cast<ComplexMemLocation*>(&acml) = memLoc;
        acml.inst = callInst;
        acml.beforeInst = true;
        traceMemLocationBeforeInstr(acml, callInst, callerFuncInfo.depVals, callerFuncInfo.depMemLocs);
    }

    updateFuncCritParams(callerFunc);
}

DominatorTree *CriticalParamTracer::getDominatorTree(const Function *f){
    return &funcInfo.find(f)->second.dt;
}

unsigned CriticalParamTracer::getFuncGist(const CriticalParamTracer::FuncInfo &fi){
    return fi.critVals.size() + fi.critMemLocs.size();
}

