#include "funcstatisticanalyzer.h"

#include "valuetoexpressionconverter.h"

#include "criticalparamanalyzer.h"

#include "customcriticalparamtracer.h"

using namespace llvm;

//template<class T, class F>
//static std::shared_ptr<AST::Node> getCallExpr(const llvm::CallInst* call,
//                                              const ValueToExpressionConverter& vtec,
//                                              const T& criteria, F nameGenerator){
//    CriticalParamAnalyzer<CriteriaType> cpa(call->getCalledFunction(), criteria);
//    FuncStatisticAnalyzer::ValueVector critParams = cpa.getCriticalParams2();
//    std::sort(critParams.begin(), critParams.end(), FuncStatisticAnalyzer::criticalParamSort);
//    auto callExpr = std::make_shared<AST::CustomFunc>(
//                nameGenerator(call->getCalledFunction()).c_str());

//    for(auto cp: critParams){
//        if(AST::ArgumentNode::classof(cp.get())){
//            auto arg = std::dynamic_pointer_cast<AST::ArgumentNode>(cp);
//            callExpr->addOperand(vtec.getValueExpr(call->getOperand(arg->getIrArgument()->getArgNo())));
//            callExpr->setOperandValueNode(callExpr->getOperandsCount()-1, cp);
//        }
//        if(AST::GlobalVarNode::classof(cp.get())){
//            auto gv = std::dynamic_pointer_cast<AST::GlobalVarNode>(cp);
//            callExpr->addOperand(gv);
//            callExpr->setOperandValueNode(callExpr->getOperandsCount()-1, cp);
//        }
//    }
//    return callExpr;
//}

static std::shared_ptr<AST::ValueNode> castValueToAstNode(const llvm::Value* val){
    const llvm::Argument* arg;
    const llvm::GlobalVariable *gv;
    if( (arg = llvm::dyn_cast<llvm::Argument>(val)) ){
        return std::shared_ptr<AST::ValueNode>(new AST::ArgumentNode(arg));
    }else if( (gv = llvm::dyn_cast<const llvm::GlobalVariable>(val)) ){
        return std::shared_ptr<AST::ValueNode>(new AST::GlobalVarNode(gv));
    }
    return std::shared_ptr<AST::ValueNode>();
}

static std::shared_ptr<AST::ValueNode> castMemLocToAstNode(const ComplexMemLocation& cml){
    if(dyn_cast<Argument>(cml.base)){
        return std::shared_ptr<AST::ValueNode>(new AST::ArgumentMemLocNode(cml));
    }else if(dyn_cast<GlobalVariable>(cml.base)){
        return std::shared_ptr<AST::ValueNode>(new AST::GlobalVarMemLocNode(cml));
    }
    return std::shared_ptr<AST::ValueNode>();
}

template<class T, class F>
static std::shared_ptr<AST::Node> getCallExpr(const llvm::CallInst* call,
                                              const ValueToExpressionConverter& vtec,
                                              T criteria, F nameGenerator){
    SimpleInstCounterCriteria<T> scc(criteria);
    CustomCriticalParamTracer ccpt(scc);
    std::set<const llvm::Value*> critVals;
    std::set<ComplexMemLocation> critMemLocs;
    ccpt.getFuncCritParams(call->getCalledFunction(), critVals, critMemLocs);

    std::vector<std::shared_ptr<AST::ValueNode>> critParams;
    for(auto val : critVals) critParams.push_back(castValueToAstNode(val));
    for(auto& cml : critMemLocs) critParams.push_back(castMemLocToAstNode(cml));

    //CriticalParamAnalyzer<CriteriaType> cpa(call->getCalledFunction(), criteria);
    //FuncStatisticAnalyzer::ValueVector critParams = cpa.getCriticalParams2();
    std::sort(critParams.begin(), critParams.end(), FuncStatisticAnalyzer::criticalParamSort);
    auto callExpr = std::make_shared<AST::CustomFunc>(
                nameGenerator(call->getCalledFunction()).c_str());

    for(auto cp: critParams){
        if(AST::ArgumentNode::classof(cp.get())){
            auto arg = std::dynamic_pointer_cast<AST::ArgumentNode>(cp);
            callExpr->addOperand(vtec.valueToExpr(call->getOperand(arg->getIrArgument()->getArgNo())));
            callExpr->setOperandValueNode(callExpr->getOperandsCount()-1, cp);
        }
        if(AST::GlobalVarNode::classof(cp.get())){
            auto gv = std::dynamic_pointer_cast<AST::GlobalVarNode>(cp);
            callExpr->addOperand(gv);
            callExpr->setOperandValueNode(callExpr->getOperandsCount()-1, cp);
        }
        if(AST::ArgumentMemLocNode::classof(cp.get())){
            auto argMem = std::dynamic_pointer_cast<AST::ArgumentMemLocNode>(cp);
            critVals.clear();
            critMemLocs.clear();
            translateCallCritMemLocs(call, {argMem->getMemLoc()}, critVals, critMemLocs);
            //callExpr->addOperand(vtec.getValueExpr(call->getOperand(arg->getIrArgument()->getArgNo())));
            AttachedComplexMemLocation acml(*critMemLocs.begin(), call, true);
            callExpr->addOperand(vtec.memLocToExpr(acml));
            callExpr->setOperandValueNode(callExpr->getOperandsCount()-1, cp);
        }
    }
    return callExpr;
}

typedef bool (*CriteriaType2)(const llvm::Instruction& inst, std::vector<const llvm::Value*>*);

template<class T, class F>
static std::shared_ptr<AST::Node> getCallExpr2(const llvm::CallInst* call,
                                              const ValueToExpressionConverter& vtec,
                                              const T& criteria, F nameGenerator){
    CriticalParamAnalyzer2<CriteriaType2> cpa(call->getCalledFunction(), criteria);
    FuncStatisticAnalyzer::ValueVector critParams = cpa.getCriticalParams2();
    std::sort(critParams.begin(), critParams.end(), FuncStatisticAnalyzer::criticalParamSort);
    auto callExpr = std::make_shared<AST::CustomFunc>(
                nameGenerator(call->getCalledFunction()).c_str());

    for(auto cp: critParams){
        if(AST::ArgumentNode::classof(cp.get())){
            auto arg = std::dynamic_pointer_cast<AST::ArgumentNode>(cp);
            callExpr->addOperand(vtec.valueToExpr(call->getOperand(arg->getIrArgument()->getArgNo())));
            callExpr->setOperandValueNode(callExpr->getOperandsCount()-1, cp);
        }
        if(AST::GlobalVarNode::classof(cp.get())){
            auto gv = std::dynamic_pointer_cast<AST::GlobalVarNode>(cp);
            callExpr->addOperand(gv);
            callExpr->setOperandValueNode(callExpr->getOperandsCount()-1, cp);
        }
    }
    return callExpr;
}

FuncStatisticAnalyzer::FuncStatisticAnalyzer(llvm::Function *func):_btc(func), _bte(func), _func(func),
    _tli(_tlii),_ac(*func),_dt(*func),_li(_dt),_se(*func, _tli, _ac, _dt, _li), _vtec(func, _se){
    countAllInstructions();
}

static std::string createCustomFlopName(const llvm::Function* f){
    std::string name = std::string(f->getName().data()) + ".flops";
    return name;
}

std::shared_ptr<AST::Node> FuncStatisticAnalyzer::getFlopExpr(){
    std::shared_ptr<AST::Node> expr = std::make_shared<AST::LiteralInt>(0);

    std::map<const llvm::BasicBlock*, OccurenceInfo<const llvm::Instruction*>> flops(_flops4.begin(), _flops4.end());
    for(auto it: _flops8){
        if(flops.find(it.first) != flops.end()){
            flops[it.first].instances.insert(it.second.instances.begin(), it.second.instances.end());
            flops[it.first].significantCalls.insert(it.second.significantCalls.begin(), it.second.significantCalls.end());
        }else{
            flops[it.first] = it.second;
        }
    }

    for(auto topCount:flops){
        std::shared_ptr<AST::Node> bbExpr = std::make_shared<AST::LiteralInt>(0);
        for(auto call:topCount.second.significantCalls){
            bbExpr = AST::createSumOp(bbExpr, getCallExpr(call, _vtec, isFloatBinaryInst, createCustomFlopName));
        }
        if(topCount.second.instances.size()){
            bbExpr = AST::createSumOp(bbExpr, std::make_shared<AST::LiteralInt>(topCount.second.instances.size()));
        }
        bbExpr = AST::createMulOp(_bte.getBasicBlockTripCount(topCount.first), bbExpr);
        expr = AST::createSumOp(expr, bbExpr);
    }
    return expr;
}

static std::string createCustomFlop4Name(const llvm::Function* f){
    std::string name = std::string(f->getName().data()) + ".flops4";
    return name;
}

std::shared_ptr<AST::Node> FuncStatisticAnalyzer::getFlop4Expr(){
    std::shared_ptr<AST::Node> expr = std::make_shared<AST::LiteralInt>(0);
    for(auto topCount:_flops4){
        std::shared_ptr<AST::Node> bbExpr = std::make_shared<AST::LiteralInt>(0);
        for(auto call:topCount.second.significantCalls){
            bbExpr = AST::createSumOp(bbExpr, getCallExpr(call, _vtec, isFloat4BinaryInst, createCustomFlop4Name));
        }
        if(topCount.second.instances.size()){
            bbExpr = AST::createSumOp(bbExpr, std::make_shared<AST::LiteralInt>(topCount.second.instances.size()));
        }
        bbExpr = AST::createMulOp(_bte.getBasicBlockTripCount(topCount.first), bbExpr);
        expr = AST::createSumOp(expr, bbExpr);
    }
    return expr;
}

static std::string createCustomFlop8Name(const llvm::Function* f){
    std::string name = std::string(f->getName().data()) + ".flops8";
    return name;
}

std::shared_ptr<AST::Node> FuncStatisticAnalyzer::getFlop8Expr(){
    std::shared_ptr<AST::Node> expr = std::make_shared<AST::LiteralInt>(0);

    for(auto topCount:_flops8){
        std::shared_ptr<AST::Node> bbExpr = std::make_shared<AST::LiteralInt>(0);
        for(auto call:topCount.second.significantCalls){
            bbExpr = AST::createSumOp(bbExpr, getCallExpr(call, _vtec, isFloat8BinaryInst, createCustomFlop8Name));
        }
        if(topCount.second.instances.size()){
            bbExpr = AST::createSumOp(bbExpr, std::make_shared<AST::LiteralInt>(topCount.second.instances.size()));
        }
        bbExpr = AST::createMulOp(_bte.getBasicBlockTripCount(topCount.first), bbExpr);
        expr = AST::createSumOp(expr, bbExpr);
    }
    return expr;
}

static std::string createCustomWriteAccessName(const llvm::Function* f){
    std::string name = std::string(f->getName().data()) + ".read_accesses";
    return name;
}

static std::string createCustomReadAccessName(const llvm::Function* f){
    std::string name = std::string(f->getName().data()) + ".write_accesses";
    return name;
}

static std::string createCustomStoreName(const llvm::Function* f){
    std::string name = std::string(f->getName().data()) + ".stores";
    return name;
}

static std::string createCustomLoadName(const llvm::Function* f){
    std::string name = std::string(f->getName().data()) + ".loads";
    return name;
}


std::shared_ptr<AST::Node> FuncStatisticAnalyzer::getWriteAccessExpr(){
    std::shared_ptr<AST::Node> expr = std::make_shared<AST::LiteralInt>(0);
    for(auto topCount:_stores){
        std::shared_ptr<AST::Node> bbExpr = std::make_shared<AST::LiteralInt>(0);
        for(auto call:topCount.second.significantCalls){
            bbExpr = AST::createSumOp(bbExpr, getCallExpr(call, _vtec, isStoreInst, createCustomWriteAccessName));
        }
        if(topCount.second.instances.size()){
            bbExpr = AST::createSumOp(bbExpr, std::make_shared<AST::LiteralInt>(topCount.second.instances.size()));
        }
        bbExpr = AST::createMulOp(_bte.getBasicBlockTripCount(topCount.first), bbExpr);
        expr = AST::createSumOp(expr, bbExpr);
    }
    return expr;
}


std::shared_ptr<AST::Node> FuncStatisticAnalyzer::getReadAccessExpr(){
    std::shared_ptr<AST::Node> expr = std::make_shared<AST::LiteralInt>(0);
    for(auto topCount:_loads){
        std::shared_ptr<AST::Node> bbExpr = std::make_shared<AST::LiteralInt>(0);
        for(auto call:topCount.second.significantCalls){
            bbExpr = AST::createSumOp(bbExpr, getCallExpr(call, _vtec, isLoadInst, createCustomReadAccessName));
        }
        if(topCount.second.instances.size()){
            bbExpr = AST::createSumOp(bbExpr, std::make_shared<AST::LiteralInt>(topCount.second.instances.size()));
        }
        bbExpr = AST::createMulOp(_bte.getBasicBlockTripCount(topCount.first), bbExpr);
        expr = AST::createSumOp(expr, bbExpr);
    }
    return expr;
}

std::shared_ptr<AST::Node> FuncStatisticAnalyzer::getStoreSizeExpr(){
    std::shared_ptr<AST::Node> expr = std::make_shared<AST::LiteralInt>(0);
    for(auto topCount:_stores){
        std::shared_ptr<AST::Node> bbExpr = std::make_shared<AST::LiteralInt>(0);
        for(auto call:topCount.second.significantCalls){
            bbExpr = AST::createSumOp(bbExpr, getCallExpr(call, _vtec, isStoreInst, createCustomStoreName));
        }
        //Split instances by size
        std::multimap<unsigned, const llvm::StoreInst*> sizeMap;
        for(auto instance : topCount.second.instances){
            sizeMap.insert(std::make_pair(instance->getOperand(0)->getType()->getScalarSizeInBits()/8, instance));
        }

        for(auto sizeInstances: sizeMap){
            auto sizeExpr = std::make_shared<AST::LiteralInt>(
                        sizeInstances.first * sizeMap.count(sizeInstances.first));
            bbExpr = AST::createSumOp(bbExpr, sizeExpr);
        }

        bbExpr = AST::createMulOp(_bte.getBasicBlockTripCount(topCount.first), bbExpr);
        expr = AST::createSumOp(expr, bbExpr);
    }
    return expr;
}

std::shared_ptr<AST::Node> FuncStatisticAnalyzer::getLoadSizeExpr(){
    std::shared_ptr<AST::Node> expr = std::make_shared<AST::LiteralInt>(0);
    for(auto topCount:_loads){
        std::shared_ptr<AST::Node> bbExpr = std::make_shared<AST::LiteralInt>(0);
        for(auto call:topCount.second.significantCalls){
            bbExpr = AST::createSumOp(bbExpr, getCallExpr(call, _vtec, isStoreInst, createCustomStoreName));
        }
        //Split instances by size
        std::multimap<unsigned, const llvm::LoadInst*> sizeMap;
        for(auto instance : topCount.second.instances){
            sizeMap.insert(std::make_pair(instance->getType()->getScalarSizeInBits()/8, instance));
        }

        for(auto sizeInstances: sizeMap){
            auto sizeExpr = std::make_shared<AST::LiteralInt>(
                        sizeInstances.first * sizeMap.count(sizeInstances.first));
            bbExpr = AST::createSumOp(bbExpr, sizeExpr);
        }

        bbExpr = AST::createMulOp(_bte.getBasicBlockTripCount(topCount.first), bbExpr);
        expr = AST::createSumOp(expr, bbExpr);
    }
    return expr;
}

struct MathNameGenerator{
    MathFuncType _type;
    MathNameGenerator(MathFuncType type):_type(type){
    }

    std::string operator() (const llvm::Function* f){
        return std::string(f->getName().data()) + std::string(".") + getMathFuncName(_type);
    }
};

std::shared_ptr<AST::Node> FuncStatisticAnalyzer::getMathFuncExpr(MathFuncType type){
    std::shared_ptr<AST::Node> expr = std::make_shared<AST::LiteralInt>(0);
    for(auto topCount:_mathCalls){
        std::shared_ptr<AST::Node> bbExpr = std::make_shared<AST::LiteralInt>(0);
        if(topCount.second.find(type)!=topCount.second.end()){
            for(auto call:topCount.second[type].significantCalls){
                MathNameGenerator gen(type);
                bbExpr = AST::createSumOp(bbExpr, getCallExpr(call, _vtec, getMathFuncCriteria(type), gen));
            }

            if(auto count = topCount.second[type].instances.size()){
                bbExpr = AST::createSumOp(bbExpr, std::make_shared<AST::LiteralInt>(count));
            }
        }


        bbExpr = AST::createMulOp(_bte.getBasicBlockTripCount(topCount.first), bbExpr);
        expr = AST::createSumOp(expr, bbExpr);
    }
    return expr;
}

struct MpiNameGenerator{
    MpiFuncType _type;
    MpiNameGenerator(MpiFuncType type):_type(type){
    }

    std::string operator() (const llvm::Function* f){
        return std::string(f->getName().data()) + std::string(".") + getMpiFuncName(_type);
    }
};

std::shared_ptr<AST::Node> FuncStatisticAnalyzer::getMpiFuncExpr(MpiFuncType type){
    std::shared_ptr<AST::Node> expr = std::make_shared<AST::LiteralInt>(0);
    for(auto topCount:_mpiCalls){
        std::shared_ptr<AST::Node> bbExpr = std::make_shared<AST::LiteralInt>(0);
        if(topCount.second.find(type)!=topCount.second.end()){
            for(auto call:topCount.second[type].significantCalls){
                MpiNameGenerator gen(type);
                bbExpr = AST::createSumOp(bbExpr, getCallExpr(call, _vtec, getMpiFuncCriteria(type), gen));
            }

            if(auto count = topCount.second[type].instances.size()){
                bbExpr = AST::createSumOp(bbExpr, std::make_shared<AST::LiteralInt>(count));
            }
        }


        bbExpr = AST::createMulOp(_bte.getBasicBlockTripCount(topCount.first), bbExpr);
        expr = AST::createSumOp(expr, bbExpr);
    }
    return expr;
}

bool sendDataCriteria(const llvm::Instruction& call, std::vector<const llvm::Value*>* out){
    if(isMpiFuncSendData(call)){
        if(out){
            if(isMpiSend(call) || isMpiIsend(call) || isMpiSendRecv(call)){
                out->push_back(call.getOperand(1));
            }
            #pragma message "implement account for variable message type"
        }
        return true;
    }
    return false;
}

bool recvDataCriteria(const llvm::Instruction& call, std::vector<const llvm::Value*>* out){
    if(isMpiFuncSendData(call)){
        if(out){
            if(isMpiRecv(call) || isMpiIrecv(call)){
                out->push_back(call.getOperand(1));
            }
            if(isMpiSendRecv(call)){
                out->push_back(call.getOperand(6));
            }
            #pragma message "implement account for changeable message type"
        }
        return true;
    }
    return false;
}

static std::string createCustomSendBytesName(const llvm::Function* f){
    std::string name = std::string(f->getName().data()) + ".sendBytes";
    return name;
}

static std::string createCustomRecvBytesName(const llvm::Function* f){
    std::string name = std::string(f->getName().data()) + ".recvBytes";
    return name;
}

int mpiTypeToSize(const llvm::Value& val){
    auto expr = llvm::dyn_cast<const llvm::ConstantExpr>(&val);
    std::string mpiTypeString = expr->getOperand(0)->getName();
    if(mpiTypeString.find("double") != std::string::npos){
        return 8;
    }
    if(mpiTypeString.find("int") != std::string::npos){
        return 4;
    }
    #pragma message "implement account for changeable message type"
}

std::shared_ptr<AST::Node> getMpiSendTypeExpr(const llvm::CallInst& callInst){
    std::shared_ptr<AST::Node> expr = std::make_shared<AST::LiteralInt>(0);
    if(isMpiFuncSendData(callInst)){
        if(isMpiSend(callInst) || isMpiIsend(callInst) || isMpiSendRecv(callInst)){
            expr = std::make_shared<AST::LiteralInt>(mpiTypeToSize(*callInst.getOperand(2)));
            #pragma message "implement account for changeable message type"
        }
    }
    return expr;
}

std::shared_ptr<AST::Node> getMpiRecvTypeExpr(const llvm::CallInst& callInst){
    std::shared_ptr<AST::Node> expr = std::make_shared<AST::LiteralInt>(0);
    if(isMpiFuncSendData(callInst)){
        if(isMpiSend(callInst) || isMpiIsend(callInst)){
            expr = std::make_shared<AST::LiteralInt>(mpiTypeToSize(*callInst.getOperand(2)));

        }
        if(isMpiSendRecv(callInst)){
            expr = std::make_shared<AST::LiteralInt>(mpiTypeToSize(*callInst.getOperand(7)));
        }
        #pragma message "implement account for changeable message type"
    }
    return expr;
}

std::shared_ptr<AST::Node> getMpiSendCountExpr(const llvm::CallInst& callInst,
                                           const ValueToExpressionConverter& vtec){
    std::shared_ptr<AST::Node> expr = std::make_shared<AST::LiteralInt>(0);
    if(isMpiFuncSendData(callInst)){
        if(isMpiSend(callInst) || isMpiIsend(callInst) || isMpiSendRecv(callInst)){
            expr = vtec.valueToExpr(callInst.getOperand(1));
        }
    }
    return expr;
}

std::shared_ptr<AST::Node> getMpiRecvCountExpr(const llvm::CallInst& callInst,
                                           const ValueToExpressionConverter& vtec){
    std::shared_ptr<AST::Node> expr = std::make_shared<AST::LiteralInt>(0);
    if(isMpiFuncSendData(callInst)){
        if(isMpiRecv(callInst) || isMpiIrecv(callInst)){
            expr = vtec.valueToExpr(callInst.getOperand(1));
        }
        if(isMpiSendRecv(callInst)){
            expr = vtec.valueToExpr(callInst.getOperand(6));
        }
    }
    return expr;
}

std::shared_ptr<AST::Node> FuncStatisticAnalyzer::getSendedByteExpr(){
    std::shared_ptr<AST::Node> expr = std::make_shared<AST::LiteralInt>(0);
    for(auto topCount:_mpiSendCalls){
        std::shared_ptr<AST::Node> bbExpr = std::make_shared<AST::LiteralInt>(0);
        for(auto call:topCount.second.significantCalls){
            bbExpr = AST::createSumOp(bbExpr, getCallExpr2(call, _vtec, sendDataCriteria, createCustomSendBytesName));
        }

        for(auto instance: topCount.second.instances){
            auto instCountExpr = getMpiSendCountExpr(*instance, _vtec);
            instCountExpr = AST::createMulOp(instCountExpr, getMpiSendTypeExpr(*instance));
            bbExpr = AST::createSumOp(bbExpr, instCountExpr);
        }

        bbExpr = AST::createMulOp(_bte.getBasicBlockTripCount(topCount.first), bbExpr);
        expr = AST::createSumOp(expr, bbExpr);
    }
    return expr;
}

std::shared_ptr<AST::Node> FuncStatisticAnalyzer::getRecievedByteExpr(){
    std::shared_ptr<AST::Node> expr = std::make_shared<AST::LiteralInt>(0);
    for(auto topCount:_mpiRecvCalls){
        std::shared_ptr<AST::Node> bbExpr = std::make_shared<AST::LiteralInt>(0);
        for(auto call:topCount.second.significantCalls){
            bbExpr = AST::createSumOp(bbExpr, getCallExpr2(call, _vtec, recvDataCriteria, createCustomRecvBytesName));
        }

        for(auto instance: topCount.second.instances){
            auto instCountExpr = getMpiRecvCountExpr(*instance, _vtec);
            instCountExpr = AST::createMulOp(instCountExpr, getMpiRecvTypeExpr(*instance));
            bbExpr = AST::createSumOp(bbExpr, instCountExpr);
        }

        bbExpr = AST::createMulOp(_bte.getBasicBlockTripCount(topCount.first), bbExpr);
        expr = AST::createSumOp(expr, bbExpr);
    }
    return expr;
}

template<class T>
static void addInstance(std::map<const llvm::BasicBlock* , FuncStatisticAnalyzer::OccurenceInfo<T>>& dst, const llvm::BasicBlock* bb, T instance){
    auto iter = dst.find(bb);
    if(iter == dst.end()){
        dst[bb] = FuncStatisticAnalyzer::OccurenceInfo<T>();
        iter = dst.find(bb);
    }
    iter->second.instances.insert(instance);
}

template<class T>
static void addSignificantFunc(std::map<const llvm::BasicBlock*, FuncStatisticAnalyzer::OccurenceInfo<T>>& dst, const llvm::BasicBlock* bb, const llvm::CallInst* call){
    auto iter = dst.find(bb);
    if(iter == dst.end()){
        dst[bb] = FuncStatisticAnalyzer::OccurenceInfo<T>();
        iter = dst.find(bb);
    }
    iter->second.significantCalls.insert(call);
}

void FuncStatisticAnalyzer::addInstance(CriteriaType criteria, const llvm::BasicBlock *bb, const llvm::Instruction *instr){
    if(criteria == isFloat4BinaryInst) ::addInstance(_flops4, bb, instr);
    else if(criteria == isFloat8BinaryInst) ::addInstance(_flops8, bb, instr);
    else if(criteria == isStoreInst) ::addInstance(_stores, bb, llvm::dyn_cast<const llvm::StoreInst>(instr));
    else if(criteria == isLoadInst) ::addInstance(_loads, bb, llvm::dyn_cast<const llvm::LoadInst>(instr));

    if(isMathCriteria(criteria)){
        const llvm::CallInst* call = llvm::dyn_cast<const llvm::CallInst>(instr);
        _mathCalls[bb][getMathFuncType(criteria)].instances.insert(call);
    }

    if(isMpiCriteria(criteria)){
        const llvm::CallInst* call = llvm::dyn_cast<const llvm::CallInst>(instr);
        _mpiCalls[bb][getMpiFuncType(criteria)].instances.insert(call);
    }

    if(criteria == isMpiFuncSendData){
        const llvm::CallInst* call = llvm::dyn_cast<const llvm::CallInst>(instr);
        ::addInstance(_mpiSendCalls, bb, call);
    }

    if(criteria == isMpiFuncRecvData){
        const llvm::CallInst* call = llvm::dyn_cast<const llvm::CallInst>(instr);
        ::addInstance(_mpiRecvCalls, bb, call);
    }
}

void FuncStatisticAnalyzer::addSignificanFunction(CriteriaType criteria, const llvm::BasicBlock *bb, const llvm::CallInst *call){
    if(criteria == isFloat4BinaryInst) addSignificantFunc(_flops4, bb, call);
    else if(criteria == isFloat8BinaryInst) addSignificantFunc(_flops8, bb, call);
    else if(criteria == isStoreInst) addSignificantFunc(_stores, bb, call);
    else if(criteria == isLoadInst) addSignificantFunc(_loads, bb, call);

    if(isMathCriteria(criteria)){
        _mathCalls[bb][getMathFuncType(criteria)].significantCalls.insert(call);
    }

    if(isMpiCriteria(criteria)){
        _mpiCalls[bb][getMpiFuncType(criteria)].significantCalls.insert(call);
    }

    if(criteria == isMpiFuncSendData){
        _mpiSendCalls[bb].significantCalls.insert(call);
    }

    if(criteria == isMpiFuncRecvData){
        _mpiRecvCalls[bb].significantCalls.insert(call);
    }
}

void FuncStatisticAnalyzer::countInstructions(CriteriaType criteria){
    for(const auto& bb:*_func){
        for(const auto& instr:bb){
            unsigned groupIndex = _btc.getBasicBlockGroup(&bb);
            auto top = _btc.getGroupTop(groupIndex);
            if(criteria(instr)){
                addInstance(criteria, top, &instr);
            }
            if(isCallInst(instr)){
                auto call = llvm::dyn_cast<const llvm::CallInst>(&instr);
                if(call->getCalledFunction() == nullptr ) continue;
                if (CriticalParamAnalyzer<CriteriaType>::isSignificantFunction(
                            call->getCalledFunction(), criteria) ){
                    addSignificanFunction(criteria, top, call);
                }
            }
        }
    }
}

void FuncStatisticAnalyzer::countAllInstructions(){
    static CriteriaType criterias[] = {isFloat4BinaryInst, isFloat8BinaryInst,
                                       isLoadInst, isStoreInst};
    for(unsigned i = 0; i < sizeof(criterias)/sizeof(criterias[0]); ++i){
        countInstructions(criterias[i]);
    }

    for(unsigned i = 0; i < sizeof(mathCriterias)/sizeof(mathCriterias[0]); ++i){
        countInstructions(mathCriterias[i]);
    }

    for(unsigned i = 0; i < sizeof(mpiCriterias)/sizeof(mpiCriterias[0]); ++i){
        countInstructions(mpiCriterias[i]);
    }

    countInstructions(isMpiFuncSendData);

    countInstructions(isMpiFuncRecvData);

    _usedMathFuncs.clear();
    for(auto i:_mathCalls){
        for(auto j:i.second){
            _usedMathFuncs.insert(j.first);
        }
    }

    _usedMpiFuncs.clear();
    for(auto i:_mpiCalls){
        for(auto j:i.second){
            _usedMpiFuncs.insert(j.first);
        }
    }
}




