#include "customcriticalparamtracer.h"

#include <string>
#include <llvm/Analysis/LoopInfo.h>

#include "easylogging++.h"

using namespace std;
using namespace llvm;

CustomCriticalParamTracer::CustomCriticalParamTracer(ParameterSelector selector):_selector(selector)
{

}

void CustomCriticalParamTracer::getFuncCritParams(const Function *func, std::set<Value *> &vals, std::set<ComplexMemLocation> &memLocs){
    calcCritParams(func);

    auto fiIt = funcInfo.find(func);
    assert(fiIt != funcInfo.end());

    auto& critVals = fiIt->second.critVals;
    for(auto val : critVals) vals.insert(const_cast<Value*>(val));

    auto& critMls = fiIt->second.critMemLocs;
    std::copy(critMls.begin(), critMls.end(), std::inserter(memLocs, memLocs.begin()));
}

void CustomCriticalParamTracer::getFuncCritParams(const Function *func, std::set<const Value *> &vals, std::set<ComplexMemLocation> &memLocs){
    calcCritParams(func);

    auto fiIt = funcInfo.find(func);
    assert(fiIt != funcInfo.end());

    auto& critVals = fiIt->second.critVals;
    for(auto val : critVals) vals.insert(const_cast<Value*>(val));

    auto& critMls = fiIt->second.critMemLocs;
    std::copy(critMls.begin(), critMls.end(), std::inserter(memLocs, memLocs.begin()));
}

void CustomCriticalParamTracer::calcCritParams(const Function *func){
    LOG(DEBUG)<<"Start to calculate critical parameters of "<<func->getName().data()<<" function";

    auto funcInfoIt = funcInfo.find(func);
    if(funcInfoIt == funcInfo.end()){
        calculatePrioriCritParams(func);
        funcInfoIt = funcInfo.find(func);
    }

    // Iterate through critical calls.
    for(auto call:funcInfoIt->second.critCalls){
        const llvm::Function* callFunc = call->getCalledFunction();

        auto callFuncIt = funcInfo.find(callFunc);

        if(callsCache.find(call) != callsCache.end() && callsCache[call] == getFuncGist(callFuncIt->second)) continue;

        if(callFuncIt != funcInfo.end()){
            traceCallCritValues(call);
            callsCache[call] = getFuncGist(callFuncIt->second);
        }else{
            callsCache[call] = 0;
        }

        calcCritParams(callFunc);

        callFuncIt = funcInfo.find(callFunc);
        if(callsCache[call] != getFuncGist(callFuncIt->second)){
            traceCallCritValues(call);
            callsCache[call] = getFuncGist(callFuncIt->second);
        }
    }

    LOG(DEBUG)<<"Finished to calculate critical parameters of "<<func->getName().data()<<" function";
}

void CustomCriticalParamTracer::updateFuncCritParams(const Function *func){
    auto ebb = &func->getEntryBlock();
    auto fiIt = funcInfo.find(func);
    assert(fiIt != funcInfo.end());

    auto& fi = fiIt->second;

    for(auto val : fi.depVals[ebb]){
        if(dyn_cast<Argument>(val)){
            fi.critVals.insert(val);
        } else if(dyn_cast<GlobalVariable>(val)){
            fi.critVals.insert(val);
        } else{
            string valStr;
            raw_string_ostream rso(valStr);val->print(rso);
            LOG(DEBUG)<<"Can't set value "<<valStr<<" as "<<func->getName().data()<<" critical value";
        }
    }

    for(auto ml : fi.depMemLocs[ebb]){
        if(dyn_cast<Argument>(ml.base)){
            fi.critMemLocs.insert(ml);
        }else if(dyn_cast<GlobalVariable>(ml.base)){
            fi.critMemLocs.insert(ml);
        }else{
            LOG(DEBUG)<<"Can't set memory location "<<ml<<" as "<<func->getName().data()<<" critical memory location";
        }
    }
}

static const BasicBlock* tryToGetValueBasicBlock(const Function* func, const Value* val){
    if(auto inst = dyn_cast<Instruction>(val)){
        return inst->getParent();
    }

    return &func->getEntryBlock();
}

void CustomCriticalParamTracer::calculatePrioriCritParams(const Function *func){
    auto fiIt = funcInfo.find(func);

    if(fiIt == funcInfo.end()){
        funcInfo.emplace(std::make_pair(func, FuncInfo(func)));
        fiIt = funcInfo.find(func);
    }

    FuncInfo& fi = fiIt->second;

    BlockTripClassifier btc(func);
    std::set<unsigned> signifGroups;

    // Find calls of significant function, and add its basic blocks to
    // significant basic blocks
    for(const llvm::BasicBlock& bb:*func){
        for(const llvm::Instruction& instr: bb){
            if(auto call = llvm::dyn_cast<llvm::CallInst>(&instr)){
                if(isSignificantFunction(call->getCalledFunction())){
                    fi.critCalls.push_back(call);
                    signifGroups.insert(btc.getBasicBlockGroup(&bb));
                }
            }
        }
    }

    std::set<const llvm::Value*> signifVals;
    std::set<AttachedComplexMemLocation> signifMemLocs;
    std::set<const BasicBlock*> signifBasicBlocks;

    // Find significant values, memory locations and basic blocks
    _selector(func, signifVals, signifMemLocs, signifBasicBlocks);

    // Deduce critical parameters of significant values
    for(auto val : signifVals){
        traceValue(val, tryToGetValueBasicBlock(func, val), fi.depVals, fi.depMemLocs);
    }

    // Deduce critical parameters of significant memory locations
    for(auto aml : signifMemLocs){
        if(aml.beforeInst){
            traceMemLocationBeforeInstr(aml, aml.inst, fi.depVals, fi.depMemLocs);
        }else{
            traceMemLocationInclBeforeInstr(aml, aml.inst, fi.depVals, fi.depMemLocs);
        }
    }

    // Gathering significant trip count groups (each bb in group executes equal
    // times)
    for(auto bb : signifBasicBlocks){
        signifGroups.insert(btc.getBasicBlockGroup(bb));
    }

    // Deduce critical parameters that influence on trip count of significant
    // basic blocks
    for(unsigned groupIndex: signifGroups){
        traceGroupCritParams(groupIndex, btc);
    }

    updateFuncCritParams(func);
}

bool CustomCriticalParamTracer::isSignificantFunction(const Function *func, std::set<const Function *> &visitedFuncs, ParameterSelector selector){
    if(func == 0) return false;

    visitedFuncs.insert(func);

    if(func->isDeclaration()) return false;

    std::set<const llvm::Value*> sVals;
    std::set<AttachedComplexMemLocation> sMemLocs;
    std::set<const BasicBlock*> sBbs;
    selector(func, sVals, sMemLocs, sBbs);

    if(sVals.size() + sMemLocs.size() + sBbs.size() != 0) return true;

    const llvm::CallInst* call;
    for(auto& bb:*func){
        for(auto &instr:bb){
            if( (call = llvm::dyn_cast<const llvm::CallInst>(&instr)) ){
                if(visitedFuncs.find(call->getCalledFunction())==visitedFuncs.end()){
                    if(isSignificantFunction(call->getCalledFunction(), visitedFuncs, selector)) return true;
                }
            }
        }
    }

    return false;
}

bool CustomCriticalParamTracer::isSignificantFunction(const Function *func){
    std::set<const llvm::Function*> visitedFuncs;
    return isSignificantFunction(func, visitedFuncs, _selector);
}

const llvm::Value* getTermCondition(const llvm::TerminatorInst* ti){
    if(auto bi = llvm::dyn_cast<llvm::BranchInst>(ti)){
        if(bi->isConditional()){
            return bi->getCondition();
        }
        return nullptr;
    }
    if(auto ri = llvm::dyn_cast<llvm::ReturnInst>(ti)){
        return nullptr;
    }
    std::string tiStr; llvm::raw_string_ostream rso(tiStr); ti->print(rso);
    LOG(WARNING)<<"Can't get condition from instruction "<<tiStr;
    return nullptr;
};

void CustomCriticalParamTracer::traceGroupCritParams(unsigned groupIndex, const BlockTripClassifier &btc){
    auto head = btc.getGroupHead(groupIndex);
    auto top = btc.getGroupTop(groupIndex);
    auto bottom = btc.getGroupBottom(groupIndex);
    auto func = btc.getFunction();
    auto fiIt = funcInfo.find(func);
    FuncInfo& fi = fiIt->second;

    // head == nullptr only if top is entry basic block
    if(head == nullptr) return;

    // Find values that decide will control flow come from head to top or will not
    std::set<Value*> descisionVals;
    findPathDecisionValues(head, top, descisionVals);

    // Deduce critical values
    for(auto val : descisionVals){
        auto targetBb = tryToGetValueBasicBlock(func, val);
        if(targetBb == nullptr){
            targetBb = top;
        }
        traceValue(val, targetBb, fi.depVals, fi.depMemLocs);
    }

    LoopInfo li(*getDominatorTree(func));
    Loop* loop = li.getLoopFor(top);
    if(loop != nullptr && loop->getHeader() == top){
        SmallVector< BasicBlock *, 10> exitingBlocks;
        loop->getExitingBlocks(exitingBlocks);
        for(auto ebb : exitingBlocks){
            auto cond = getTermCondition(ebb->getTerminator());
            traceValue(cond, ebb, fi.depVals, fi.depMemLocs);
        }
    }

    traceGroupCritParams(btc.getBasicBlockGroup(head), btc);
}

void CustomCriticalParamTracer::traceCallCritValues(const CallInst *callInst){
    auto calleeFunc = callInst->getCalledFunction();
    auto& calleeFuncInfo = funcInfo.find(calleeFunc)->second;

    auto callerFunc = callInst->getFunction();
    auto& callerFuncInfo = funcInfo.find(callerFunc)->second;

    std::set<const Value*> callCritVals;
    std::set<ComplexMemLocation> callCritMemLocs;

    translateCallCritVals(callInst, calleeFuncInfo.critVals, callCritVals, callCritMemLocs);
    translateCallCritMemLocs(callInst, calleeFuncInfo.critMemLocs, callCritVals, callCritMemLocs);

    auto callBb = callInst->getParent();
    for(auto val : callCritVals){
        traceValue(val, callBb, callerFuncInfo.depVals, callerFuncInfo.depMemLocs);
    }

    for(auto memLoc : callCritMemLocs){
        AttachedComplexMemLocation acml(memLoc, callInst, true);

        traceMemLocationBeforeInstr(acml, callInst, callerFuncInfo.depVals, callerFuncInfo.depMemLocs);
    }

    updateFuncCritParams(callerFunc);
}

DominatorTree *CustomCriticalParamTracer::getDominatorTree(const Function *f){
    return &funcInfo.find(f)->second.dt;
}

unsigned CustomCriticalParamTracer::getFuncGist(const CustomCriticalParamTracer::FuncInfo &fi){
    return fi.critVals.size() + fi.critMemLocs.size();
}
