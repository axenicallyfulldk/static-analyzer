#include "blocktripclassifier.h"

#include <llvm/IR/CFG.h>

void BlockTripClassifier::getInnerBasicBlocks(const llvm::BasicBlock *parent,
                                         const llvm::BasicBlock *child,
                                         std::set<const llvm::BasicBlock *> &innerBlocks) const{
    llvm::SmallVector<llvm::BasicBlock*,8> parentDescendants;
    _dt.getDescendants(const_cast<llvm::BasicBlock*>(parent), parentDescendants);

    for(auto& bb:parentDescendants){
        if(!_dt.dominates(child,bb) && bb!=parent && bb!=child){
            innerBlocks.insert(bb);
        }
    }
}

bool BlockTripClassifier::parentInnerHasExternalParentSucc(const llvm::BasicBlock *parent,
                                           const llvm::BasicBlock *child,
                                           std::set<const llvm::BasicBlock *> &innerBlocks) const{
    for(auto succIt = llvm::succ_begin(parent); succIt != llvm::succ_end(parent); ++succIt){
        const llvm::BasicBlock* succ=succIt.operator*();

        if(innerBlocks.find(succ)==innerBlocks.end() && succ!=child) return true;
    }

    for(auto &ibb: innerBlocks){
        for(auto succIt = llvm::succ_begin(ibb); succIt != llvm::succ_end(ibb); ++succIt){
            const llvm::BasicBlock* succ=succIt.operator*();

            if(innerBlocks.find(succ)==innerBlocks.end() && succ!=child) return true;
        }
    }

    return false;
}

bool BlockTripClassifier::childInnerHasExternalChildPred(const llvm::BasicBlock *parent,
                                          const llvm::BasicBlock *child,
                                          std::set<const llvm::BasicBlock *> &innerBlocks) const{
    for(auto predIt = llvm::pred_begin(child); predIt != llvm::pred_end(child); ++predIt){
        const llvm::BasicBlock* pred=predIt.operator*();

        if(innerBlocks.find(pred)==innerBlocks.end() && pred!=parent) return true;
    }

    for(auto &ibb: innerBlocks){
        for(auto predIt = llvm::pred_begin(ibb); predIt != llvm::pred_end(ibb); ++predIt){
            const llvm::BasicBlock* pred=predIt.operator*();

            if(innerBlocks.find(pred)==innerBlocks.end() && pred!=parent) return true;
        }
    }

    return false;
}

bool BlockTripClassifier::parentInnerHasExternalSucc(const llvm::BasicBlock *parent, const llvm::BasicBlock *child, std::set<const llvm::BasicBlock *> &innerBlocks) const{
    for(auto succIt = llvm::succ_begin(parent); succIt != llvm::succ_end(parent); ++succIt){
        const llvm::BasicBlock* succ=succIt.operator*();

        if(innerBlocks.find(succ)==innerBlocks.end() && succ!=child && succ!=parent)
            return true;
    }

    for(auto &ibb: innerBlocks){
        for(auto succIt = llvm::succ_begin(ibb); succIt != llvm::succ_end(ibb); ++succIt){
            const llvm::BasicBlock* succ=succIt.operator*();

            if(innerBlocks.find(succ)==innerBlocks.end() && succ!=child && succ!=parent)
                return true;
        }
    }

    return false;
}

bool BlockTripClassifier::parentInnerHasExternalPred(const llvm::BasicBlock *parent, const llvm::BasicBlock *child, std::set<const llvm::BasicBlock *> &innerBlocks) const{
    for(auto pred:predecessors(parent)){
        if(_dt.dominates(child, pred)){
            return true;
        }
    }

    for(auto &ibb: innerBlocks){
        for(auto pred:predecessors(ibb )){
            if(_dt.dominates(child, pred)){
                return true;
            }
        }
    }

    return false;
}

bool BlockTripClassifier::executeSameCount(const llvm::BasicBlock *firstB,
                                           const llvm::BasicBlock *secondB) const{
    const llvm::BasicBlock* parent, *child;

    //Check if parent bb dominates child bb and child bb post-dominate parent bb
    if(_dt.dominates(firstB, secondB)){
        parent = firstB;
        child = secondB;
    }else if(_dt.dominates(secondB, firstB)){
        parent = secondB;
        child = firstB;
    }else{
        return false;
    }

    if(!_pdt.dominates(child, parent)){
        return false;
    }

    // Check if parent bb and inner bbs don't have parent bb and
    // external bbs as a successor. Check if inner bbs and child
    // bb don't have child and external bbs as a predecessor.
    std::set<const llvm::BasicBlock*> innerBlocks;
    getInnerBasicBlocks(parent, child, innerBlocks);

    if( parentInnerHasExternalParentSucc(parent, child, innerBlocks) ||
            childInnerHasExternalChildPred(parent, child, innerBlocks)) return false;

    return true;
}

bool BlockTripClassifier::sumForwardEdgesTripCountEquals(const llvm::BasicBlock *firstB, const llvm::BasicBlock *secondB) const{
    const llvm::BasicBlock* parent, *child;

    //Check if parent bb dominates child bb and child bb post-dominate parent bb
    if(_dt.dominates(firstB, secondB)){
        parent = firstB;
        child = secondB;
    }else if(_dt.dominates(secondB, firstB)){
        parent = secondB;
        child = firstB;
    }else{
        return false;
    }

    if(!_pdt.dominates(child, parent)){
        return false;
    }

    std::set<const llvm::BasicBlock*> innerBlocks;
    getInnerBasicBlocks(parent, child, innerBlocks);

    if(parentInnerHasExternalSucc(parent, child, innerBlocks) &&
            parentInnerHasExternalPred(parent, child, innerBlocks)){
        return false;
    }
    return true;
}

const llvm::BasicBlock *BlockTripClassifier::calcTopBasicBlock(const llvm::BasicBlock *bb) const{
    const llvm::BasicBlock* top = bb;
    auto dtNode = _dt.getNode(const_cast<llvm::BasicBlock*>(bb));
    while((dtNode = dtNode->getIDom())!=nullptr){
        if(executeSameCount(bb,dtNode->getBlock())){
            top = dtNode->getBlock();
        }
    }
    return top;
}

const llvm::BasicBlock *BlockTripClassifier::calcHeadBasicBlock(const llvm::BasicBlock *bb) const{
    const llvm::BasicBlock* head = nullptr;
    auto dtNode = _dt.getNode(const_cast<llvm::BasicBlock*>(bb));
    while((dtNode = dtNode->getIDom())!=nullptr){
        if(!_pdt.dominates(bb, dtNode->getBlock())){
            return dtNode->getBlock();
        }

        // Try to catch enveloping rotated loop
        std::set<const llvm::BasicBlock*> innerBlocks;
        getInnerBasicBlocks(dtNode->getBlock(), bb, innerBlocks);

        if(childInnerHasExternalChildPred(dtNode->getBlock(), bb, innerBlocks)){
            for(auto pred:llvm::predecessors(dtNode->getBlock())){
                if(_dt.dominates(bb,pred) && bb!=pred) return dtNode->getBlock();
            }
        }
    }
    return head;
}

void BlockTripClassifier::calculate(){
    using namespace llvm;

    // Determine each basic block's top. Top is basic block that
    // dominates all other basic blocks in the group.
    std::multimap<const BasicBlock*, const BasicBlock*> topBlocks;
    for(const BasicBlock& bb:*_f){
        auto topBb = calcTopBasicBlock(&bb);
        topBlocks.insert(std::pair<const BasicBlock*, const BasicBlock*>(topBb, &bb));
    }

    // Iterate through topBlocks and building _bbGroup, _groups and _groupHead
    std::map<const BasicBlock*, unsigned> headIndex;
    for(auto& i: topBlocks){
        auto it = headIndex.find(i.first);
        if(it == headIndex.end()){
            headIndex.insert({i.first, static_cast<unsigned>(_groups.size())});
            _groups.emplace_back();
            it = headIndex.find(i.first);
        }
        _bbGroup.insert({i.second, it->second});
        _groups[it->second].push_back(i.second);
    }

    // Find top basic blocks, bottom basic block and head basic block
    for(unsigned i=0; i<getGroupCount();++i){
        auto& group = _groups[i];
        auto top = group[0];
        auto bottom = group[0];
        for(auto bb:group){
            if(_dt.dominates(bb,top)){
                top = bb;
            }
            if(_pdt.dominates(bb,bottom)){
                bottom = bb;
            }
        }
        _groupTop.push_back(top);
        _groupBottom.push_back(bottom);
        _groupHead.push_back(calcHeadBasicBlock(top));
    }
}

BlockTripClassifier::BlockTripClassifier(const llvm::Function* f):_f(f),
    _dt(*const_cast<llvm::Function*>(f))
{
    _pdt.recalculate(*const_cast<llvm::Function*>(f));
    calculate();
}

const llvm::Function *BlockTripClassifier::getFunction() const{
    return _f;
}

unsigned BlockTripClassifier::getGroupCount() const{
    return _groups.size();
}

void BlockTripClassifier::getGroup(unsigned groupIndex, std::vector<const llvm::BasicBlock *> &group) const{
    std::copy(_groups[groupIndex].begin(), _groups[groupIndex].end(), std::back_inserter(group));
}

const llvm::BasicBlock *BlockTripClassifier::getGroupTop(unsigned groupIndex) const {
    return _groupTop[groupIndex];
}

const llvm::BasicBlock *BlockTripClassifier::getGroupBottom(unsigned groupIndex) const{
    return _groupBottom[groupIndex];
}

const llvm::BasicBlock *BlockTripClassifier::getGroupHead(unsigned groupIndex) const{
    return _groupHead[groupIndex];
}

unsigned BlockTripClassifier::getBasicBlockGroup(const llvm::BasicBlock *bb) const {
    return _bbGroup.at(bb);
}
