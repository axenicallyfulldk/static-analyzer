#ifndef CRITICALPARAMANALYZER_H
#define CRITICALPARAMANALYZER_H

#include <llvm/IR/Function.h>
#include <llvm/Analysis/LoopInfo.h>

#include "syntax_tree.h"

#include <set>
#include <map>

//#include "blocktripevaluater.h"
#include "blocktripclassifier.h"
//#include "syntax_tree.h"


/// This class finds parameters that decide will be instructions (that fit given
/// criteria) executed or not
template <class T>
class CriticalParamAnalyzer
{

    static void printCritParams(const std::set<const llvm::Value*>& critParams ){
        const llvm::Argument* arg;
        const llvm::GlobalVariable* gv;
        for(auto val:critParams){
            if((arg = llvm::dyn_cast<const llvm::Argument>(val))){
                llvm::outs()<<arg->getArgNo()<<" ";
            }else if((gv = llvm::dyn_cast<const llvm::GlobalVariable>(val))){
                gv->print(llvm::outs());
                llvm::outs()<<" ";
            }
        }
        llvm::outs()<<"\n";
    }

    static void printFuncCritParams(const std::map<const llvm::Function*, std::set<const llvm::Value*>>& cc){
        for(auto f:cc){
            llvm::outs()<<f.first->getName()<<" ";
            printCritParams(f.second);
        }
        llvm::outs().flush();
    }

private:
    std::set<const llvm::Value*> _criticalParams;
    T _criteria;
    const llvm::Function* _func;

    void findUsedValues(const llvm::Value* val, std::set<const llvm::Value*>& usedValues){
        if(usedValues.find(val)!=usedValues.end()) return;

        usedValues.insert(val);

        const llvm::Instruction* instr = llvm::dyn_cast<const llvm::Instruction>(val);

        if(instr != nullptr){
            for(llvm::Value* v:instr->operands()){
                findUsedValues(v, usedValues);
            }
        }
    }

    // Check if given value is parmeter. Parameter is function arguments and
    // global variables
    static bool isParameter(const llvm::Value* val){
        if(auto arg = llvm::dyn_cast<const llvm::Argument>(val)){
            return true;
        }

        if(auto gv = llvm::dyn_cast<const llvm::GlobalVariable>(val)){
            return true;
        }

        return false;
    }

    // Finds function's parameters which influence on group execution
    void traceGroupCritParams(unsigned groupIndex, const BlockTripClassifier& btc, const llvm::PostDominatorTree& pdt, std::set<const llvm::Value*>& critParams){
        auto head = btc.getGroupHead(groupIndex);
        //auto top = btc.getGroupTop(groupIndex);
        auto bottom = btc.getGroupBottom(groupIndex);

        if(head == nullptr) return;

        // Find all head successors that lead to group bbs
        std::set<const llvm::BasicBlock*> headSucc(llvm::succ_begin(head), llvm::succ_end(head));
        std::set<const llvm::BasicBlock*> postDomHeadSucc;
        std::copy_if(headSucc.begin(), headSucc.end(), std::inserter(postDomHeadSucc, postDomHeadSucc.begin()),
                     [&bottom, &pdt](const llvm::BasicBlock* bb){return pdt.dominates(bottom, bb);});

        // There is path that doesn't lead from head to group
        if(headSucc.size() != postDomHeadSucc.size()){
            std::set<const llvm::Value*> usedValues;
            findUsedValues(head->getTerminator(), usedValues);
            for(const auto val:usedValues){
                if(isParameter(val)) critParams.insert(val);
            }
        }

        traceGroupCritParams(btc.getBasicBlockGroup(head), btc, pdt, critParams);
    }


    // Finds critical params that decides, will be executed significant instructions
    // and functions or not, significant instructions that are executed in the function
    // called from the given one are not taken into account
    void calcPrioriCriticalParams(const llvm::Function* f, std::set<const llvm::Value*>& critParams){
        BlockTripClassifier btc(f);
        llvm::PostDominatorTree pdt;
        pdt.recalculate(*const_cast<llvm::Function*>(f));

        const llvm::CallInst* call;

        std::set<unsigned> significantGroups;
        for(auto &bb:*f){
            bool containsSignificantInstr = false;
            for(auto &i:bb){
                if(_criteria(i)){
                    containsSignificantInstr = true;
                    break;
                }

                if((call = llvm::dyn_cast<const llvm::CallInst>(&i))){
                    if(isSignificantFunction(call->getCalledFunction(), _criteria)){
                        containsSignificantInstr = true;
                        break;
                    }
                }
            }
            if(containsSignificantInstr)
                significantGroups.insert(btc.getBasicBlockGroup(&bb));
        }

        for(unsigned groupIndex: significantGroups){
            traceGroupCritParams(groupIndex, btc, pdt, critParams);
        }
    }


    void getCallsCriticalValues(const llvm::CallInst* call, const std::set<const llvm::Value*>& callFuncCritParam, std::set<const llvm::Value*>& critVals){
        if(callFuncCritParam.size() == 0) return;

//        BlockTripClassifier btc(call->getFunction());
//        llvm::PostDominatorTree pdt;
//        pdt.recalculate(*const_cast<llvm::Function*>(call->getFunction()));

        std::set<const llvm::Value*> usedValues;

        const llvm::Argument* arg;
        const llvm::GlobalVariable *gv;
        for(auto val:callFuncCritParam){
            if( (arg = llvm::dyn_cast<const llvm::Argument>(val)) ){
                findUsedValues(call->getOperand(arg->getArgNo()), usedValues);
            }else if( (gv = llvm::dyn_cast<const llvm::GlobalVariable>(val)) ){
                usedValues.insert(gv);
            }
        }

        //traceGroupCritParams(btc.getBasicBlockGroup(call->getParent()), btc, pdt, usedValues);

        for(auto val:usedValues){
            if(isParameter(val)){
                critVals.insert(val);
            }
        }
    }

    void calcCriticalParams( const llvm::Function* f,
                                               std::map<const llvm::Function*, std::set<const llvm::Value*>>& funcCritParamCache,
                                               std::map<const llvm::CallInst*, unsigned>& callsCache){
        if(funcCritParamCache.find(f)==funcCritParamCache.end()){
            calcPrioriCriticalParams(f, funcCritParamCache[f]);
        }

        std::vector<const llvm::CallInst*> calls;
        getSignificantCalls(f, calls);

        for(auto call:calls){
            const llvm::Function* callFunc = call->getCalledFunction();

            if(callsCache.find(call) != callsCache.end() && callsCache[call] == funcCritParamCache[callFunc].size()) continue;

            if(funcCritParamCache.find(callFunc) != funcCritParamCache.end()){
                getCallsCriticalValues(call, funcCritParamCache[callFunc], funcCritParamCache[f]);
                callsCache[call] = funcCritParamCache[callFunc].size();
            }else{
                callsCache[call] = 0;
            }

            calcCriticalParams(callFunc, funcCritParamCache, callsCache);

            if(callsCache[call] != funcCritParamCache[callFunc].size()){
                getCallsCriticalValues(call, funcCritParamCache[callFunc], funcCritParamCache[f]);
                callsCache[call] = funcCritParamCache[callFunc].size();
            }
        }
    }


    std::set<const llvm::Value*> calcCriticalParams( const llvm::Function* f){
        std::map<const llvm::Function*, std::set<const llvm::Value*>> funcCritParamCache;
        std::map<const llvm::CallInst*, unsigned> callsCache;
        calcCriticalParams(f, funcCritParamCache, callsCache);
        return funcCritParamCache[f];
    }

    static bool isSignificantFunction(const llvm::Function* func, std::set<const llvm::Function*>& visitedFuncs, const T& criteria){
        if(func == 0) return false;

        visitedFuncs.insert(func);

        if(func->isDeclaration()) return false;

        const llvm::CallInst* call;
        for(auto& bb:*func){
            for(auto &instr:bb){
                if(criteria(instr)) return true;
                if( (call = llvm::dyn_cast<const llvm::CallInst>(&instr)) ){
                    if(visitedFuncs.find(call->getCalledFunction())==visitedFuncs.end()){
                        if(isSignificantFunction(call->getCalledFunction(), visitedFuncs, criteria)) return true;
                    }
                }
            }
        }
        return false;
    }


    void getSignificantCalls(const llvm::Function* func, std::vector<const llvm::CallInst*>& calls){
        const llvm::CallInst* call;
        for(const llvm::BasicBlock& bb:*func){
            for(const llvm::Instruction& instr: bb){
                if((call = llvm::dyn_cast<const llvm::CallInst>(&instr))){
                    if(isSignificantFunction(call->getCalledFunction(), _criteria)){
                        calls.push_back(call);
                    }
                }
            }
        }
    }


public:
    CriticalParamAnalyzer(const llvm::Function* f, T criteria):_criteria(criteria), _func(f){
        _criticalParams = calcCriticalParams(f);
    }

    // Return critical parameters
    std::set<const llvm::Value*> getCriticalParams(){
        return _criticalParams;
    }

    std::vector<std::shared_ptr<AST::ValueNode>> getCriticalParams2(){
        std::vector<std::shared_ptr<AST::ValueNode>> cps;

        const llvm::Argument* arg;
        const llvm::GlobalVariable *gv;
        for(auto cp: _criticalParams){
            if( (arg = llvm::dyn_cast<const llvm::Argument>(cp)) ){
                cps.emplace_back(new AST::ArgumentNode(arg));
            }else if( (gv = llvm::dyn_cast<const llvm::GlobalVariable>(cp)) ){
                cps.emplace_back(new AST::GlobalVarNode(gv));
            }
        }

        return cps;
    }

    static bool isSignificantFunction(const llvm::Function* func, const T& criteria){
        std::set<const llvm::Function*> visitedFuncs;
        return isSignificantFunction(func, visitedFuncs, criteria);
    }


    // Function is significant if it contains instructions that are significant
    // according to given criteria, or can call function that contains such
    // instructions
    bool isSignificantFunction(){
        return isSignificantFunction(_func, _criteria);
    }
};


/// This class finds parameters that decide will be instructions (that fit given
/// criteria) executed or not
template <class T>
class CriticalParamAnalyzer2
{

    static void printCritParams(const std::set<const llvm::Value*>& critParams ){
        const llvm::Argument* arg;
        const llvm::GlobalVariable* gv;
        for(auto val:critParams){
            if((arg = llvm::dyn_cast<const llvm::Argument>(val))){
                llvm::outs()<<arg->getArgNo()<<" ";
            }else if((gv = llvm::dyn_cast<const llvm::GlobalVariable>(val))){
                gv->print(llvm::outs());
                llvm::outs()<<" ";
            }
        }
        llvm::outs()<<"\n";
    }

    static void printFuncCritParams(const std::map<const llvm::Function*, std::set<const llvm::Value*>>& cc){
        for(auto f:cc){
            llvm::outs()<<f.first->getName()<<" ";
            printCritParams(f.second);
        }
        llvm::outs().flush();
    }

private:
    std::set<const llvm::Value*> _criticalParams;
    T _criteria;
    const llvm::Function* _func;

    void findUsedValues(const llvm::Value* val, std::set<const llvm::Value*>& usedValues){
        if(usedValues.find(val)!=usedValues.end()) return;

        usedValues.insert(val);

        const llvm::Instruction* instr = llvm::dyn_cast<const llvm::Instruction>(val);

        if(instr != nullptr){
            for(llvm::Value* v:instr->operands()){
                findUsedValues(v, usedValues);
            }
        }
    }

    // Check if given value is parmeter. Parameter is function arguments and
    // global variables
    static bool isParameter(const llvm::Value* val){
        if(auto arg = llvm::dyn_cast<const llvm::Argument>(val)){
            return true;
        }

        if(auto gv = llvm::dyn_cast<const llvm::GlobalVariable>(val)){
            return true;
        }

        return false;
    }

    // Finds function's parameters which influence on group execution
    void traceGroupCritParams(unsigned groupIndex, const BlockTripClassifier& btc, const llvm::PostDominatorTree& pdt, std::set<const llvm::Value*>& critParams){
        auto head = btc.getGroupHead(groupIndex);
        auto top = btc.getGroupTop(groupIndex);
        auto bottom = btc.getGroupBottom(groupIndex);

        if(head == nullptr) return;

        // Find all head successors that lead to group bbs
        std::set<const llvm::BasicBlock*> headSucc(llvm::succ_begin(head), llvm::succ_end(head));
        std::set<const llvm::BasicBlock*> postDomHeadSucc;
        std::copy_if(headSucc.begin(), headSucc.end(), std::inserter(postDomHeadSucc, postDomHeadSucc.begin()),
                     [&bottom, &pdt](const llvm::BasicBlock* bb){return pdt.dominates(bottom, bb);});

        // There is path that doesn't lead from head to group
        if(headSucc.size() != postDomHeadSucc.size()){
            std::set<const llvm::Value*> usedValues;
            findUsedValues(head->getTerminator(), usedValues);
            for(const auto val:usedValues){
                if(isParameter(val)) critParams.insert(val);
            }
        }

        traceGroupCritParams(btc.getBasicBlockGroup(head), btc, pdt, critParams);
    }


    // Finds critical params that decides, will be executed significant instructions
    // and functions or not, significant instructions that are executed in the function
    // called from the given one are not taken into account
    void calcPrioriCriticalParams(const llvm::Function* f, std::set<const llvm::Value*>& critParams){
        BlockTripClassifier btc(f);
        llvm::PostDominatorTree pdt;
        pdt.recalculate(*const_cast<llvm::Function*>(f));

        const llvm::CallInst* call;

        std::set<unsigned> significantGroups;
        std::vector<const llvm::Value*> significantValues;
        for(auto &bb:*f){
            bool containsSignificantInstr = false;
            for(auto &i:bb){
                if(_criteria(i, &significantValues)){
                    containsSignificantInstr = true;
                }

                if((call = llvm::dyn_cast<const llvm::CallInst>(&i))){
                    if(isSignificantFunction(call->getCalledFunction(), _criteria)){
                        containsSignificantInstr = true;
                    }
                }
            }
            if(containsSignificantInstr)
                significantGroups.insert(btc.getBasicBlockGroup(&bb));
        }

        for(unsigned groupIndex: significantGroups){
            traceGroupCritParams(groupIndex, btc, pdt, critParams);
        }

        std::set<const llvm::Value*> usedValues;
        for(auto v:significantValues){
            findUsedValues(v, usedValues);
        }

        for(auto val:usedValues){
            if(isParameter(val)){
                critParams.insert(val);
            }
        }
    }


    void getCallsCriticalValues(const llvm::CallInst* call, const std::set<const llvm::Value*>& callFuncCritParam, std::set<const llvm::Value*>& critVals){
        if(callFuncCritParam.size() == 0) return;

//        BlockTripClassifier btc(call->getFunction());
//        llvm::PostDominatorTree pdt;
//        pdt.recalculate(*const_cast<llvm::Function*>(call->getFunction()));

        std::set<const llvm::Value*> usedValues;

        const llvm::Argument* arg;
        const llvm::GlobalVariable *gv;
        for(auto val:callFuncCritParam){
            if( (arg = llvm::dyn_cast<const llvm::Argument>(val)) ){
                findUsedValues(call->getOperand(arg->getArgNo()), usedValues);
            }else if( (gv = llvm::dyn_cast<const llvm::GlobalVariable>(val)) ){
                usedValues.insert(gv);
            }
        }

        //traceGroupCritParams(btc.getBasicBlockGroup(call->getParent()), btc, pdt, usedValues);

        for(auto val:usedValues){
            if(isParameter(val)){
                critVals.insert(val);
            }
        }
    }

    void calcCriticalParams( const llvm::Function* f,
                                               std::map<const llvm::Function*, std::set<const llvm::Value*>>& funcCritParamCache,
                                               std::map<const llvm::CallInst*, unsigned>& callsCache){
        if(funcCritParamCache.find(f) == funcCritParamCache.end()){
            calcPrioriCriticalParams(f, funcCritParamCache[f]);
        }

        std::vector<const llvm::CallInst*> calls;
        getSignificantCalls(f, calls);

        for(auto call:calls){
            const llvm::Function* callFunc = call->getCalledFunction();

            if(callsCache.find(call) != callsCache.end() && callsCache[call] == funcCritParamCache[callFunc].size()) continue;

            if(funcCritParamCache.find(callFunc) != funcCritParamCache.end()){
                getCallsCriticalValues(call, funcCritParamCache[callFunc], funcCritParamCache[f]);
                callsCache[call] = funcCritParamCache[callFunc].size();
            }else{
                callsCache[call] = 0;
            }

            calcCriticalParams(callFunc, funcCritParamCache, callsCache);

            if(callsCache[call] != funcCritParamCache[callFunc].size()){
                getCallsCriticalValues(call, funcCritParamCache[callFunc], funcCritParamCache[f]);
                callsCache[call] = funcCritParamCache[callFunc].size();
            }
        }
    }


    std::set<const llvm::Value*> calcCriticalParams( const llvm::Function* f){
        std::map<const llvm::Function*, std::set<const llvm::Value*>> funcCritParamCache;
        std::map<const llvm::CallInst*, unsigned> callsCache;
        calcCriticalParams(f, funcCritParamCache, callsCache);
        return funcCritParamCache[f];
    }

    static bool isSignificantFunction(const llvm::Function* func, std::set<const llvm::Function*>& visitedFuncs, const T& criteria){
        if(func == 0) return false;

        visitedFuncs.insert(func);

        if(func->isDeclaration()) return false;

        const llvm::CallInst* call;
        for(auto& bb:*func){
            for(auto &instr:bb){
                if(criteria(instr, 0)) return true;
                if( (call = llvm::dyn_cast<const llvm::CallInst>(&instr)) ){
                    if(visitedFuncs.find(call->getCalledFunction())==visitedFuncs.end()){
                        if(isSignificantFunction(call->getCalledFunction(), visitedFuncs, criteria)) return true;
                    }
                }
            }
        }
        return false;
    }


    void getSignificantCalls(const llvm::Function* func, std::vector<const llvm::CallInst*>& calls){
        const llvm::CallInst* call;
        for(const llvm::BasicBlock& bb:*func){
            for(const llvm::Instruction& instr: bb){
                if((call = llvm::dyn_cast<const llvm::CallInst>(&instr))){
                    if(isSignificantFunction(call->getCalledFunction(), _criteria)){
                        calls.push_back(call);
                    }
                }
            }
        }
    }


public:
    CriticalParamAnalyzer2(const llvm::Function* f, T criteria):_criteria(criteria), _func(f){
        _criticalParams = calcCriticalParams(f);
    }

    // Return critical parameters
    std::set<const llvm::Value*> getCriticalParams(){
        return _criticalParams;
    }

    std::vector<std::shared_ptr<AST::ValueNode>> getCriticalParams2(){
        std::vector<std::shared_ptr<AST::ValueNode>> cps;

        const llvm::Argument* arg;
        const llvm::GlobalVariable *gv;
        for(auto cp: _criticalParams){
            if( (arg = llvm::dyn_cast<const llvm::Argument>(cp)) ){
                cps.emplace_back(new AST::ArgumentNode(arg));
            }else if( (gv = llvm::dyn_cast<const llvm::GlobalVariable>(cp)) ){
                cps.emplace_back(new AST::GlobalVarNode(gv));
            }
        }

        return cps;
    }

    static bool isSignificantFunction(const llvm::Function* func, const T& criteria){
        std::set<const llvm::Function*> visitedFuncs;
        return isSignificantFunction(func, visitedFuncs, criteria);
    }


    // Function is significant if it contains instructions that are significant
    // according to given criteria, or can call function that contains such
    // instructions
    bool isSignificantFunction(){
        return isSignificantFunction(_func, _criteria);
    }
};

#endif // FUNCSTATISTICANALYZER_H
