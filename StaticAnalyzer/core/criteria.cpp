#include "criteria.h"

bool isFloatBinaryInst(const llvm::Instruction& inst){
    typedef llvm::BinaryOperator::BinaryOps BinaryOpType;

    BinaryOpType opType=static_cast<BinaryOpType>(inst.getOpcode());

    switch(opType){
    case BinaryOpType::FAdd:
    case BinaryOpType::FSub:
    case BinaryOpType::FMul:
    case BinaryOpType::FDiv:
        return true;
    default:
        return false;
    }
}

bool isFloat4BinaryInst(const llvm::Instruction& inst){
    return isFloatBinaryInst(inst) &&
            inst.getType()->getTypeID() == llvm::Type::FloatTyID;
}

bool isFloat8BinaryInst(const llvm::Instruction& inst){
    return isFloatBinaryInst(inst) &&
            inst.getType()->getTypeID() == llvm::Type::DoubleTyID;
}

bool isStoreInst(const llvm::Instruction& inst){
    if(llvm::dyn_cast<llvm::StoreInst>(&inst)!=nullptr) return true;
    return false;
}

bool isLoadInst(const llvm::Instruction& inst){
    if(llvm::dyn_cast<llvm::LoadInst>(&inst)!=nullptr) return true;
    return false;
}

bool isCallInst(const llvm::Instruction& inst){
    if(llvm::dyn_cast<llvm::CallInst>(&inst)!=nullptr) return true;
    return false;
}


#define MATH_FUNC4_CRITERIA(inst, name) auto call = llvm::dyn_cast<const llvm::CallInst>(&inst);\
    if(call && call->getCalledFunction()){\
        auto func = call->getCalledFunction();\
        if(func->getName() == #name && func->getReturnType()->isFloatTy())\
            return true;\
    }\
    return false;

#define MATH_FUNC8_CRITERIA(inst, name) auto call = llvm::dyn_cast<const llvm::CallInst>(&inst);\
    if(call && call->getCalledFunction()){\
        auto func = call->getCalledFunction();\
        if(func->getName() == #name && func->getReturnType()->isDoubleTy())\
            return true;\
    }\
    return false;

bool isSin4(const llvm::Instruction &inst){
    MATH_FUNC4_CRITERIA(inst, sin);
}

bool isSin8(const llvm::Instruction &inst){
    MATH_FUNC8_CRITERIA(inst, sin);
}

bool isCos4(const llvm::Instruction &inst){
    MATH_FUNC4_CRITERIA(inst, cos);
}

bool isCos8(const llvm::Instruction &inst){
    MATH_FUNC8_CRITERIA(inst, cos);
}

bool isTan4(const llvm::Instruction &inst){
    MATH_FUNC4_CRITERIA(inst, tan);
}

bool isTan8(const llvm::Instruction &inst){
    MATH_FUNC8_CRITERIA(inst, tan);
}

bool isSqrt4(const llvm::Instruction &inst){
    MATH_FUNC4_CRITERIA(inst, sqrt);
}

bool isSqrt8(const llvm::Instruction &inst){
    MATH_FUNC8_CRITERIA(inst, sqrt);
}

bool isLog4(const llvm::Instruction &inst){
    MATH_FUNC4_CRITERIA(inst, log);
}

bool isLog8(const llvm::Instruction &inst){
    MATH_FUNC8_CRITERIA(inst, log);
}

bool isLog10_4(const llvm::Instruction &inst){
    MATH_FUNC4_CRITERIA(inst, log10);
}

bool isLog10_8(const llvm::Instruction &inst){
    MATH_FUNC8_CRITERIA(inst, log10);
}

const CriteriaType mathCriterias[] = {isSin4, isSin8, isCos4, isCos8,
                                       isTan4, isTan8, isSqrt4, isSqrt8,
                                       isLog4, isLog8, isLog10_4, isLog10_8};


bool isMathCriteria(CriteriaType criteria){
    for(unsigned i = 0; i<sizeof(mathCriterias)/sizeof(mathCriterias[0]); ++i){
        if(criteria == mathCriterias[i]) return true;
    }
    return false;
}

bool isMathFunc(const llvm::Instruction &inst){
    for(unsigned i = 0; i<sizeof(mathCriterias)/sizeof(mathCriterias[0]); ++i){
        if(mathCriterias[i](inst)) return true;
    }
    return false;
}

MathFuncType getMathFuncType(CriteriaType criteria){
    if(criteria == isSin4) return MathFuncType::SIN4;
    if(criteria == isCos4) return MathFuncType::COS4;
    if(criteria == isTan4) return MathFuncType::TAN4;
    if(criteria == isSqrt4) return MathFuncType::SQRT4;
    if(criteria == isLog4) return MathFuncType::LOG4;
    if(criteria == isLog10_4) return MathFuncType::LOG10_4;

    if(criteria == isSin8) return MathFuncType::SIN8;
    if(criteria == isCos8) return MathFuncType::COS8;
    if(criteria == isTan8) return MathFuncType::TAN8;
    if(criteria == isSqrt8) return MathFuncType::SQRT8;
    if(criteria == isLog8) return MathFuncType::LOG8;
    if(criteria == isLog10_8) return MathFuncType::LOG10_8;

    return MathFuncType::NONE;
}

CriteriaType getMathFuncCriteria(MathFuncType mft){
    if(mft == MathFuncType::SIN4) return isSin4;
    if(mft == MathFuncType::COS4) return isCos4;
    if(mft == MathFuncType::TAN4) return isTan4;
    if(mft == MathFuncType::SQRT4) return isSqrt4;
    if(mft == MathFuncType::LOG4) return isLog4;
    if(mft == MathFuncType::LOG10_4) return isLog10_4;

    if(mft == MathFuncType::SIN8) return isSin8;
    if(mft == MathFuncType::COS8) return isCos8;
    if(mft == MathFuncType::TAN8) return isTan8;
    if(mft == MathFuncType::SQRT8) return isSqrt8;
    if(mft == MathFuncType::LOG8) return isLog8;
    if(mft == MathFuncType::LOG10_8) return isLog10_8;

    return nullptr;
}


std::string getMathFuncName(MathFuncType mft){
    if(mft == MathFuncType::SIN4) return "sin4";
    if(mft == MathFuncType::COS4) return "cos4";
    if(mft == MathFuncType::TAN4) return "tan4";
    if(mft == MathFuncType::SQRT4) return "sqrt4";
    if(mft == MathFuncType::LOG4) return "log4";
    if(mft == MathFuncType::LOG10_4) return "log10_4";

    if(mft == MathFuncType::SIN8) return "sin8";
    if(mft == MathFuncType::COS8) return "cos8";
    if(mft == MathFuncType::TAN8) return "tan8";
    if(mft == MathFuncType::SQRT8) return "sqrt8";
    if(mft == MathFuncType::LOG8) return "log8";
    if(mft == MathFuncType::LOG10_8) return "log10_8";

    return "";
}

#define MPI_FUNC_CRITERIA(inst, name) auto call = llvm::dyn_cast<const llvm::CallInst>(&inst);\
    if(call && call->getCalledFunction()){\
        auto func = call->getCalledFunction();\
        if(func->getName() == #name)\
            return true;\
    }\
    return false;


bool isMpiSend(const llvm::Instruction &inst){
    MPI_FUNC_CRITERIA(inst, MPI_Send);
}

bool isMpiRecv(const llvm::Instruction &inst){
    MPI_FUNC_CRITERIA(inst, MPI_Recv);
}

bool isMpiSendRecv(const llvm::Instruction &inst){
    //MPI_FUNC_CRITERIA(inst, MPI_Sendrecv);
    auto call = llvm::dyn_cast<const llvm::CallInst>(&inst);
    if(call && call->getCalledFunction()){
        auto func = call->getCalledFunction();
        if(func->getName() == "MPI_Sendrecv")
                return true;
    }
    return false;
}

bool isMpiSendRecvReplace(const llvm::Instruction &inst){
    MPI_FUNC_CRITERIA(inst, MPI_Sendrecv_replace);
}

bool isMpiBarrier(const llvm::Instruction &inst){
    MPI_FUNC_CRITERIA(inst, MPI_Barrier);
}

bool isMpiIsend(const llvm::Instruction &inst){
    MPI_FUNC_CRITERIA(inst, MPI_Isend);
}

bool isMpiIrecv(const llvm::Instruction &inst){
    MPI_FUNC_CRITERIA(inst, MPI_Irecv);
}

const CriteriaType mpiCriterias[] = {isMpiSend, isMpiRecv, isMpiSendRecv,
                                      isMpiSendRecvReplace, isMpiBarrier,
                                      isMpiIsend, isMpiIrecv};

bool isMpiCriteria(CriteriaType criteria){
    for(unsigned i = 0; i<sizeof(mpiCriterias)/sizeof(mpiCriterias[0]); ++i){
        if(criteria == mpiCriterias[i]) return true;
    }
    return false;
}

bool isMpiFunc(const llvm::Instruction &inst){
    for(unsigned i = 0; i<sizeof(mpiCriterias)/sizeof(mpiCriterias[0]); ++i){
        if(mpiCriterias[i](inst)) return true;
    }
    return false;
}

static const CriteriaType mpiSendCriterias[] = {isMpiSend, isMpiSendRecv,
                                          isMpiSendRecvReplace, isMpiIsend};

bool isMpiFuncSendData(const llvm::Instruction &inst){
    for(unsigned i = 0; i < sizeof (mpiSendCriterias)/sizeof(mpiSendCriterias[0]); ++i){
        if(mpiSendCriterias[i](inst)) return true;
    }
    return false;
}

static const CriteriaType mpiRecvCriterias[] = {isMpiRecv, isMpiSendRecv,
                                      isMpiSendRecvReplace, isMpiIrecv};

bool isMpiFuncRecvData(const llvm::Instruction &inst){
    for(unsigned i = 0; i < sizeof (mpiRecvCriterias)/sizeof(mpiSendCriterias[0]); ++i){
        if(mpiRecvCriterias[i](inst)) return true;
    }
    return false;
}

MpiFuncType getMpiFuncType(CriteriaType criteria){
    if(criteria == isMpiSend) return MpiFuncType::SEND;
    if(criteria == isMpiRecv) return MpiFuncType::RECV;
    if(criteria == isMpiSendRecv) return MpiFuncType::SENDRECV;
    if(criteria == isMpiSendRecvReplace) return MpiFuncType::SENDRECVREPLACE;
    if(criteria == isMpiBarrier) return MpiFuncType::BARRIER;
    if(criteria == isMpiIsend) return MpiFuncType::ISEND;
    if(criteria == isMpiIrecv) return MpiFuncType::IRECV;

    return MpiFuncType::NONE;
}

CriteriaType getMpiFuncCriteria(MpiFuncType mft){
    if(mft == MpiFuncType::SEND) return isMpiSend;
    if(mft == MpiFuncType::RECV) return isMpiRecv;
    if(mft == MpiFuncType::SENDRECV) return isMpiSendRecv;
    if(mft == MpiFuncType::SENDRECVREPLACE) return isMpiSendRecvReplace;
    if(mft == MpiFuncType::BARRIER) return isMpiBarrier;
    if(mft == MpiFuncType::ISEND) return isMpiIsend;
    if(mft == MpiFuncType::IRECV) return isMpiIrecv;

    return nullptr;
}

std::string getMpiFuncName(MpiFuncType mft){
    if(mft == MpiFuncType::SEND) return "send";
    if(mft == MpiFuncType::RECV) return "recv";
    if(mft == MpiFuncType::SENDRECV) return "sendrecv";
    if(mft == MpiFuncType::SENDRECVREPLACE) return "sendrecvreplace";
    if(mft == MpiFuncType::BARRIER) return "barrier";
    if(mft == MpiFuncType::ISEND) return "isend";
    if(mft == MpiFuncType::IRECV) return "irecv";

    return "";
}

