#ifndef UTILS_H
#define UTILS_H

#include <set>
#include <llvm/IR/Instructions.h>

#include "memloctypes.h"

std::string getSimpleBasicBlockLabel(const llvm::BasicBlock *Node);

/// Returns true if instruction modfies memory location
bool doesInstrModifiesLocation(const llvm::Instruction* instr, ComplexMemLocation ml);

bool doesInstModifiesLocation(const llvm::Instruction* inst, AttachedComplexMemLocation acml);

/// Returns critical parameters
void deduceInstrCritParamsRespectToMemLoc(const llvm::Instruction* instr, ComplexMemLocation ml,
                                         std::set<llvm::Value*>& critVals,
                                         std::set<ComplexMemLocation>& critMemLocs);

void deduceInstrCritParamsRespectToMemLoc(const llvm::Instruction* instr, ComplexMemLocation ml,
                                         std::set<const llvm::Value*>& critVals,
                                         std::set<ComplexMemLocation>& critMemLocs);

void deduceInstCritParamsRespectToMemLoc(const llvm::Instruction* instr, AttachedComplexMemLocation ml,
                                         std::set<llvm::Value*>& critVals,
                                         std::set<ComplexMemLocation>& critMemLocs);

void deduceInstCritParamsRespectToMemLoc(const llvm::Instruction* instr, AttachedComplexMemLocation ml,
                                         std::set<const llvm::Value*>& critVals,
                                         std::set<ComplexMemLocation>& critMemLocs);


/// Translate callee function's critical values to local parameters of
/// caller function
void translateCallCritVals(const llvm::CallInst* callInst, const std::set<llvm::Value*>& callCritVals,
                           std::set<llvm::Value*>& critVals, std::set<ComplexMemLocation>& critLocs);

void translateCallCritVals(const llvm::CallInst* callInst, const std::set<const llvm::Value*>& callCritVals,
                           std::set<const llvm::Value*>& critVals, std::set<ComplexMemLocation>& critLocs);

/// Translate callee function's critical memory locations to local parameters of
/// caller function
void translateCallCritMemLocs(const llvm::CallInst* callInst, const std::set<ComplexMemLocation>& callCritMemLocs,
                              std::set<llvm::Value*>& critVals, std::set<ComplexMemLocation>& critLocs);

void translateCallCritMemLocs(const llvm::CallInst* callInst, const std::set<ComplexMemLocation>& callCritMemLocs,
                              std::set<const llvm::Value*>& critVals, std::set<ComplexMemLocation>& critLocs);

/// Returns caller values and memory locations used in callee function to calculate
/// return value.
void deduceCallCritParams(const llvm::CallInst* callInst, std::set<llvm::Value*>& callCritVals,
                           std::set<ComplexMemLocation>& callCritLocs);

void deduceCallCritParams(const llvm::CallInst* callInst, std::set<const llvm::Value*>& callCritVals,
                           std::set<ComplexMemLocation>& callCritLocs);


std::string valToString(const llvm::Value* val);

#endif // UTILS_H
