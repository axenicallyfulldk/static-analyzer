#include "flowsensitiveaa.h"

#include "utils.h"

using namespace  llvm;

FlowSensitiveAA::AliasGroupsPtr FlowSensitiveAA::handleValue(const Value *val, FlowSensitiveAA::AliasGroupsPtr ags){
    if(auto inst = dyn_cast<Instruction>(val)){
        return handleInst(inst, ags);
    }

    if(auto arg = dyn_cast<Argument>(val)){
        return handleArgument(arg, ags);
    }

    if(auto gv = dyn_cast<GlobalVariable>(val)){
        return handleGlobalVariable(gv, ags);
    }

    return ags;
}

FlowSensitiveAA::AliasGroupsPtr FlowSensitiveAA::handleInst(const Instruction *inst, FlowSensitiveAA::AliasGroupsPtr ags){
    if(auto allocaInst = dyn_cast<AllocaInst>(inst)){
        return handleAllocaInst(allocaInst, ags);
    }

    if(auto bitcastInst = dyn_cast<BitCastInst>(inst)){
        return handleBitcastInst(bitcastInst, ags);
    }

    if(auto gepInst = dyn_cast<GetElementPtrInst>(inst)){
        return handleGepInst(gepInst, ags);
    }

    if(auto loadInst = dyn_cast<LoadInst>(inst)){
        return handleLoadInst(loadInst, ags);
    }

    if(auto storeInst = dyn_cast<StoreInst>(inst)){
        return handleStoreInst(storeInst, ags);
    }

    if(auto callInst = dyn_cast<CallInst>(inst)){
        return handleCallInstr(callInst, ags);
    }

    return ags;
}

FlowSensitiveAA::AliasGroupsPtr FlowSensitiveAA::handleArgument(const Argument *arg, FlowSensitiveAA::AliasGroupsPtr ags){
    auto argGroup = ags->getGroupByValue(arg);
    if(argGroup) return ags;

    if(!arg->getType()->isPointerTy()){
        return ags;
    }

    auto newAgs = std::make_shared<AliasGroups>(*ags);
    auto newArgGroup = std::make_shared<AliasGroup>();
    newArgGroup->id = _register.getIdForPointer(arg);
    newArgGroup->values.insert(arg);
    newAgs->addGroup(newArgGroup);
    return newAgs;
}

FlowSensitiveAA::AliasGroupsPtr FlowSensitiveAA::handleGlobalVariable(const GlobalVariable *gv, FlowSensitiveAA::AliasGroupsPtr ags){
    auto gvGroup = ags->getGroupByValue(gv);
    if(gvGroup) return ags;

    if(!gv->getType()->isPointerTy()){
        return ags;
    }

    auto newAgs = std::make_shared<AliasGroups>(*ags);
    auto newGvGroup = std::make_shared<AliasGroup>();
    newGvGroup->values.insert(gv);
    newGvGroup->id = _register.getIdForPointer(gv);
    newAgs->addGroup(newGvGroup);
    return newAgs;
}

FlowSensitiveAA::AliasGroupsPtr FlowSensitiveAA::handleAllocaInst(const AllocaInst *allocaInst, FlowSensitiveAA::AliasGroupsPtr ags){
    auto allocaGroup = ags->getGroupByValue(allocaInst);
    if(allocaGroup){
        return ags;
    }

    auto newAgs = std::make_shared<AliasGroups>(*ags);
    auto newGroup = std::make_shared<AliasGroup>();
    newGroup->values.insert(allocaInst);
    newGroup->id = _register.getIdForPointer(allocaInst);
    newAgs->addGroup(newGroup);
    return newAgs;
}

FlowSensitiveAA::AliasGroupsPtr FlowSensitiveAA::handleBitcastInst(const llvm::BitCastInst* bitcastInst, FlowSensitiveAA::AliasGroupsPtr ags){
    auto newAgs = ags;
    auto bitcastGroup = newAgs->getGroupByValue(bitcastInst);
    if(bitcastGroup){
        return newAgs;
    }

    auto castedVal = bitcastInst->getOperand(0);
    auto srcGroup = newAgs->getGroupByValue(castedVal);
    if(!srcGroup){
        newAgs = handleValue(castedVal, newAgs);
        srcGroup = newAgs->getGroupByValue(castedVal);
    }
    if(!srcGroup){
        auto srcStr = valToString(castedVal);
        auto bitcastStr = valToString(bitcastInst);
        LOG(WARNING)<<"Failed to handle source "<<srcStr<<" of bitcast inst "<<bitcastStr;
        return newAgs;
    }

    newAgs = std::make_shared<AliasGroups>(*newAgs);
    auto newSrcGroup = std::make_shared<AliasGroup>(*srcGroup);
    newSrcGroup->values.insert(bitcastInst);
    newAgs->replaceGroup(srcGroup->id, newSrcGroup);

    return newAgs;
}

FlowSensitiveAA::AliasGroupsPtr FlowSensitiveAA::handleCallInstr(const CallInst *callInst, FlowSensitiveAA::AliasGroupsPtr ags){
    return ags;
}

FlowSensitiveAA::AliasGroupsPtr FlowSensitiveAA::handleGepInst(const GetElementPtrInst *gepInst, FlowSensitiveAA::AliasGroupsPtr ags){
    auto newAgs = ags;

    auto gepGroup = newAgs->getGroupByValue(gepInst);
    auto gepMemLoc = MemLocation::get(gepInst);
    if(gepGroup){
        auto baseGroup = newAgs->getGroupByValue(gepMemLoc.base);
        RelativeMemLoc aliasMemLoc;
        aliasMemLoc.baseId = baseGroup->id;
        aliasMemLoc.shift = gepMemLoc.shift;
        aliasMemLoc.size = gepMemLoc.size;

        if(gepGroup->hasMemLoc(aliasMemLoc)){
            return newAgs;
        }

        newAgs = std::make_shared<AliasGroups>(*newAgs);

        auto newGepGroup = std::make_shared<AliasGroup>(*gepGroup);
        newGepGroup->values.erase(gepInst);

        if(newGepGroup->isEmpty()){
            newAgs->removeGroup(gepGroup->id);
        }else{
            newAgs->replaceGroup(gepGroup->id, newGepGroup);
        }
    }

    newAgs = std::make_shared<AliasGroups>(*newAgs);
    auto memLocGroup = newAgs->getGroupByMemLoc(gepMemLoc);
    if(memLocGroup){
        auto newMemLocGroup = std::make_shared<AliasGroup>(*memLocGroup);
        newMemLocGroup->values.insert(gepInst);
        newAgs->replaceGroup(memLocGroup->id, newMemLocGroup);
        return newAgs;
    }

    // Check that alias group for base is already created
    auto baseGroup = newAgs->getGroupByValue(gepMemLoc.base);
    if(!baseGroup){
        newAgs = handleValue(gepMemLoc.base, newAgs);
        baseGroup = newAgs->getGroupByValue(gepMemLoc.base);
    }
    assert(baseGroup.get() != nullptr);

    auto newMemLocGroup = std::make_shared<AliasGroup>();
    newMemLocGroup->values.insert(gepInst);
    RelativeMemLoc aml;
    aml.baseId = baseGroup->id;
    aml.size = gepMemLoc.size;
    aml.shift = gepMemLoc.shift;
    newMemLocGroup->memLocs.insert(aml);
    newMemLocGroup->id = _register.getIdForRelativeMemLoc(aml);
    newAgs->addGroup(newMemLocGroup);
    return newAgs;
}

FlowSensitiveAA::AliasGroupsPtr FlowSensitiveAA::handleLoadInst(const LoadInst *loadInst, FlowSensitiveAA::AliasGroupsPtr ags){
    AliasGroupsPtr newAgs = ags;
    auto pointerGroup = ags->getGroupByValue(loadInst->getPointerOperand());
    if(!pointerGroup){
        newAgs = handleValue(loadInst->getPointerOperand(), newAgs);
        pointerGroup = newAgs->getGroupByValue(loadInst->getPointerOperand());
    }
    assert(pointerGroup.get() != nullptr);


    if(!loadInst->getType()->isPointerTy()) {
        return newAgs;
    }

    // Check if group for loaded address has been added
    auto loadGroup = newAgs->getGroupByValue(loadInst);
    if(loadGroup){
        if(loadGroup->hasDerefValue(pointerGroup->id)){
            return newAgs;
        }

        // If load group doesn't contain pointer group as dereference,
        // then remove load from this group
        newAgs = std::make_shared<AliasGroups>(*newAgs);

        auto newLoadGroup = std::make_shared<AliasGroup>(*loadGroup);
        newLoadGroup->values.erase(loadInst);

        if(newLoadGroup->isEmpty()){
            newAgs->removeGroup(loadGroup->id);
        }else{
            newAgs->replaceGroup(loadGroup->id, newLoadGroup);
        }
    }

    newAgs = std::make_shared<AliasGroups>(*newAgs);
    auto derefGroup = newAgs->getGroupByDerefGroup(pointerGroup->id);
    if(derefGroup){
        auto newDerefGroup = std::make_shared<AliasGroup>(*derefGroup);
        newDerefGroup->values.insert(loadInst);
        newAgs->replaceGroup(derefGroup->id, newDerefGroup);
    }else{
        auto newDerefGroup = std::make_shared<AliasGroup>();
        newDerefGroup->id = _register.getIdForDerefPointer(pointerGroup->id);
        newDerefGroup->values.insert(loadInst);
        newDerefGroup->derefValues.insert(pointerGroup->id);
        newAgs->addGroup(newDerefGroup);
    }

    return newAgs;
}

FlowSensitiveAA::AliasGroupsPtr FlowSensitiveAA::handleStoreInst(const StoreInst *storeInst, FlowSensitiveAA::AliasGroupsPtr ags){
    AliasGroupsPtr newAgs = ags;

    auto pointerGroup = ags->getGroupByValue(storeInst->getPointerOperand());
    if(!pointerGroup){
        newAgs = handleValue(storeInst->getPointerOperand(), newAgs);
        pointerGroup = newAgs->getGroupByValue(storeInst->getPointerOperand());
    }
    if(!pointerGroup){
        auto storeInstStr = valToString(storeInst);
        auto pointerStr = valToString(storeInst->getPointerOperand());
        LOG(WARNING)<<"Failed to handle value "<<pointerStr<<" that is used as pointer operand of "<<storeInstStr;
        return newAgs;
    }

    if(!storeInst->getValueOperand()->getType()->isPointerTy()) {
        return newAgs;
    }

    auto derefGroup = newAgs->getGroupByDerefGroup(pointerGroup->id);
    if(derefGroup){
        if(derefGroup->hasValue(storeInst->getValueOperand())){
            return newAgs;
        }

        newAgs = std::make_shared<AliasGroups>(*newAgs);

        auto newDerefGroup = std::make_shared<AliasGroup>(*derefGroup);
        newDerefGroup->derefValues.erase(pointerGroup->id);
        if(newDerefGroup->isEmpty()){
            newAgs->removeGroup(derefGroup->id);
        }else{
            newAgs->replaceGroup(derefGroup->id, newDerefGroup);
        }
        newAgs->replaceGroup(derefGroup->id, newDerefGroup);
    }

    newAgs = std::make_shared<AliasGroups>(*newAgs);

    auto valueGroup = newAgs->getGroupByValue(storeInst->getValueOperand());
    if(!valueGroup){
        newAgs = handleValue(storeInst->getValueOperand(), newAgs);
        valueGroup = newAgs->getGroupByValue(storeInst->getValueOperand());
    }
    if(!valueGroup){
        auto storeInstStr = valToString(storeInst);
        auto valueStr = valToString(storeInst->getValueOperand());
        LOG(WARNING)<<"Failed to handle value "<<valueStr<<" that is used as value operand of "<<storeInstStr;
        return newAgs;
    }

    auto newValueGroup = std::make_shared<AliasGroup>(*valueGroup);
    newValueGroup->derefValues.insert(pointerGroup->id);
    newAgs->replaceGroup(valueGroup->id, newValueGroup);

    return newAgs;
}

bool FlowSensitiveAA::isMemLocationBelongToAliasGroup(const ComplexMemLocation &cml, FlowSensitiveAA::AliasGroupsConstPtr ags, FlowSensitiveAA::AliasGroupPtr ag) const{
    assert(cml.shifts.size()>0);

    if(cml.shifts.size() == cml.shiftValues.size() && cml.shiftValues.back() != nullptr){
        return ag->hasValue(cml.shiftValues.back());
    }

    if(cml.shifts.size() == 1){
        for(auto ml : ag->memLocs){
            auto baseGroup = ags->getGroup(ml.baseId);
            if(ml.shift == cml.shifts[0] && baseGroup->hasValue(cml.base)){
                return true;
            }
        }
        return false;
    }

    for(auto ml : ag->memLocs){
        if(ml.shift == cml.shifts.back()){
            auto baseGroup = ags->getGroup(ml.baseId);
            for(auto derefAg : baseGroup->derefValues){
                auto derefGroup = ags->getGroup(derefAg);
                if(isMemLocationBelongToAliasGroup(cml.getSubLocation(), ags, derefGroup)){
                    return true;
                }
            }
        }
    }

    return false;
}

FlowSensitiveAA::AliasGroupPtr FlowSensitiveAA::getAliasGroupByComplexMemLocation(const ComplexMemLocation &cml, FlowSensitiveAA::AliasGroupsConstPtr ags) const{
    assert(cml.shifts.size()>0);

    std::set<std::shared_ptr<AliasGroup>> res;
    for(auto agIt : *ags){
        if(isMemLocationBelongToAliasGroup(cml, ags, agIt.second)){
            return agIt.second;
        }
    }

    return AliasGroupPtr();
}

bool FlowSensitiveAA::alias(ComplexMemLocation ml1, ComplexMemLocation ml2, FlowSensitiveAA::AliasGroupsConstPtr ags){
    auto ag1 = getAliasGroupByComplexMemLocation(ml1, ags);
    auto ag2 = getAliasGroupByComplexMemLocation(ml2, ags);

    return ag1 == ag2 && ag1;
}

bool FlowSensitiveAA::alias(const Value *val, ComplexMemLocation cml, FlowSensitiveAA::AliasGroupsConstPtr ags){
    auto ag1 = ags->getGroupByValue(val);
    auto ag2 = getAliasGroupByComplexMemLocation(cml, ags);

    return ag1 == ag2 && ag1;
}

bool FlowSensitiveAA::alias(ComplexMemLocation cml, const Value *val, FlowSensitiveAA::AliasGroupsConstPtr ags){
    return alias(val, cml, ags);
}

bool FlowSensitiveAA::alias(const Value *val1, const Value *val2, FlowSensitiveAA::AliasGroupsConstPtr ags){
    if(val1 == val2) return true;

    auto ag1 = ags->getGroupByValue(val1);
    auto ag2 = ags->getGroupByValue(val2);

    return ag1 == ag2 && ag1;
}

void FlowSensitiveAA::calculateBasicBlock(const BasicBlock *curBb, FlowSensitiveAA::AliasGroupsPtr ags){
    auto newAgs = ags;
    for(auto& inst : *curBb){
        newAgs = handleInst(&inst, newAgs);

        for(auto ags : _aliasData[&inst]){
            if(*ags == *newAgs){

                return;
            }
        }

        _aliasData[&inst].push_back(newAgs);
    }

    auto succs = llvm::successors(curBb);
    if(std::distance(succs.begin(), succs.end())==1){
        calculateBasicBlock(*succs.begin(), newAgs);
    }else{
        for(auto bb : llvm::successors(curBb)){
            auto branchAgs = std::make_shared<AliasGroups>(*newAgs);
            branchAgs->setFlowId(_lastFlowId++);
            calculateBasicBlock(bb, branchAgs);
        }
    }
}

void FlowSensitiveAA::clean(){
    _func = nullptr;
    _register.clean();
    _aliasData.clear();
    _flowMerge.clear();
    _lastFlowId = 0;
}

void FlowSensitiveAA::calculate(const Function *func){
    clean();
    _func = func;

    auto ags = std::make_shared<AliasGroups>();
    ags->setFlowId(_lastFlowId++);
    calculateBasicBlock(&func->getEntryBlock(), ags);
}

static std::set<const llvm::Instruction*> getPreviousInstructions(const llvm::Instruction* inst){
    if(inst->getPrevNode()){
        return std::set<const llvm::Instruction*>({inst->getPrevNode()});
    }

    auto instBb = inst->getParent();

    std::set<const llvm::Instruction*> prevInsts;
    for(auto pred : llvm::predecessors(instBb)){
        prevInsts.insert(pred->getTerminator());
    }
    return prevInsts;
}

static std::set<const llvm::Instruction*> getTermInstructions(const llvm::Function* func){
    std::set<const llvm::Instruction*> insts;
    for(auto& bb : *func){
        if(bb.getSingleSuccessor() == nullptr){
            insts.insert(bb.getTerminator());
        }
    }
    return insts;
}

bool FlowSensitiveAA::alias(AttachedComplexMemLocation acml, const Value *val){
    std::set<AliasGroupsPtr> ags1;
    if(acml.beforeInst){
        auto prevInsts = getPreviousInstructions(acml.inst);
        for(auto prevInst : prevInsts){
            auto& data = _aliasData[prevInst];
            std::copy(data.begin(), data.end(), std::inserter(ags1, ags1.begin()));
        }
    }else{
        auto& data = _aliasData[acml.inst];
        std::copy(data.begin(), data.end(), std::inserter(ags1, ags1.begin()));
    }

    std::set<AliasGroupsPtr> ags2;
    for(auto& termInst : getTermInstructions(_func)){
        auto& data = _aliasData[termInst];
        std::copy(data.begin(), data.end(), std::inserter(ags2, ags2.begin()));
    }

    std::set<MemLocId> potentialMemLocs1;
    for(auto ags : ags1){
        auto ag = getAliasGroupByComplexMemLocation(acml, ags);
        if(ag) potentialMemLocs1.insert(ag->id);
    }

    std::set<MemLocId> potentialMemLocs2;
    for(auto ags : ags2){
        auto ag = ags->getGroupByValue(val);
        if(ag) potentialMemLocs2.insert(ag->id);
    }

    std::set<MemLocId> memLocIntersect;
    std::set_intersection(potentialMemLocs1.begin(), potentialMemLocs1.end(),
                          potentialMemLocs2.begin(), potentialMemLocs2.end(),
                          std::inserter(memLocIntersect, memLocIntersect.begin()));

    return memLocIntersect.size();
}

bool FlowSensitiveAA::alias(const Value *val, AttachedComplexMemLocation acml){
    return alias(acml, val);
}

bool FlowSensitiveAA::alias(const Value *val1, const Value *val2){
    if(val1 == val2) return true;

    std::set<AliasGroupsPtr> agsSet;
    for(auto& termInst : getTermInstructions(_func)){
        auto& data = _aliasData[termInst];
        std::copy(data.begin(), data.end(), std::inserter(agsSet, agsSet.begin()));
    }

    for(auto ags : agsSet){
        auto ag1 = ags->getGroupByValue(val1);
        auto ag2 = ags->getGroupByValue(val2);
        if(ag1 && ag2 && ag1->id == ag2->id) return true;
    }
    return false;
}

bool FlowSensitiveAA::alias(AttachedComplexMemLocation acml1, AttachedComplexMemLocation acml2){
    std::set<AliasGroupsPtr> ags1;
    if(acml1.beforeInst){
        auto prevInsts = getPreviousInstructions(acml1.inst);
        for(auto prevInst : prevInsts){
            auto& data = _aliasData[prevInst];
            std::copy(data.begin(), data.end(), std::inserter(ags1, ags1.begin()));
        }
    }else{
        auto& data = _aliasData[acml1.inst];
        std::copy(data.begin(), data.end(), std::inserter(ags1, ags1.begin()));
    }

    std::set<AliasGroupsPtr> ags2;
    if(acml2.beforeInst){
        auto prevInsts = getPreviousInstructions(acml2.inst);
        for(auto prevInst : prevInsts){
            auto& data = _aliasData[prevInst];
            std::copy(data.begin(), data.end(), std::inserter(ags2, ags2.begin()));
        }
    }else{
        auto& data = _aliasData[acml2.inst];
        std::copy(data.begin(), data.end(), std::inserter(ags2, ags2.begin()));
    }

    std::set<MemLocId> potentialMemLocs1;
    for(auto ags : ags1){
        auto ag = getAliasGroupByComplexMemLocation(acml1, ags);
        if(ag) potentialMemLocs1.insert(ag->id);
    }

    std::set<MemLocId> potentialMemLocs2;
    for(auto ags : ags2){
        auto ag = getAliasGroupByComplexMemLocation(acml2, ags);
        if(ag) potentialMemLocs2.insert(ag->id);
    }

    std::set<MemLocId> memLocIntersect;
    std::set_intersection(potentialMemLocs1.begin(), potentialMemLocs1.end(),
                          potentialMemLocs2.begin(), potentialMemLocs2.end(),
                          std::inserter(memLocIntersect, memLocIntersect.begin()));

    return memLocIntersect.size();
}

AnalysisKey FlowSensitiveAAWrap::Key;

std::ostream &operator<<(std::ostream &os, const FlowSensitiveAA::RelativeMemLoc &obj){
    os<<"{"<<obj.baseId<<","<<obj.shift<<","<<obj.size<<"}";
}

std::ostream &operator<<(std::ostream &os, const FlowSensitiveAA::AliasGroup &obj){
    os<<obj.id << " : <";
    os<<"[";
    std::stringstream valStream;
    for(auto val : obj.values){
        std::string valString;
        llvm::raw_string_ostream rso(valString);
        val->print(rso);
        valStream<<valString<<", ";
    }
    std::string valsString = valStream.str();
    os<<valsString.substr(0, valsString.size()-2);
    os<<"], [";
    std::stringstream memStream;
    for(auto ml : obj.memLocs){
        memStream<<ml<<", ";
    }
    std::string memString = memStream.str();
    os<<memString.substr(0, memString.size() - 2);

    os<<"], [";
    std::stringstream derefStream;
    for(auto dv : obj.derefValues){
        derefStream<<dv<<", ";
    }
    std::string derefString = derefStream.str();
    os<<derefString.substr(0, derefString.size() -2);
    os<<"]>";
}

std::ostream &operator<<(std::ostream &os, const FlowSensitiveAA::AliasGroups &obj){
    std::stringstream ss;
    for(auto it : obj){
        //std::string ssrt =  std::to_string(it.first);
        //ss<<ssrt<<" - ";
        ss<<*it.second;
        ss<<", ";
//        ss<<*it.second;
    }
    std::string str = ss.str();
    os<<str.substr(0, str.size()-2);
}
