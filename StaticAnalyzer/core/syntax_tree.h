#ifndef SYNTAX_TREE_H
#define SYNTAX_TREE_H

#include <list>
#include <memory>

#include <llvm/IR/GlobalVariable.h>
#include <llvm/IR/Constants.h>
#include <llvm/IR/Instructions.h>

#include "memloctypes.h"


namespace AST{
    enum class NodeType{ LessThan, LessOrEqual, GreaterThan, GreaterOrEqual,
                         EqualTo, NotEqualTo, Not, And, Or, MaxFunc, MinFunc, SumOp, SubOp,
                         MulOp, DivOp, CustomFunc, Argument, GlobalVar, ArgMemLoc, GlobalVarMemLoc,
                         LiteralBool, LiteralInt, LiteralFloat, Unknown };

    class Node{
    private:
        NodeType _type;
        const char* _typeStr;

        std::weak_ptr<Node> _parent;

    protected:
        Node(NodeType type, const char* typeStr);

    public:
        virtual ~Node();

        virtual std::string to_string()const=0;

        bool hasParent()const;

        std::shared_ptr<Node> getParent()const;

        NodeType getType()const;

        const char* getTypeString()const;
    };

    class ExprNode: public Node{
    private:
        std::vector<std::shared_ptr<Node>> _childs;

        const char* _funcName;
        const char* _opName;

        bool _isOperator;
        bool _isCommutative;
        bool _isUnary;

    protected:

        ExprNode(bool isOp, bool isComm, bool isUnary, NodeType type,
                 const char* typeStr);

        ExprNode(bool isOp, bool isComm, bool isUnary, const char* funcName, const char* opName,
                 NodeType type, const char* typeStr);

        void setOpName(const char* opName);

        void setFuncName(const char* funcName);

    public:

        const char* getFuncName()const;

        const char* getOpName()const;

        bool isOperator()const;

        bool isUnaryOpeator()const;

        bool isCommutative()const;

        unsigned getOperandsCount()const;

        std::shared_ptr<Node> getOperand(unsigned i)const;

        void addOperand(std::shared_ptr<Node> op);

        void setOperand(unsigned i, std::shared_ptr<Node> op);

        virtual std::shared_ptr<Node> removeOperand(unsigned i);

        virtual std::string to_string()const;

        static bool classof(const Node* node);
    };

    class BooleanNode: public ExprNode{
    protected:
        BooleanNode(bool isOp, bool isComm, bool isUnary, const char* funcName,
                    const char* opName, NodeType type, const char* typeStr);

    public:

        static bool classof(const Node* node);
    };

    class LessThen: public BooleanNode{
    private:
        static constexpr const char _funcNameStr[]="lt";
        static constexpr const char _opNameStr[]="<";

        static constexpr const char _typeStr[]="less than";

    public:
        LessThen();

        static bool classof(const Node* node);
    };

    class LessOrEqual: public BooleanNode{
    private:
        static constexpr const char _funcNameStr[]="le";
        static constexpr const char _opNameStr[]="<=";

        static constexpr const char _typeStr[]="less or equal";

    public:
        LessOrEqual();

        static bool classof(const Node* node);
    };

    class GreaterThen: public BooleanNode{
    private:
        static constexpr const char _funcNameStr[]="gt";
        static constexpr const char _opNameStr[]=">";

        static constexpr const char _typeStr[]="greater than";

    public:
        GreaterThen();

        static bool classof(const Node* node);
    };

    class GreaterOrEqual: public BooleanNode{
    private:
        static constexpr const char _funcNameStr[]="ge";
        static constexpr const char _opNameStr[]=">=";

        static constexpr const char _typeStr[]="greater or equal";

    public:
        GreaterOrEqual();

        static bool classof(const Node* node);
    };

    class EqualTo: public BooleanNode{
    private:
        static constexpr const char _funcNameStr[]="eq";
        static constexpr const char _opNameStr[]="==";

        static constexpr const char _typeStr[]="equal";

    public:
        EqualTo();

        static bool classof(const Node* node);
    };

    class NotEqualTo: public BooleanNode{
    private:
        static constexpr const char _funcNameStr[]="ne";
        static constexpr const char _opNameStr[]="<>";

        static constexpr const char _typeStr[]="not equal";

    public:
        NotEqualTo();

        static bool classof(const Node* node);
    };

    class Not: public BooleanNode{
    private:
        static constexpr const char _funcNameStr[]="not";
        static constexpr const char _opNameStr[]="not";

        static constexpr const char _typeStr[]="not";

    public:
        Not();

        static bool classof(const Node* node);
    };

    class And: public BooleanNode{
    private:
        static constexpr const char _funcNameStr[]="and";
        static constexpr const char _opNameStr[]="&";

        static constexpr const char _typeStr[]="and";

    public:
        And():BooleanNode (true, true, false, _funcNameStr, _opNameStr,
                                  NodeType::And, _typeStr){
        }

        static bool classof(const Node* node){
            return node->getType() == NodeType::And;
        }

//        virtual std::string to_string()const;
    };

    class Or: public BooleanNode{
    private:
        static constexpr const char _funcNameStr[]="or";
        static constexpr const char _opNameStr[]="|";

        static constexpr const char _typeStr[]="or";

    public:
        Or():BooleanNode (true, true, false, _funcNameStr, _opNameStr,
                                  NodeType::Or, _typeStr){
        }

        static bool classof(const Node* node){
            return node->getType() == NodeType::Or;
        }
    };

    class MaxFunc: public ExprNode{
    private:
        static constexpr const char _funcNameStr[]="max";

        static constexpr const char _typeStr[]="max";

    public:
        MaxFunc();

        static bool classof(const Node* node);
    };

    class MinFunc: public ExprNode{
    private:
        static constexpr const char _funcNameStr[]="min";

        static constexpr const char _typeStr[]="min";

    public:
        MinFunc();

        static bool classof(const Node* node);
    };

    class SumOp: public ExprNode{
    private:
        static constexpr const char _funcNameStr[]="sum";
        static constexpr const char _opNameStr[]="+";

        static constexpr const char _typeStr[]="sum";

    public:
        SumOp();

        static bool classof(const Node* node);
    };

    class SubOp: public ExprNode{
    private:
        static constexpr const char _funcNameStr[]="minus";
        static constexpr const char _opNameStr[]="-";

        static constexpr const char _typeStr[]="sum";

    public:
        SubOp();

        static bool classof(const Node* node);
    };

    class MulOp: public ExprNode{
    private:
        static constexpr const char _funcNameStr[]="mul";
        static constexpr const char _opNameStr[]="*";

        static constexpr const char _typeStr[]="mul";

    public:
        MulOp();

        static bool classof(const Node* node);
    };

    class DivOp: public ExprNode{
    private:
        static constexpr const char _funcNameStr[]="div";
        static constexpr const char _opNameStr[]="/";

        static constexpr const char _typeStr[]="div";

    public:
        DivOp();

        static bool classof(const Node* node);
    };

    class ValueNode;

    class CustomFunc: public ExprNode{
    private:
        static constexpr const char _funcNameStr[]="custom";

        static constexpr const char _typeStr[]="custom";

        std::string _customFuncName;

        std::vector<std::shared_ptr<ValueNode>> _values;

    public:
        CustomFunc();

        CustomFunc(const char* funcName);

        void setCustomFuncName(const char* name);

        std::string getCustomFuncName()const;

        void setOperandValueNode(unsigned i, std::shared_ptr<ValueNode> op){
            if(_values.size() <= i) _values.resize(i+1);
            _values[i] = op;
        }

        virtual std::shared_ptr<Node> removeOperand(unsigned i){
            _values.erase(_values.begin() + i);
            return ExprNode::removeOperand(i);
        }

        std::string to_string() const;

        static bool classof(const Node* node);
    };

    class ValueNode: public Node{
    protected:
        ValueNode(NodeType type, const char* typeStr);

    public:

        static bool classof(const Node* node);
    };

    class ArgumentNode: public ValueNode{
    private:
        static constexpr const char _typeStr[]="argument";

        std::string _argName;
        const llvm::Argument *_arg;

    public:
        ArgumentNode(const char* argName);

        ArgumentNode(const llvm::Argument* arg);

        const llvm::Argument* getIrArgument(){
            return _arg;
        }

        static bool classof(const Node* node);

        virtual std::string to_string()const {
            return std::string("p")+_argName;
        }
    };

    class GlobalVarNode: public ValueNode{
    private:
        static constexpr const char _typeStr[]="global variable";

        std::string _globalVarName;
        const llvm::GlobalVariable *_globalVar;

    public:
        GlobalVarNode(const char* gvName);

        GlobalVarNode(const llvm::GlobalVariable * gv);

        static bool classof(const Node* node);

        virtual std::string to_string()const {
            return std::string("gv_")+_globalVarName;
        }
    };

    class ArgumentMemLocNode: public ValueNode{
    private:
        static constexpr const char _typeStr[] = "memloc with argument base";

        std::string _argMemLocName;
        ComplexMemLocation _memLoc;

    public:

        ArgumentMemLocNode(const char* argMemLocName);

        ArgumentMemLocNode(ComplexMemLocation memLoc);

        ComplexMemLocation getMemLoc(){
            return _memLoc;
        }

        static bool classof(const Node* node);

        virtual std::string to_string()const {
            return std::string("p") + _argMemLocName;
        }
    };

    class GlobalVarMemLocNode: public ValueNode{
    private:
        static constexpr const char _typeStr[] = "memloc with global variable base";

        std::string _gvMemLocName;
        ComplexMemLocation _memLoc;

    public:

        GlobalVarMemLocNode(const char* gvMemLocName);

        GlobalVarMemLocNode(ComplexMemLocation memLoc);

        ComplexMemLocation getMemLoc(){
            return _memLoc;
        }

        static bool classof(const Node* node);

        virtual std::string to_string()const {
            return std::string("gv_") + _gvMemLocName;
        }
    };

    class LiteralNode: public ValueNode{
    protected:
        std::string _literalStr;
        const llvm::ConstantData *_literal;

    public:
        LiteralNode(NodeType type, const char* typeStr);

        LiteralNode(const llvm::ConstantData * cd, NodeType type, const char* typeStr);

        static bool classof(const Node* node);

        virtual std::string to_string()const;
    };

    class LiteralBool: public LiteralNode{
    private:
        static constexpr const char _typeStr[]="literal bool";

        bool _literalBool;

    public:

        LiteralBool(const llvm::ConstantInt * ci);

        LiteralBool(bool value);

        bool getValue(){return _literalBool;}

        static bool classof(const Node* node);
    };

    class LiteralInt: public LiteralNode{
    private:
        static constexpr const char _typeStr[]="literal int";

        int64_t _literalInt;

    public:

        LiteralInt(const llvm::ConstantInt * ci);

        LiteralInt(int64_t value);

        int64_t getValue(){return _literalInt;}

        static bool classof(const Node* node);
    };

    class LiteralFloat: public LiteralNode{
    private:
        static constexpr const char _typeStr[]="literal float";

        double _literalFloat;

    public:

        LiteralFloat(const llvm::ConstantFP * cf);

        LiteralFloat(double value);

        double getValue(){return _literalFloat;}

        static bool classof(const Node* node);
    };

    class UnknownNode: public Node{
    protected:
        static constexpr const char _typeStr[]="literal float";

        std::string _info;

    public:

        UnknownNode():Node(NodeType::Unknown, _typeStr){}

        UnknownNode(const char* info):Node(NodeType::Unknown, _typeStr), _info(info){}

        virtual std::string to_string()const {
            return _info;
        }

        static bool classof(const Node* node){
            return node->getType() == NodeType::Unknown;
        }
    };

    extern std::shared_ptr<AST::Node> FailNode;

    bool isZero(std::shared_ptr<Node> val);

    bool isOne(std::shared_ptr<Node> val);

    std::shared_ptr<Node> createSumOp(std::vector<std::shared_ptr<Node>>& values, std::shared_ptr<Node> defVal = std::make_shared<LiteralInt>(0));

    std::shared_ptr<Node> createSumOp(std::shared_ptr<Node> f, std::shared_ptr<Node> s, std::shared_ptr<Node> defVal = std::make_shared<LiteralInt>(0));

    std::shared_ptr<Node> createSumOp(std::shared_ptr<Node> f, std::shared_ptr<Node> s, std::shared_ptr<Node> t, std::shared_ptr<Node> defVal = std::make_shared<LiteralInt>(0));


    std::shared_ptr<Node> createOrOp(std::vector<std::shared_ptr<Node>>& values, std::shared_ptr<Node> defVal = std::make_shared<LiteralBool>(false));

    std::shared_ptr<Node> createOrOp(std::shared_ptr<Node> f, std::shared_ptr<Node> s, std::shared_ptr<Node> defVal = std::make_shared<LiteralBool>(false));

    std::shared_ptr<Node> createOrOp(std::shared_ptr<Node> f, std::shared_ptr<Node> s, std::shared_ptr<Node> t, std::shared_ptr<Node> defVal = std::make_shared<LiteralBool>(false));


    std::shared_ptr<Node> createMulOp(std::vector<std::shared_ptr<Node>>& values, std::shared_ptr<Node> defVal = std::make_shared<LiteralInt>(1));

    std::shared_ptr<Node> createMulOp(std::shared_ptr<Node> f, std::shared_ptr<Node> s, std::shared_ptr<Node> defVal = std::make_shared<LiteralInt>(1));

    std::shared_ptr<Node> createMulOp(std::shared_ptr<Node> f, std::shared_ptr<Node> s, std::shared_ptr<Node> t, std::shared_ptr<Node> defVal = std::make_shared<LiteralInt>(1));


    std::shared_ptr<Node> createAndOp(std::vector<std::shared_ptr<Node>>& values, std::shared_ptr<Node> defVal = std::make_shared<LiteralBool>(true));

    std::shared_ptr<Node> createAndOp(std::shared_ptr<Node> f, std::shared_ptr<Node> s, std::shared_ptr<Node> defVal = std::make_shared<LiteralBool>(true));

    std::shared_ptr<Node> createAndOp(std::shared_ptr<Node> f, std::shared_ptr<Node> s, std::shared_ptr<Node> t, std::shared_ptr<Node> defVal = std::make_shared<LiteralBool>(true));


    std::shared_ptr<Node> createNotOp(std::shared_ptr<Node> f);

}


#endif // SYNTAX_TREE_H
