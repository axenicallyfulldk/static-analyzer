#include "memloctypes.h"

#include <iostream>

#include <llvm/IR/Dominators.h>
#include <llvm/IR/Operator.h>
#include <llvm/IR/CallSite.h>
#include <llvm/Analysis/TargetLibraryInfo.h>
#include <llvm/Analysis/InstructionSimplify.h>
#include <llvm/IR/GetElementPtrTypeIterator.h>
#include <llvm/Analysis/ValueTracking.h>
#include <llvm/Analysis/AssumptionCache.h>

#include "easylogging++.h"

using namespace llvm;

/// To ensure a pointer offset fits in an integer of size PointerSize
/// (in bits) when that size is smaller than 64. This is an issue in
/// particular for 32b programs with negative indices that rely on two's
/// complement wrap-arounds for precise alias information.
static int64_t adjustToPointerSize(int64_t Offset, unsigned PointerSize) {
  assert(PointerSize <= 64 && "Invalid PointerSize!");
  unsigned ShiftBits = 64 - PointerSize;
  return (int64_t)((uint64_t)Offset << ShiftBits) >> ShiftBits;
}

const Value *GetLinearExpression(
    const Value *V, APInt &Scale, APInt &Offset, unsigned &ZExtBits,
    unsigned &SExtBits, const DataLayout &DL, unsigned Depth,
    AssumptionCache *AC, DominatorTree *DT, bool &NSW, bool &NUW) {
  assert(V->getType()->isIntegerTy() && "Not an integer value");

  // Limit our recursion depth.
  if (Depth == 6) {
    Scale = 1;
    Offset = 0;
    return V;
  }

  if (const ConstantInt *Const = dyn_cast<ConstantInt>(V)) {
    // If it's a constant, just convert it to an offset and remove the variable.
    // If we've been called recursively, the Offset bit width will be greater
    // than the constant's (the Offset's always as wide as the outermost call),
    // so we'll zext here and process any extension in the isa<SExtInst> &
    // isa<ZExtInst> cases below.
    Offset += Const->getValue().zextOrSelf(Offset.getBitWidth());
    assert(Scale == 0 && "Constant values don't have a scale");
    return V;
  }

  if (const BinaryOperator *BOp = dyn_cast<BinaryOperator>(V)) {
    if (ConstantInt *RHSC = dyn_cast<ConstantInt>(BOp->getOperand(1))) {
      // If we've been called recursively, then Offset and Scale will be wider
      // than the BOp operands. We'll always zext it here as we'll process sign
      // extensions below (see the isa<SExtInst> / isa<ZExtInst> cases).
      APInt RHS = RHSC->getValue().zextOrSelf(Offset.getBitWidth());

      switch (BOp->getOpcode()) {
      default:
        // We don't understand this instruction, so we can't decompose it any
        // further.
        Scale = 1;
        Offset = 0;
        return V;
      case Instruction::Or:
        // X|C == X+C if all the bits in C are unset in X.  Otherwise we can't
        // analyze it.
        if (!MaskedValueIsZero(BOp->getOperand(0), RHSC->getValue(), DL, 0, AC,
                               BOp, DT)) {
          Scale = 1;
          Offset = 0;
          return V;
        }
        LLVM_FALLTHROUGH;
      case Instruction::Add:
        V = GetLinearExpression(BOp->getOperand(0), Scale, Offset, ZExtBits,
                                SExtBits, DL, Depth + 1, AC, DT, NSW, NUW);
        Offset += RHS;
        break;
      case Instruction::Sub:
        V = GetLinearExpression(BOp->getOperand(0), Scale, Offset, ZExtBits,
                                SExtBits, DL, Depth + 1, AC, DT, NSW, NUW);
        Offset -= RHS;
        break;
      case Instruction::Mul:
        V = GetLinearExpression(BOp->getOperand(0), Scale, Offset, ZExtBits,
                                SExtBits, DL, Depth + 1, AC, DT, NSW, NUW);
        Offset *= RHS;
        Scale *= RHS;
        break;
      case Instruction::Shl:
        V = GetLinearExpression(BOp->getOperand(0), Scale, Offset, ZExtBits,
                                SExtBits, DL, Depth + 1, AC, DT, NSW, NUW);
        Offset <<= RHS.getLimitedValue();
        Scale <<= RHS.getLimitedValue();
        // the semantics of nsw and nuw for left shifts don't match those of
        // multiplications, so we won't propagate them.
        NSW = NUW = false;
        return V;
      }

      if (isa<OverflowingBinaryOperator>(BOp)) {
        NUW &= BOp->hasNoUnsignedWrap();
        NSW &= BOp->hasNoSignedWrap();
      }
      return V;
    }
  }

  // Since GEP indices are sign extended anyway, we don't care about the high
  // bits of a sign or zero extended value - just scales and offsets.  The
  // extensions have to be consistent though.
  if (isa<SExtInst>(V) || isa<ZExtInst>(V)) {
    Value *CastOp = cast<CastInst>(V)->getOperand(0);
    unsigned NewWidth = V->getType()->getPrimitiveSizeInBits();
    unsigned SmallWidth = CastOp->getType()->getPrimitiveSizeInBits();
    unsigned OldZExtBits = ZExtBits, OldSExtBits = SExtBits;
    const Value *Result =
        GetLinearExpression(CastOp, Scale, Offset, ZExtBits, SExtBits, DL,
                            Depth + 1, AC, DT, NSW, NUW);

    // zext(zext(%x)) == zext(%x), and similarly for sext; we'll handle this
    // by just incrementing the number of bits we've extended by.
    unsigned ExtendedBy = NewWidth - SmallWidth;

    if (isa<SExtInst>(V) && ZExtBits == 0) {
      // sext(sext(%x, a), b) == sext(%x, a + b)

      if (NSW) {
        // We haven't sign-wrapped, so it's valid to decompose sext(%x + c)
        // into sext(%x) + sext(c). We'll sext the Offset ourselves:
        unsigned OldWidth = Offset.getBitWidth();
        Offset = Offset.trunc(SmallWidth).sext(NewWidth).zextOrSelf(OldWidth);
      } else {
        // We may have signed-wrapped, so don't decompose sext(%x + c) into
        // sext(%x) + sext(c)
        Scale = 1;
        Offset = 0;
        Result = CastOp;
        ZExtBits = OldZExtBits;
        SExtBits = OldSExtBits;
      }
      SExtBits += ExtendedBy;
    } else {
      // sext(zext(%x, a), b) = zext(zext(%x, a), b) = zext(%x, a + b)

      if (!NUW) {
        // We may have unsigned-wrapped, so don't decompose zext(%x + c) into
        // zext(%x) + zext(c)
        Scale = 1;
        Offset = 0;
        Result = CastOp;
        ZExtBits = OldZExtBits;
        SExtBits = OldSExtBits;
      }
      ZExtBits += ExtendedBy;
    }

    return Result;
  }

  Scale = 1;
  Offset = 0;
  return V;
}


static bool isDecomposable(const Value* val){
    if(dyn_cast<Argument>(val)) return false;
    if(dyn_cast<GlobalVariable>(val)) return false;
    //if(dyn_cast<Constant>(val)) return false;
    if(dyn_cast<AllocaInst>(val)) return false;

    return true;
}


/// If V is a symbolic pointer expression, decompose it into a base pointer
/// with a constant offset and a number of scaled symbolic offsets.
///
/// The scaled symbolic offsets (represented by pairs of a Value* and a scale
/// in the VarIndices vector) are Value*'s that are known to be scaled by the
/// specified amount, but which may have other unrepresented high bits. As
/// such, the gep cannot necessarily be reconstructed from its decomposed form.
///
/// When DataLayout is around, this function is capable of analyzing everything
/// that GetUnderlyingObject can look through. To be able to do that
/// GetUnderlyingObject and DecomposeGEPExpression must use the same search
/// depth (MaxLookupSearchDepth). When DataLayout not is around, it just looks
/// through pointer casts.
static bool decomposeGep(const Value* V, MemLocation& ml, const DataLayout& dl,
                         AssumptionCache* AC, DominatorTree* DT){
    unsigned MaxLookup = 10;

    ml.shiftValue = const_cast<Value*>(V);

    do {
        if(!isDecomposable(V)){
            ml.base = const_cast<Value*>(V);
            return true;
        }

        if(dyn_cast<LoadInst>(V)){
            ml.base = const_cast<Value*>(V);
            return true;
        }

        const Operator* op = dyn_cast<Operator>(V);
        if(!op){
            if (const GlobalAlias *GA = dyn_cast<GlobalAlias>(V)) {
                if (!GA->isInterposable()) {
                    V = GA->getAliasee();
                    continue;
                }
            }
            ml.base = const_cast<Value*>(V);
            return false;
        }
        if (op->getOpcode() == Instruction::BitCast ||
            op->getOpcode() == Instruction::AddrSpaceCast) {
            V = op->getOperand(0);
            continue;
        }
        const GEPOperator *GEPOp = dyn_cast<GEPOperator>(op);
        if (!GEPOp) {
            if (const Instruction *I = dyn_cast<Instruction>(V)){
                if (const Value *Simplified =
                        SimplifyInstruction(const_cast<Instruction *>(I), dl)) {
                    V = Simplified;
                    continue;
                }
                // Practice showed that it can be phi node, wich have same incoming values or
                // the gep instructions with the same parameters
                if(const PHINode *pn = dyn_cast<PHINode>(V)){
                    auto firstGep = dyn_cast<GetElementPtrInst>(pn->getIncomingValue(0));
                    auto secondGep = dyn_cast<GetElementPtrInst>(pn->getIncomingValue(1));
                    if(firstGep != nullptr && secondGep != nullptr &&
                            firstGep->getNumIndices() == secondGep->getNumIndices() &&
                            firstGep->getPointerOperand() == secondGep->getPointerOperand()
                            ){
                        auto fgiIt = firstGep->idx_begin();
                        auto sgiIt = secondGep->idx_begin();
                        for(unsigned ind = 0; ind < firstGep->getNumIndices(); ++ind){
                            if(*fgiIt != *sgiIt){
                                break;
                            }
                            ++fgiIt;
                            ++sgiIt;
                        }
                        if(fgiIt == firstGep->idx_end()){
                            V = firstGep;
                            continue;
                        }
                    }

                    auto firstLoad = dyn_cast<LoadInst>(pn->getIncomingValue(0));
                    auto secondLoad = dyn_cast<LoadInst>(pn->getIncomingValue(1));
                    if(firstLoad != nullptr && secondLoad != nullptr && firstLoad->getPointerOperand() == secondLoad->getPointerOperand()){
                        V = firstLoad;
                        continue;
                    }
                }
            }

            ml.base = const_cast<Value*>(V);
            return false;
        }

        // Don't attempt to analyze GEPs over unsized objects.
        if (!GEPOp->getSourceElementType()->isSized()) {
            ml.base = const_cast<Value*>(V);
            return false;
        }

        unsigned AS = GEPOp->getPointerAddressSpace();
        // Walk the indices of the GEP, accumulating them into BaseOff/VarIndices.
        gep_type_iterator GTI = gep_type_begin(GEPOp);
        unsigned PointerSize = dl.getPointerSizeInBits(AS);
        // Assume all GEP operands are constants until proven otherwise.
        bool GepHasConstantOffset = true;
        for (User::const_op_iterator I = GEPOp->op_begin() + 1, E = GEPOp->op_end();
             I != E; ++I, ++GTI) {
            const Value *Index = *I;
            // Compute the (potentially symbolic) offset in bytes for this index.
            if (StructType *STy = GTI.getStructTypeOrNull()) {
                // For a struct, add the member offset.
                unsigned FieldNo = cast<ConstantInt>(Index)->getZExtValue();
                if (FieldNo == 0)
                    continue;
                ml.shift.offset +=
                        dl.getStructLayout(STy)->getElementOffset(FieldNo);
                continue;
            }

            // For an array/pointer, add the element offset, explicitly scaled.
            if (const ConstantInt *CIdx = dyn_cast<ConstantInt>(Index)) {
                if (CIdx->isZero())
                    continue;
                ml.shift.offset +=
                        dl.getTypeAllocSize(GTI.getIndexedType()) * CIdx->getSExtValue();
                continue;
            }

            GepHasConstantOffset = false;

            uint64_t Scale = dl.getTypeAllocSize(GTI.getIndexedType());
            unsigned ZExtBits = 0, SExtBits = 0;

            // If the integer type is smaller than the pointer size, it is implicitly
            // sign extended to pointer size.
            unsigned Width = Index->getType()->getIntegerBitWidth();
            if (PointerSize > Width)
                SExtBits += PointerSize - Width;

            // Use GetLinearExpression to decompose the index into a C1*V+C2 form.
            APInt IndexScale(Width, 0), IndexOffset(Width, 0);
            bool NSW = true, NUW = true;
            Index = GetLinearExpression(Index, IndexScale, IndexOffset, ZExtBits,
                                        SExtBits, dl, 0, AC, DT, NSW, NUW);

            // The GEP index scale ("Scale") scales C1*V+C2, yielding (C1*V+C2)*Scale.
            // This gives us an aggregate computation of (C1*Scale)*V + C2*Scale.
            ml.shift.offset += IndexOffset.getSExtValue() * Scale;
            Scale *= IndexScale.getSExtValue();

            // If we already had an occurrence of this index variable, merge this
            // scale into it.  For example, we want to handle:
            //   A[x][x] -> x*16 + x*4 -> x*20
            // This also ensures that 'x' only appears in the index list once.
            auto& lo = ml.shift.linearOffset;
            for(unsigned i = 0, e = lo.size(); i!= e; ++i){
                if(lo[i].val == Index){
                    Scale += lo[i].scale;
                    lo.erase(lo.begin() + i);
                }
            }

            // Make sure that we have a scale that makes sense for this target's
            // pointer size.
            Scale = adjustToPointerSize(Scale, PointerSize);

            if (Scale) {
                LinearOffset loo;
                loo.val = Index;
                loo.scale = Scale;
                lo.push_back(loo);
            }
        }
        // Take care of wrap-arounds
        if (GepHasConstantOffset) {
            ml.shift.offset = adjustToPointerSize(ml.shift.offset, PointerSize);
        }

        // Analyze the base pointer next.
        V = GEPOp->getOperand(0);

    }while(MaxLookup);

    // If the chain of expressions is too deep, just return early.
    ml.base = const_cast<Value*>(V);

    return true;
}




static FunctionAnalysisManager FAM;

static bool init(){
    FAM.registerPass([&] { return TargetLibraryAnalysis(); });
    FAM.registerPass([&] { return AssumptionAnalysis(); });
    FAM.registerPass([&] { return DominatorTreeAnalysis(); });
//    FAM.registerPass([&] { return LoopAnalysis(); });
//    FAM.registerPass([&] { return BasicAA(); });
//    FAM.registerPass([&] { return ScalarEvolutionAnalysis(); });
//    FAM.registerPass([&] { return SCEVAA(); });
//    FAM.registerPass([&] { return CFLAndersAA(); });
    return true;
}


static auto hmm = init();

static MemLocation initNullMemLocation(){
    MemLocation ml;
    return ml;
}

const MemLocation NullMemLocation = initNullMemLocation();

MemLocation::MemLocation():base(nullptr), size(0), shiftValue(nullptr){}

MemLocation MemLocation::get(const LoadInst *li){

    auto& dl = li->getModule()->getDataLayout();
    auto ac = &FAM.getResult<AssumptionAnalysis>(*const_cast<Function*>(li->getFunction()));
    auto dt = &FAM.getResult<DominatorTreeAnalysis>(*const_cast<Function*>(li->getFunction()));

    MemLocation resMl;

    auto type = li->getType();
    resMl.size = type->isSized() ? dl.getTypeSizeInBits(type)/8 : 0;
    li->getAAMetadata(resMl.AATags);

    resMl.derefValue = const_cast<LoadInst*>(li);

    bool hasNoErr = decomposeGep(li->getPointerOperand(), resMl, dl, ac, dt);
    if(!hasNoErr){
        std::string loadStr; raw_string_ostream loadStrem(loadStr); li->print(loadStrem);
        LOG(WARNING)<<"Failed to decompose memory location from instruction "<<loadStr;
        return NullMemLocation;
    }

    return resMl;
}

MemLocation MemLocation::get(const StoreInst *si){
    auto& dl = si->getModule()->getDataLayout();
    auto ac = &FAM.getResult<AssumptionAnalysis>(*const_cast<Function*>(si->getFunction()));
    auto dt = &FAM.getResult<DominatorTreeAnalysis>(*const_cast<Function*>(si->getFunction()));

    MemLocation resMl;

    auto type = si->getValueOperand()->getType();
    resMl.size = type->isSized() ? dl.getTypeSizeInBits(type)/8 : 0;
    si->getAAMetadata(resMl.AATags);

    resMl.derefValue = nullptr;

    bool hasNoErr = decomposeGep(si->getPointerOperand(), resMl, dl, ac, dt);
    if(!hasNoErr){
        std::string storeStr; raw_string_ostream storeStrem(storeStr); si->print(storeStrem);
        LOG(WARNING)<<"Failed to decompose memory location from instruction "<<storeStr;
        return NullMemLocation;
    }

    return resMl;
}

//There is an assumption
MemLocation MemLocation::get(const GetElementPtrInst *gep){

    auto& dl = gep->getModule()->getDataLayout();
    auto ac = &FAM.getResult<AssumptionAnalysis>(*const_cast<Function*>(gep->getFunction()));
    auto dt = &FAM.getResult<DominatorTreeAnalysis>(*const_cast<Function*>(gep->getFunction()));

    MemLocation resMl;

    auto type = gep->getResultElementType();
    resMl.size = type->isSized() ? dl.getTypeSizeInBits(type)/8 : 0;
    gep->getAAMetadata(resMl.AATags);

    resMl.derefValue = nullptr;

    bool hasNoErr = decomposeGep(gep, resMl, dl, ac, dt);
    if(!hasNoErr){
        std::string gepStr; raw_string_ostream gepStrem(gepStr); resMl.base->print(gepStrem);
        LOG(WARNING)<<"Failed to decompose memory location from gep instruction "<<gepStr;
        return NullMemLocation;
    }

    return resMl;
}

MemLocation MemLocation::get(const AllocaInst *ai){
    auto& dl = ai->getModule()->getDataLayout();

    MemLocation resMl;

    resMl.base = const_cast<AllocaInst*>(ai);

    auto type = ai->getAllocatedType();
    resMl.size = type->isSized() ? dl.getTypeSizeInBits(type)/8 : 0;

    ai->getAAMetadata(resMl.AATags);

    LocationShift ls;
    ls.offset = 0;
    resMl.shift = ls;

    resMl.derefValue = nullptr;

    resMl.shiftValue = const_cast<AllocaInst*>(ai);

    return resMl;
}

MemLocation MemLocation::get(const Argument *arg){
    auto& dl = arg->getParent()->getParent()->getDataLayout();

    MemLocation resMl;

    auto type = arg->getType();
    if( !type->isPointerTy()){
        std::string argStr;
        llvm::raw_string_ostream rss(argStr);arg->print(rss);
        LOG(WARNING)<<"Argument "<<argStr<<" has to be a pointer to get a mem location";
        return NullMemLocation;
    }

    resMl.base = const_cast<Argument*>(arg);

    type = type->getPointerElementType();
    resMl.size = type->isSized() ? dl.getTypeSizeInBits(type)/8 : 0;

    LocationShift ls;
    ls.offset = 0;
    resMl.shift = ls;

    resMl.derefValue = nullptr;

    resMl.shiftValue = const_cast<Argument*>(arg);

    return resMl;
}

MemLocation MemLocation::get(const GlobalVariable *gv){
    auto& dl = gv->getParent()->getDataLayout();

    MemLocation resMl;

    Type* type = gv->getType();
    type = type->getPointerElementType();

    resMl.base = const_cast<GlobalVariable*>(gv);
    resMl.size = type->isSized() ? dl.getTypeSizeInBits(type)/8 : 0;

    LocationShift ls;
    ls.offset = 0;
    resMl.shift = ls;

    resMl.derefValue = nullptr;

    resMl.shiftValue = const_cast<GlobalVariable*>(gv);

    return resMl;
}

MemLocation MemLocation::get(const Value *val){
    if(auto loadInst = dyn_cast<LoadInst>(val)){
        return MemLocation::get(loadInst);
    }
    if(auto storeInst = dyn_cast<StoreInst>(val)){
        return MemLocation::get(storeInst);
    }
    if(auto gepInst = dyn_cast<GetElementPtrInst>(val)){
        return MemLocation::get(gepInst);
    }
    if(auto allocInst = dyn_cast<AllocaInst>(val)){
        return MemLocation::get(allocInst);
    }
    if(auto arg = dyn_cast<Argument>(val)){
        return MemLocation::get(arg);
    }
    if(auto gv = dyn_cast<GlobalVariable>(val)){
        return MemLocation::get(gv);
    }

    std::string valStr; raw_string_ostream valStrem(valStr); val->print(valStrem);
    LOG(WARNING)<<"Can't get memory location from this value "<<valStr;
    return NullMemLocation;
}


Value *MemLocation::getAddressValue(){
    return shiftValue;
}

void MemLocation::clearOptionalFields(){
    shiftValue = nullptr;
    derefValue = nullptr;
    AATags = AAMDNodes();
}

bool MemLocation::operator==(const MemLocation &cml) const{
    if(base != cml.base || size != cml.size ||
            shift != cml.shift) return false;

    return true;
}

bool MemLocation::operator!=(const MemLocation &cml) const{
    return !(*this == cml);
}

bool MemLocation::operator<(const MemLocation &cml) const{
    if(base < cml.base) return true;
    else if(cml.base < base) return false;

    if(shift < cml.shift) return true;
    else if(cml.shift < shift) return false;

    if(size < cml.size) return true;
    else if(cml.size < size) return false;

    return false;
}

MemoryLocation MemLocation::toLLVMMemoryLocation() const{
    if(shiftValue == nullptr){
        LOG(WARNING)<<"Can't create valid llvm memory location for "<<*this;
        return MemoryLocation(nullptr, size, AATags);
    }
    return MemoryLocation(shiftValue, size, AATags);
}

std::ostream &operator<<(std::ostream &os, const LinearOffset &obj){
    std::string valueString;
    llvm::raw_string_ostream valueOstream(valueString);
    obj.val->print(valueOstream);
    os<<valueString<<"*"<<obj.scale;
    return os;
}

std::ostream &operator<<(std::ostream &os, const LocationShift &obj){
    os<<obj.offset;
    for(size_t i = 0; i < obj.linearOffset.size(); ++i){
        os<<"+"<<obj.linearOffset[i];
    }
    return os;
}

std::ostream &operator<<(std::ostream &os, const MemLocation &obj)
{
    os<<"(";

    std::string baseString;
    llvm::raw_string_ostream baseOstream(baseString);
    if (obj.base != nullptr){
        obj.base->print(baseOstream);
    }else{
        baseString = "null";
    }

    os<<baseString;
    os<<"+"<<obj.shift<<")";

    return os;
}


static ComplexMemLocation initNullComplexMemLocation(){
    ComplexMemLocation cml;
    return cml;
}

const ComplexMemLocation NullComplexMemLocation = initNullComplexMemLocation();

ComplexMemLocation::ComplexMemLocation():base(nullptr), size(0){}

ComplexMemLocation ComplexMemLocation::get(const LoadInst *li){

    auto& dl = li->getModule()->getDataLayout();
    auto ac = &FAM.getResult<AssumptionAnalysis>(*const_cast<Function*>(li->getFunction()));
    auto dt = &FAM.getResult<DominatorTreeAnalysis>(*const_cast<Function*>(li->getFunction()));

    ComplexMemLocation resMl;
    MemLocation ml;

    auto type = li->getType();
    resMl.size = type->isSized() ? dl.getTypeSizeInBits(type)/8 : 0;
    li->getAAMetadata(resMl.AATags);

    resMl.derefValues.push_back(const_cast<LoadInst*>(li));

    bool hasNoErr = decomposeGep(li->getPointerOperand(), ml, dl, ac, dt);
    if(!hasNoErr){
        std::string loadStr; raw_string_ostream loadStrem(loadStr); li->print(loadStrem);
        LOG(WARNING)<<"Failed to decompose memory location from instruction "<<loadStr;
        return NullComplexMemLocation;
    }

    resMl.shifts.push_back(ml.shift);
    resMl.shiftValues.push_back(ml.shiftValue);
    resMl.base = ml.base;

    if(isDecomposable(resMl.base)){
        auto ml = ComplexMemLocation::get(resMl.base);
        if(ml == NullComplexMemLocation){
            std::string baseStr; raw_string_ostream baseStrem(baseStr); resMl.base->print(baseStrem);
            LOG(WARNING)<<"Failed to decompose memory location from instruction "<<baseStr;
            return NullComplexMemLocation;
        }

        resMl.shifts.insert(resMl.shifts.begin(), ml.shifts.begin(), ml.shifts.end());
        resMl.shiftValues.insert(resMl.shiftValues.begin(), ml.shiftValues.begin(), ml.shiftValues.end());
        resMl.base = ml.base;
    }
    return resMl;
}

ComplexMemLocation ComplexMemLocation::get(const StoreInst *si){
    auto& dl = si->getModule()->getDataLayout();
    auto ac = &FAM.getResult<AssumptionAnalysis>(*const_cast<Function*>(si->getFunction()));
    auto dt = &FAM.getResult<DominatorTreeAnalysis>(*const_cast<Function*>(si->getFunction()));

    ComplexMemLocation resMl;
    MemLocation ml;

    auto type = si->getValueOperand()->getType();
    resMl.size = type->isSized() ? dl.getTypeSizeInBits(type)/8 : 0;
    si->getAAMetadata(resMl.AATags);

    resMl.derefValues.clear();
    resMl.derefValues.push_back(nullptr);

    bool hasNoErr = decomposeGep(si->getPointerOperand(), ml, dl, ac, dt);
    if(!hasNoErr){
        std::string storeStr; raw_string_ostream storeStrem(storeStr); si->print(storeStrem);
        LOG(WARNING)<<"Failed to decompose memory location from instruction "<<storeStr;
        return NullComplexMemLocation;
    }

    resMl.shifts.push_back(ml.shift);
    resMl.shiftValues.push_back(ml.shiftValue);
    resMl.base = ml.base;

    if(isDecomposable(resMl.base)){
        auto ml = ComplexMemLocation::get(resMl.base);
        if(ml == NullComplexMemLocation){
            std::string baseStr; raw_string_ostream baseStrem(baseStr); resMl.base->print(baseStrem);
            LOG(WARNING)<<"Failed to decompose memory location from instruction "<<baseStr;
            return NullComplexMemLocation;
        }

        resMl.shifts.insert(resMl.shifts.begin(), ml.shifts.begin(), ml.shifts.end());
        resMl.shiftValues.insert(resMl.shiftValues.begin(), ml.shiftValues.begin(), ml.shiftValues.end());
        resMl.base = ml.base;
    }
    return resMl;
}

//There is an assumption
ComplexMemLocation ComplexMemLocation::get(const GetElementPtrInst *gep){

    auto& dl = gep->getModule()->getDataLayout();
    auto ac = &FAM.getResult<AssumptionAnalysis>(*const_cast<Function*>(gep->getFunction()));
    auto dt = &FAM.getResult<DominatorTreeAnalysis>(*const_cast<Function*>(gep->getFunction()));

    ComplexMemLocation resMl;
    MemLocation ml;

    auto type = gep->getResultElementType();
    resMl.size = type->isSized() ? dl.getTypeSizeInBits(type)/8 : 0;
    gep->getAAMetadata(resMl.AATags);

    resMl.derefValues.clear();
    resMl.derefValues.push_back(nullptr);

    bool hasNoErr = decomposeGep(gep, ml, dl, ac, dt);
    if(!hasNoErr){
        std::string gepStr; raw_string_ostream gepStrem(gepStr); resMl.base->print(gepStrem);
        LOG(WARNING)<<"Failed to decompose memory location from gep instruction "<<gepStr;
        return NullComplexMemLocation;
    }

    resMl.base = ml.base;
    resMl.shifts.push_back(ml.shift);
    resMl.shiftValues.push_back(ml.shiftValue);

    if(isDecomposable(resMl.base)){
        auto ml = ComplexMemLocation::get(resMl.base);
        if(ml == NullComplexMemLocation){
            std::string baseStr; raw_string_ostream baseStrem(baseStr); resMl.base->print(baseStrem);
            LOG(WARNING)<<"Failed to decompose memory location from instruction "<<baseStr;
            return NullComplexMemLocation;
        }

        resMl.shifts.insert(resMl.shifts.begin(), ml.shifts.begin(), ml.shifts.end());
        resMl.shiftValues.insert(resMl.shiftValues.begin(), ml.shiftValues.begin(), ml.shiftValues.end());
        resMl.base = ml.base;
    }
    return resMl;
}

ComplexMemLocation ComplexMemLocation::get(const AllocaInst *ai){

    MemLocation ml = MemLocation::get(ai);

    ComplexMemLocation resMl;
    resMl.base = ml.base;
    resMl.size = ml.size;
    resMl.AATags = ml.AATags;
    resMl.shifts.push_back(ml.shift);
    resMl.derefValues.push_back(ml.derefValue);
    resMl.shiftValues.push_back(ml.shiftValue);

    return resMl;
}

ComplexMemLocation ComplexMemLocation::get(const Argument *arg){

    MemLocation ml = MemLocation::get(arg);

    ComplexMemLocation resMl;
    resMl.base = ml.base;
    resMl.size = ml.size;
    resMl.AATags = ml.AATags;
    resMl.shifts.push_back(ml.shift);
    resMl.derefValues.push_back(ml.derefValue);
    resMl.shiftValues.push_back(ml.shiftValue);

    return resMl;
}

ComplexMemLocation ComplexMemLocation::get(const GlobalVariable *gv){
    MemLocation ml = MemLocation::get(gv);

    ComplexMemLocation resMl;
    resMl.base = ml.base;
    resMl.size = ml.size;
    resMl.AATags = ml.AATags;
    resMl.shifts.push_back(ml.shift);
    resMl.derefValues.push_back(ml.derefValue);
    resMl.shiftValues.push_back(ml.shiftValue);

    return resMl;
}

ComplexMemLocation ComplexMemLocation::get(const Value *val){
    if(auto loadInst = dyn_cast<LoadInst>(val)){
        return ComplexMemLocation::get(loadInst);
    }
    if(auto storeInst = dyn_cast<StoreInst>(val)){
        return ComplexMemLocation::get(storeInst);
    }
    if(auto gepInst = dyn_cast<GetElementPtrInst>(val)){
        return ComplexMemLocation::get(gepInst);
    }
    if(auto allocInst = dyn_cast<AllocaInst>(val)){
        return ComplexMemLocation::get(allocInst);
    }
    if(auto arg = dyn_cast<Argument>(val)){
        return ComplexMemLocation::get(arg);
    }
    if(auto gv = dyn_cast<GlobalVariable>(val)){
        return ComplexMemLocation::get(gv);
    }

    std::string valStr; raw_string_ostream valStrem(valStr); val->print(valStrem);
    LOG(WARNING)<<"Can't get memory location from this value "<<valStr;
    return NullComplexMemLocation;
}

static size_t tryToGetValueSize(const Value* val){
    if(auto instr = dyn_cast<Instruction>(val)){
        auto& dl = instr->getModule()->getDataLayout();
        auto type = instr->getType();
        return type->isSized() ? dl.getTypeSizeInBits(type)/8 : 0;
    }
    return 0;
}

static const DataLayout* tryToGetDataLayout(const Value* val){
    if(auto instr = dyn_cast<Instruction>(val)){
        return &instr->getModule()->getDataLayout();
    }
    if(auto arg = dyn_cast<Argument>(val)){
        return &arg->getParent()->getParent()->getDataLayout();
    }
    if(auto gv = dyn_cast<GlobalVariable>(val)){
        return &gv->getParent()->getDataLayout();
    }
    return nullptr;
}

static bool tryToGetAAMetadata(const Value* val, AAMDNodes& aamdn){
    if(auto instr = dyn_cast<Instruction>(val)){
        instr->getAAMetadata(aamdn);
        return true;
    }
    return false;
}

//ComplexMemLocation ComplexMemLocation::getSubLocation(unsigned offsetIndex){
//    ComplexMemLocation cml = *this;
//    if(offsetIndex + 1 >= shifts.size()){
//        return cml;
//    }

//    cml.shifts.resize(offsetIndex + 1);
//    cml.shiftValues.resize(offsetIndex + 1);

//    //Since it is sub location it has to point to pointer
//    auto dl = tryToGetDataLayout(cml.base);
//    cml.size = dl->getPointerSizeInBits()/8;

//    if(cml.shiftValues.back() != nullptr){
//        tryToGetAAMetadata(cml.shiftValues.back(), cml.AATags);
//    }else{
//        cml.AATags = AAMDNodes();
//    }
//    return cml;
//}

ComplexMemLocation ComplexMemLocation::getSubLocation(int offsetCount) const{
    ComplexMemLocation cml = *this;
    int shiftSize = shifts.size();
    if(offsetCount >= shiftSize){
        return cml;
    }

    if(offsetCount == -1) {
        offsetCount = shifts.size()-1;
    }

    cml.shifts.resize(offsetCount);
    cml.shiftValues.resize(offsetCount);

    //Since it is sub location it has to point to pointer
    auto dl = tryToGetDataLayout(cml.base);
    cml.size = dl->getPointerSizeInBits()/8;

    if(cml.shiftValues.back() != nullptr){
        tryToGetAAMetadata(cml.shiftValues.back(), cml.AATags);
    }else{
        cml.AATags = AAMDNodes();
    }
    return cml;
}

Value *ComplexMemLocation::getAddressValue(){
#pragma message "FIXME"
    return shifts.size() == shiftValues.size() ? shiftValues.back() : nullptr;
}

void ComplexMemLocation::clearOptionalFields(){
    std::fill(shiftValues.begin(), shiftValues.end(), nullptr);
    std::fill(derefValues.begin(), derefValues.end(), nullptr);
}

bool ComplexMemLocation::operator==(const ComplexMemLocation &cml) const{
    if(base != cml.base || size != cml.size ||
            shifts.size() != cml.shifts.size()) return false;

    for(size_t i = 0; i < shifts.size(); ++i){
        if( shifts[i] != cml.shifts[i]) return false;
    }

    return true;
}

bool ComplexMemLocation::operator!=(const ComplexMemLocation &cml) const{
    return !(*this == cml);
}

bool ComplexMemLocation::operator<(const ComplexMemLocation &cml) const{
    if(base < cml.base) return true;
    else if(cml.base < base) return false;

    if(shifts.size() < cml.shifts.size()) return true;
    else if (cml.shifts.size() < shifts.size()) return false;

    for(size_t i = 0; i < shifts.size(); ++i){
        if(shifts[i] < cml.shifts[i]) return true;
    }

    if(size < cml.size) return true;
    else if(cml.size < size) return false;

    return false;
}

MemoryLocation ComplexMemLocation::toLLVMMemoryLocation() const{
    if(shifts.size() != shiftValues.size() || shiftValues.size() == 0 ||
            shiftValues.back() == nullptr){
        std::cerr<<"Can't create valid llvm memory location "<<__FILE__<<std::endl;
        return MemoryLocation(nullptr, size, AATags);
    }
    return MemoryLocation(shiftValues.back(), size, AATags);
}


std::ostream &operator<<(std::ostream &os, const ComplexMemLocation &obj)
{
    for(size_t i = 0; i< obj.shifts.size(); ++i){
        os<<"(";
    }

    std::string baseString;
    llvm::raw_string_ostream baseOstream(baseString);
    if (obj.base != nullptr){
        obj.base->print(baseOstream);
    }else{
        baseString = "null";
    }

    os<<baseString;

    if(obj.shifts.size()){
        for(size_t i = 0; i< obj.shifts.size() - 1; ++i){
            os<<"+"<<obj.shifts[i]<<")";
        }

        os<<"+"<<obj.shifts.back()<<","<<obj.size<<")";
    }

    return os;
}




