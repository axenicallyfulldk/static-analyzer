#ifndef BLOCKTRIPCLASSIFIER_H
#define BLOCKTRIPCLASSIFIER_H

#include <llvm/IR/Function.h>
#include <llvm/IR/Dominators.h>
#include <llvm/Analysis/PostDominators.h>
#include <llvm/IR/CFG.h>

#include <vector>
#include <map>

/// This class is devoted to find basic blocks in function that trip count is
/// equal. All basic blocks in function belong to a certain group. Each basic
/// block in a group has the same trip count as each other basic block in this
/// group.
class BlockTripClassifier
{
    const llvm::Function* _f;

    llvm::DominatorTree _dt;
    llvm::PostDominatorTree _pdt;

    std::map<const llvm::BasicBlock*, unsigned> _bbGroup;

    std::vector<std::vector<const llvm::BasicBlock*>> _groups;
    std::vector<const llvm::BasicBlock*> _groupTop;
    std::vector<const llvm::BasicBlock*> _groupBottom;
    std::vector<const llvm::BasicBlock*> _groupHead;

    /// Return inner basic blocks. Inner bbs - are basic blocks that are strictly
    /// dominated by parent bb and not dominated by child node.
    void getInnerBasicBlocks(const llvm::BasicBlock* parent, const llvm::BasicBlock* child,
                             std::set<const llvm::BasicBlock*>& innerBlocks) const;

    /// Check if parent bb and inner bbs have parent bb or external bbs
    /// as a successor
    bool parentInnerHasExternalParentSucc(const llvm::BasicBlock* parent, const llvm::BasicBlock* child,
                               std::set<const llvm::BasicBlock*>& innerBlocks) const;

    /// Check if inner bbs and child bb have child or external bbs as
    /// a predecessor
    bool childInnerHasExternalChildPred(const llvm::BasicBlock *parent, const llvm::BasicBlock *child,
                              std::set<const llvm::BasicBlock *> &innerBlocks) const;

    /// Check if parent bb and inner bbs have external bbs as a successor
    bool parentInnerHasExternalSucc(const llvm::BasicBlock* parent, const llvm::BasicBlock* child,
                               std::set<const llvm::BasicBlock*>& innerBlocks) const;

    /// Check if inner bbs and parent bb have a predecessor that dominated by child bb
    bool parentInnerHasExternalPred(const llvm::BasicBlock* parent, const llvm::BasicBlock* child,
                         std::set<const llvm::BasicBlock*>& innerBlocks) const;


    /// Two basic blocks (parent and child) have the same trip count if:
    /// 1. Parent bb dominates child bb and child bb post-dominates parent bb
    /// 2. Parent bb and inner bbs don't have parent bb or external bbs
    /// as a successor
    /// 3. Inner bbs and child bb don't have child or external bbs as
    /// a predecessor
    ///
    /// Parent bb - is basic block that dominates child bb
    /// Child bb - is basic block that is dominated by parent bb
    /// Inner bbs - are basic blocks that is dominated by parent bb and
    /// not dominated by child bb
    /// External bbs - are basic blocks that not a parent, not a child, and not
    /// an inner.
    bool executeSameCount(const llvm::BasicBlock* firstB, const llvm::BasicBlock* secondB)const;

    /// Return true if sum of trip counts of forward edges incoming to firstB
    /// and secondB is equal
    bool sumForwardEdgesTripCountEquals(const llvm::BasicBlock* firstB, const llvm::BasicBlock* secondB)const;

    /// Return top basic block. Top bb dominates each other bb in the group and
    /// have the same trip count as others bbs in the group.
    const llvm::BasicBlock* calcTopBasicBlock(const llvm::BasicBlock* bb) const;

    /// Return head basic block. In reducible graphs this function supposed to
    /// return basic block where a decision to execute bb is made.
    const llvm::BasicBlock* calcHeadBasicBlock(const llvm::BasicBlock* bb) const;

    void calculate();

public:
    BlockTripClassifier(const llvm::Function* f);

    /// Return classified function
    const llvm::Function* getFunction() const;

    /// Return count of groups in the function
    unsigned getGroupCount()const;

    /// Get all basic blocks that belongs to a group
    void getGroup(unsigned groupIndex, std::vector<const llvm::BasicBlock *> &group) const;

    /// Return a top bb of a group. Top basic block dominates each other basic
    /// block in the group and it is part of the group.
    const llvm::BasicBlock* getGroupTop(unsigned groupIndex)const;

    /// Return a bottom bb of a group. Bottom basic block post-dominates each
    /// other basic block in the group and it is part of the group.
    const llvm::BasicBlock* getGroupBottom(unsigned groupIndex)const;

    /// Return a head bb of a group.
    const llvm::BasicBlock* getGroupHead(unsigned groupIndex) const;

    /// Return group index which basic block belongs to
    unsigned getBasicBlockGroup(const llvm::BasicBlock* bb)const;
};

#endif // BLOCKTRIPCLASSIFIER_H
