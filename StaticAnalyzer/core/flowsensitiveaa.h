#ifndef FLOWSENSITIVEAA_H
#define FLOWSENSITIVEAA_H

#include <memory>
#include <cassert>
#include <set>
#include <map>
#include "memloctypes.h"
#include "llvm/IR/Value.h"
#include "llvm/IR/GlobalVariable.h"
#include "llvm/IR/CFG.h"
#include "llvm/IR/PassManager.h"

class FlowSensitiveAA{
private:
public:
    const llvm::Function* _func;

    using MemLocId = int64_t;
    struct RelativeMemLoc{
        MemLocId baseId;
        LocationShift shift;
        uint64_t size;

        bool operator==(const RelativeMemLoc& aml)const{
            return baseId == aml.baseId && shift == aml.shift && size == aml.size;
        }

        bool operator<(const RelativeMemLoc& aml)const{
            if(baseId < aml.baseId) return true;
            else if(aml.baseId < baseId) return false;

            if(shift < aml.shift) return true;
            else if (aml.shift < shift) return false;

            if(size < aml.size) return true;
            else if (aml.size < size) return false;

            return false;
        }
    };

    struct MemLocRegister{
        MemLocId lastMemLocId;

        std::map<RelativeMemLoc, MemLocId> aliasMemLoc;
        std::map<const llvm::Value*, MemLocId> pointerMemLoc;
        std::map<MemLocId, MemLocId> derefMemLoc;

        MemLocRegister():lastMemLocId(0){}

        bool hasRelativeMemLoc(RelativeMemLoc aml){
            return aliasMemLoc.find(aml) != aliasMemLoc.end();
        }
        bool hasPointer(const llvm::Value* pointer){
            return pointerMemLoc.find(pointer) != pointerMemLoc.end();
        }
        bool hasDerefPointer(MemLocId memLocId){
            return derefMemLoc.find(memLocId) != derefMemLoc.end();
        }

        MemLocId getIdForRelativeMemLoc(RelativeMemLoc rml){
            auto it = aliasMemLoc.find(rml);
            if(it == aliasMemLoc.end()){
                auto id = lastMemLocId++;
                aliasMemLoc[rml] = id;
                return id;
            }
            return it->second;
        }

        MemLocId getIdForPointer(const llvm::Value* pointer){
            auto it = pointerMemLoc.find(pointer);
            if(it == pointerMemLoc.end()){
                auto id = lastMemLocId++;
                pointerMemLoc[pointer] = id;
                return id;
            }
            return it->second;
        }

        MemLocId getIdForDerefPointer(MemLocId memLocId){
            auto it = derefMemLoc.find(memLocId);
            if(it == derefMemLoc.end()){
                auto id = lastMemLocId++;
                derefMemLoc[memLocId] = id;
                return id;
            }
            return it->second;
        }

        void clean(){
            lastMemLocId = 0;
            aliasMemLoc.clear();
            pointerMemLoc.clear();
            derefMemLoc.clear();
        }
    };

    MemLocRegister _register;

    class AliasGroup{
    public:
        MemLocId id;
        std::set<const llvm::Value*> values;
        std::set<RelativeMemLoc> memLocs;
        std::set<MemLocId> derefValues;

        bool hasValue(const llvm::Value* val) const{
            return values.find(val) != values.end();
        }

        bool hasDerefValue(MemLocId groupId) const{
            return derefValues.find(groupId) != derefValues.end();
        }

        bool hasMemLoc(RelativeMemLoc aml) const{
            return memLocs.find(aml) != memLocs.end();
        }

        bool hasMemLocWithBase(MemLocId baseId) const{
            for(auto& ml : memLocs){
                if(ml.baseId == baseId) return true;
            }
            return false;
        }

        bool isEmpty() const{
            return values.size() == 0 && memLocs.size() == 0 && derefValues.size() == 0;
        }
    };

    using AliasGroupPtr = std::shared_ptr<AliasGroup>;

    class AliasGroups: public std::map<MemLocId, AliasGroupPtr> {
    public:

        AliasGroups():std::map<MemLocId, AliasGroupPtr>(), _lastGroupId(0), _flowId(0){
        }

        void setFlowId(int64_t flowId){
            _flowId = flowId;
        }

        int64_t getFlowId(){
            return _flowId;
        }

        void addGroup(AliasGroupPtr group){
            (*this)[group->id] = group;
        }

        void removeGroup(MemLocId groupId){
            auto groupIt = this->find(groupId);
            if(groupIt != this->end()){
                this->erase(groupIt);
            }

            // Delete usages of this memory locations from another memory locations
            std::set<MemLocId> modGroups;
            for(auto agIt : *this){
                if(agIt.second->hasMemLocWithBase(groupId) || agIt.second->hasDerefValue(groupId)) modGroups.insert(agIt.first);
            }

            for(auto agId : modGroups){
                auto& sourceGroup = (*this)[agId];
                auto newGroup = std::make_shared<AliasGroup>();
                for(auto val : sourceGroup->values) newGroup->values.insert(val);
                for(auto aml : sourceGroup->memLocs){
                    if(aml.baseId != groupId) newGroup->memLocs.insert(aml);
                }
                for(auto derefVal : sourceGroup->derefValues){
                    if(derefVal != groupId) newGroup->derefValues.insert(derefVal);
                }
                this->replaceGroup(agId, newGroup);
            }
        }

        void replaceGroup(MemLocId groupId, AliasGroupPtr ag){
            ag->id = groupId;
            (*this)[ag->id] = ag;
        }

        AliasGroupPtr getGroup(MemLocId groupId) const{
            auto groupIt = this->find(groupId);
            if(groupIt != this->end()){
                return groupIt->second;
            }
            return AliasGroupPtr();
        }

        AliasGroupPtr getGroupByValue(const llvm::Value* val) const{
            AliasGroupPtr agRes;
            for(auto agIt : *this){
                if(agIt.second->hasValue(val)){
                    agRes = agIt.second;
                    break;
                }
            }
            return agRes;
        }

        AliasGroupPtr getGroupByMemLoc(const MemLocation& memLoc) const{
            AliasGroupPtr agRes;
            auto baseGroup = getGroupByValue(memLoc.base);
            if(baseGroup.get() == nullptr) return agRes;
            for(auto agIt : *this){
                bool hasMemLoc = false;
                for(auto ml : agIt.second->memLocs){
                    if(ml.baseId != baseGroup->id) continue;
                    if(ml.shift != memLoc.shift) continue;
                    if(ml.size != memLoc.size) continue;
                    hasMemLoc = true;
                    break;
                }
                if(hasMemLoc){
                    agRes = agIt.second;
                    break;
                }
            }
            return agRes;
        }

        AliasGroupPtr getGroupByDerefGroup(MemLocId groupId) const{
            for(auto ag : *this){
                if(ag.second->hasDerefValue(groupId)){
                    return ag.second;
                }
            }
            return AliasGroupPtr();
        }

        bool operator==(const AliasGroups& ags) const{
            return this == &ags || isEqual(*this, ags);
        }


    private:
        MemLocId _lastGroupId;
        int64_t _flowId;

        static bool isEqual(const AliasGroups& ags1, const RelativeMemLoc& ml1, const AliasGroups& ags2, const RelativeMemLoc& ml2){
            if(ml1.size != ml2.size) return false;

            if(ml1.shift != ml2.shift) return false;

            if(!isEqual(ags1, ags1.getGroup(ml1.baseId), ags2, ags2.getGroup(ml2.baseId))) return false;

            return true;
        }

        static bool isEqual(const AliasGroups& ags1, AliasGroupPtr ag1, const AliasGroups& ags2, AliasGroupPtr ag2){
            if(!(ag1->values == ag2->values)) return false;

            for(auto& ml1 : ag1->memLocs){
                bool hasEqual = false;
                for(auto& ml2 : ag2->memLocs){
                    if(isEqual(ags1, ml1, ags2, ml2)){
                        hasEqual = true;
                        break;
                    }
                }
                if(!hasEqual) return false;
            }

            for(auto agId1 : ag1->derefValues){
                bool hasEqual = false;
                for(auto agId2 : ag2->derefValues){
                    if(isEqual(ags1, ags1.getGroup(agId1), ags2, ags2.getGroup(agId2))){
                        hasEqual = true;
                        break;
                    }
                }
                if(!hasEqual) return false;
            }

            return true;
        }

        static bool isEqual(const AliasGroups& ags1, const AliasGroups& ags2){
            for(auto ag1 : ags1){
                bool hasEqual = false;
                for(auto ag2 : ags2){
                    if(ag1.second == ag2.second || isEqual(ags1, ag1.second, ags2, ag2.second)){
                        hasEqual = true;
                        break;
                    }
                }
                if(!hasEqual) return false;
            }
            return true;
        }
    };

    using AliasGroupsPtr = std::shared_ptr<AliasGroups>;
    using AliasGroupsConstPtr = std::shared_ptr<const AliasGroups>;

    AliasGroupsPtr handleValue(const llvm::Value* val, AliasGroupsPtr ags);

    AliasGroupsPtr handleInst(const llvm::Instruction* inst, AliasGroupsPtr ags);

    AliasGroupsPtr handleArgument(const llvm::Argument* arg, AliasGroupsPtr ags);

    AliasGroupsPtr handleGlobalVariable(const llvm::GlobalVariable* gv, AliasGroupsPtr ags);

    AliasGroupsPtr handleAllocaInst(const llvm::AllocaInst* allocaInst, AliasGroupsPtr ags);

    AliasGroupsPtr handleBitcastInst(const llvm::BitCastInst* bitcastInst, AliasGroupsPtr ags);

    AliasGroupsPtr handleCallInstr(const llvm::CallInst* callInst, AliasGroupsPtr ags);

    AliasGroupsPtr handleGepInst(const llvm::GetElementPtrInst* gepInst, AliasGroupsPtr ags);

    AliasGroupsPtr handleLoadInst(const llvm::LoadInst* loadInst, AliasGroupsPtr ags);

    AliasGroupsPtr handleStoreInst(const llvm::StoreInst* storeInst, AliasGroupsPtr ags);

    bool isMemLocationBelongToAliasGroup(const ComplexMemLocation& cml, AliasGroupsConstPtr ags, AliasGroupPtr ag) const;

    AliasGroupPtr getAliasGroupByComplexMemLocation(const ComplexMemLocation& cml, AliasGroupsConstPtr ags) const;

    bool alias(ComplexMemLocation ml1, ComplexMemLocation ml2, AliasGroupsConstPtr ags);

    bool alias(const llvm::Value* val, ComplexMemLocation cml, AliasGroupsConstPtr ags);

    bool alias(ComplexMemLocation cml, const llvm::Value* val, AliasGroupsConstPtr ags);

    bool alias(const llvm::Value* val1, const llvm::Value* val2, AliasGroupsConstPtr ags);

    std::map<const llvm::Instruction*, std::vector<AliasGroupsPtr>> _aliasData;
    std::map<int64_t, int64_t> _flowMerge;
    int64_t _lastFlowId;

    void calculateBasicBlock(const llvm::BasicBlock* curBb, AliasGroupsPtr ags);

    void clean();

public:

    FlowSensitiveAA():_lastFlowId(0){}

    void calculate(const llvm::Function* func);

    bool alias(AttachedComplexMemLocation acml, const llvm::Value* val);

    bool alias(const llvm::Value* val, AttachedComplexMemLocation acml);

    bool alias(const llvm::Value* val1, const llvm::Value* val2);

    bool alias(AttachedComplexMemLocation acml1, AttachedComplexMemLocation acml2);
};

class FlowSensitiveAAWrap : public llvm::AnalysisInfoMixin<FlowSensitiveAAWrap> {
  friend AnalysisInfoMixin<FlowSensitiveAAWrap>;

  static llvm::AnalysisKey Key;

public:
  using Result = FlowSensitiveAA;

  Result run(llvm::Function &F, llvm::FunctionAnalysisManager &AM){
      FlowSensitiveAA fsaa;
      fsaa.calculate(&F);
      return fsaa;
  }
};

std::ostream& operator<<(std::ostream& os, const FlowSensitiveAA::RelativeMemLoc& obj);

std::ostream& operator<<(std::ostream& os, const FlowSensitiveAA::AliasGroup& obj);

std::ostream& operator<<(std::ostream& os, const FlowSensitiveAA::AliasGroups& obj);

#endif // FLOWSENSITIVEAA_H
