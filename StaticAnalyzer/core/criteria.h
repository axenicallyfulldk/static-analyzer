#ifndef CRITERIA_H
#define CRITERIA_H

#include <llvm/IR/Instructions.h>
#include <llvm/IR/BasicBlock.h>

#include <set>

#include "memloctypes.h"


typedef bool (*CriteriaType)(const llvm::Instruction& inst);

template<typename InstCriteria>
struct SimpleInstCounterCriteria{
    InstCriteria criteria;

    SimpleInstCounterCriteria(InstCriteria instCriteria):criteria(instCriteria){}

    bool operator()(const llvm::Function* func, std::set<const llvm::Value*>& sigVals,
                    std::set<AttachedComplexMemLocation>& sigMemLocs, std::set<const llvm::BasicBlock*>& sigBbs){
        for(auto &bb : *func){
            for(auto& inst : bb){
                if(criteria(inst)){
                    sigBbs.insert(&bb);
                }
            }
        }
    }
};

bool isFloatBinaryInst(const llvm::Instruction& inst);

bool isFloat4BinaryInst(const llvm::Instruction& inst);

bool isFloat8BinaryInst(const llvm::Instruction& inst);

bool isStoreInst(const llvm::Instruction& inst);

bool isLoadInst(const llvm::Instruction& inst);

bool isCallInst(const llvm::Instruction& inst);


enum class MathFuncType{NONE, SIN4, SIN8, COS4, COS8, TAN4, TAN8, SQRT4, SQRT8,
                    LOG4, LOG8, LOG10_4, LOG10_8 };

extern const CriteriaType mathCriterias[12];

bool isSin4(const llvm::Instruction& inst);
bool isSin8(const llvm::Instruction& inst);

bool isCos4(const llvm::Instruction& inst);
bool isCos8(const llvm::Instruction& inst);

bool isTan4(const llvm::Instruction& inst);
bool isTan8(const llvm::Instruction& inst);

bool isSqrt4(const llvm::Instruction& inst);
bool isSqrt8(const llvm::Instruction& inst);

bool isLog4(const llvm::Instruction& inst);
bool isLog8(const llvm::Instruction& inst);

bool isLog10_4(const llvm::Instruction& inst);
bool isLog10_8(const llvm::Instruction& inst);

bool isMathCriteria(CriteriaType criteria);

bool isMathFunc(const llvm::Instruction& inst);


MathFuncType getMathFuncType(CriteriaType criteria);
CriteriaType getMathFuncCriteria(MathFuncType mft);

std::string getMathFuncName(MathFuncType mft);


enum class MpiFuncType{NONE, SEND, RECV, SENDRECV, SENDRECVREPLACE, BARRIER, ISEND, IRECV };

extern const CriteriaType mpiCriterias[7];

bool isMpiSend(const llvm::Instruction& inst);

bool isMpiRecv(const llvm::Instruction& inst);

bool isMpiSendRecv(const llvm::Instruction& inst);

bool isMpiSendRecvReplace(const llvm::Instruction& inst);

bool isMpiBarrier(const llvm::Instruction& inst);

bool isMpiIsend(const llvm::Instruction& inst);

bool isMpiIrecv(const llvm::Instruction& inst);

bool isMpiCriteria(CriteriaType criteria);

bool isMpiFunc(const llvm::Instruction& inst);

bool isMpiFuncSendData(const llvm::Instruction& inst);

bool isMpiFuncRecvData(const llvm::Instruction& inst);

MpiFuncType getMpiFuncType(CriteriaType criteria);
CriteriaType getMpiFuncCriteria(MpiFuncType mft);

std::string getMpiFuncName(MpiFuncType mft);

#endif // CRITERIA_H
