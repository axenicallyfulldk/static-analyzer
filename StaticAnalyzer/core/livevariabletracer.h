#ifndef LIVEVARIABLETRACER_H
#define LIVEVARIABLETRACER_H

#include <llvm/IR/Instructions.h>
#include <llvm/IR/CFG.h>
#include <llvm/IR/Dominators.h>
#include <vector>
#include <map>
#include <set>


#include "cfg_utils.h"
#include "memloctypes.h"
#include "utils.h"


class LiveVariableTracer
{
public:
    LiveVariableTracer();
    virtual ~LiveVariableTracer();

protected:

    // Traces value since given bb
    virtual void traceValue(const llvm::Value* val, const llvm::BasicBlock* curBb,
                            std::map<const llvm::BasicBlock*, std::set<const llvm::Value*>>& bbDepedentVals,
                            std::map<const llvm::BasicBlock*, std::set<ComplexMemLocation>>& bbDependentLocs);

    // Trace return value of call instruction
    virtual void traceCallInst(const llvm::CallInst* callInst, const llvm::BasicBlock* curBb,
                               std::map<const llvm::BasicBlock*, std::set<const llvm::Value*>>& bbDepedentVals,
                               std::map<const llvm::BasicBlock*, std::set<ComplexMemLocation>>& bbDependentLocs);

    // Traces memory location before and including instruction
    virtual void traceMemLocationInclBeforeInstr(AttachedComplexMemLocation ml, const llvm::Instruction* instr,
                                         std::map<const llvm::BasicBlock *, std::set<const llvm::Value *> > &bbDepedentVals,
                                         std::map<const llvm::BasicBlock*, std::set<ComplexMemLocation>>& bbDependentLocs);

    // Traces memory location before instruction
    virtual void traceMemLocationBeforeInstr(AttachedComplexMemLocation ml, const llvm::Instruction* instr,
                                     std::map<const llvm::BasicBlock*, std::set<const llvm::Value*>>& bbDepedentVals,
                                     std::map<const llvm::BasicBlock*, std::set<ComplexMemLocation>>& bbDependentLocs);


    // Finds values on which it depeneds that control flow comes from common dominator
    // to specific phi's bb predecessor
    virtual void findPhiControlDecisionVals(const llvm::PHINode *phi, std::set<llvm::Value*>& controlDecisionVals);

    // Returns dominator tree
    virtual llvm::DominatorTree* getDominatorTree(const llvm::Function* f) = 0;
};

#endif // LIVEVARIABLETRACER_H
