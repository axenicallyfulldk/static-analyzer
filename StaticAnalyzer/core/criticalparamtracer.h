#ifndef CRITICALPARAMTRACER_H
#define CRITICALPARAMTRACER_H

#include "livevariabletracer.h"

#include <llvm/IR/Function.h>
#include <set>
#include <map>

// Class that finds critical parameters of function. Critical parameters are
// used to calculate value that function return.
class CriticalParamTracer : public LiveVariableTracer
{
public:
    CriticalParamTracer();

    void getFuncCritParams(const llvm::Function* func, std::set<llvm::Value*>& vals, std::set<ComplexMemLocation>& memLocs);

    void getFuncCritParams(const llvm::Function* func, std::set<const llvm::Value*>& vals, std::set<ComplexMemLocation>& memLocs);

protected:

    struct FuncInfo{
        std::set<const llvm::Value*> critVals; // Critical values of function
        std::set<ComplexMemLocation> critMemLocs; // Critical memory locations of function
        std::list<const llvm::CallInst*> critCalls; // Calls
        std::map<const llvm::BasicBlock*, std::set<const llvm::Value*>> depVals;
        std::map<const llvm::BasicBlock*, std::set<ComplexMemLocation>> depMemLocs;
        llvm::DominatorTree dt;
        FuncInfo(const llvm::Function* func):dt(const_cast<llvm::Function&>(*func)){}
    };

    std::map<const llvm::Function*, FuncInfo> funcInfo;
    std::map<const llvm::CallInst*, unsigned> callsCache;

    // Calculates function's critical parameters
    void calcCritParams(const llvm::Function* func);

    // Find values that decide to which bb control flow will come first.
    void findControlDecisionVals(const std::set<llvm::BasicBlock*>& bbs,
                                 std::set<llvm::Value*>& controlDecisionVals);

    // Fetch data about critical parameters at entry block and add it to
    // function's critical parameters.
    void updateFuncCritParams(const llvm::Function* func);

    // Calculate critical parameters without taking into accout calls' critical
    // parameters.
    void calculatePrioriCritParams(const llvm::Function* func);

    // Add call to significant calls
    virtual void traceCallInst(llvm::CallInst *callInst, llvm::BasicBlock *curBb,
                               std::map<const llvm::BasicBlock *, std::set<llvm::Value *> > &bbDepedentVals,
                               std::map<const llvm::BasicBlock *, std::set<ComplexMemLocation> > &bbDependentLocs);

    // Trace call's critical parameters to caller critical parameters.
    void traceCallCritValues(const llvm::CallInst* callInst);

    // Returns dominator tree for given function
    virtual llvm::DominatorTree* getDominatorTree(const llvm::Function* f);

    // Returns count of critical parameters of function.
    static unsigned getFuncGist(const FuncInfo& fi);
};

#endif // CRITICALPARAMTRACER_H
