#ifndef CFG_UTILS_H
#define CFG_UTILS_H

#include "cfg.h"
#include <set>
#include <stack>
#include <boost/bimap.hpp>

#include "easylogging++.h"

#include <llvm/IR/BasicBlock.h>
#include <llvm/IR/Instructions.h>
#include <llvm/IR/CFG.h>
#include <llvm/Support/raw_ostream.h>

#include "syntax_tree.h"

template<class BlockType>
using CfgMapping = boost::bimap<const BlockType*, const BlockType*>;

/// Returns control flow subgraph with entry block setted to "from" block. Subgraph
/// include all blocks and edges reachable from "from" block. Subgraph doesn't include
/// blocks and edges reachable only from "stop" blocks.
template<class BlockType>
typename BlockType::CfgType getReachableFromSubgraph(const BlockType& from, std::set<const BlockType*> stops, CfgMapping<BlockType>* mapping = nullptr){
    using CfgType = typename BlockType::CfgType;
    using BlockIterator = typename CfgType::template BlockIterator<true>;
    using BlockRange = typename CfgType::template BlockRange<true>;
    using MapVal = typename CfgMapping<BlockType>::value_type;

    // Create mapping if it is not passed to the function
    std::unique_ptr<CfgMapping<BlockType>> mapManager;
    if(mapping == nullptr){
        mapping = new CfgMapping<BlockType>();
        mapManager.reset(mapping);
    }

//    auto& origGraph = from.getGraph();
    CfgType resGraph;

    std::set<const BlockType*> visitedBlocks;

    // Add "from" block to the subgraph and set mapping for it.
    auto& fromRes = resGraph.addBlock(from.getData());
    mapping->insert(MapVal(&from, &fromRes));

    auto blockRangeToPair = [](BlockRange br){return std::pair<BlockIterator, BlockIterator>(br.begin(), br.end());};

    // Define stack that is necessary to rid of recursion dfs.
    std::stack<std::pair<BlockIterator, BlockIterator>> succStack;
    if(stops.find(&from) == stops.end())
        succStack.push(blockRangeToPair(from.successors()));

    // Add "from" block to visited blocks.
    visitedBlocks.insert(&from);

    while(succStack.size()){
        auto succLevel = succStack.top();
        succStack.pop();

        if(succLevel.first == succLevel.second){
            if(succStack.size()) ++succStack.top().first;
            continue;
        }

        auto& pred = succStack.size() ? *mapping->left.at(&*succStack.top().first) : fromRes;

        auto curBb = &*succLevel.first;

        if(visitedBlocks.find(curBb) == visitedBlocks.end()){
            // If block is not visited yet, add block to the graph. And add its
            // successors to the stack
            auto& succRes = resGraph.addBlock(curBb->getData());
            mapping->insert(MapVal(curBb, &succRes));
            resGraph.addEdge(pred, succRes);
            if(stops.find(curBb) == stops.end()){
                succStack.push(succLevel);
                succStack.push(blockRangeToPair(curBb->successors()));
            }else{
                ++succLevel.first;
                succStack.push(succLevel);
            }
            visitedBlocks.insert(curBb);
            continue;
        }else{
            // If block is visited we have to add only edge
            resGraph.addEdge(pred, *mapping->left.at(curBb));
            ++succLevel.first;
            succStack.push(succLevel);
            continue;
        }
    }
    return resGraph;
}

/// Returns control flow subgraph which contains all blocks and edge from which
/// we can reach "to" block.
template<class BlockType>
typename BlockType::CfgType getReachesToSubgraph(const BlockType& to, CfgMapping<BlockType>* mapping = nullptr){
    using CfgType = typename BlockType::CfgType;
    using BlockIterator = typename CfgType::template BlockIterator<true>;
    using BlockRange = typename CfgType::template BlockRange<true>;
    using MapVal = typename CfgMapping<BlockType>::value_type;

    std::unique_ptr<CfgMapping<BlockType>> mapManager;
    if(mapping == nullptr){
        mapping = new CfgMapping<BlockType>();
        mapManager.reset(mapping);
    }

    auto& origGraph = to.getGraph();
    CfgType resGraph;
    auto& toRes = resGraph.addBlock(to.getData());
    mapping->insert(MapVal(&to, &toRes));

    std::set<const BlockType*> visitedBlocks;
    visitedBlocks.insert(&to);

    auto blockRangeToPair = [](BlockRange br){return std::pair<BlockIterator, BlockIterator>(br.begin(), br.end());};

    std::stack<std::pair<BlockIterator, BlockIterator>> predStack;
    predStack.push(blockRangeToPair(to.predecessors()));

    while(predStack.size()){
        auto predLevel = predStack.top();
        predStack.pop();

        if(predLevel.first == predLevel.second){
            if(predStack.size()) ++predStack.top().first;
            continue;
        }

        auto& succ = predStack.size() ? *mapping->left.at(&*predStack.top().first) : toRes;

        if(visitedBlocks.find(&*predLevel.first) == visitedBlocks.end()){
            auto& predRes = resGraph.addBlock((*predLevel.first).getData());
            mapping->insert(MapVal(&*predLevel.first, &predRes));
            resGraph.addEdge(predRes, succ);
            predStack.push(predLevel);
            predStack.push(blockRangeToPair(predLevel.first->predecessors()));
            visitedBlocks.insert(&*predLevel.first);
            continue;
        } else {
            resGraph.addEdge(*mapping->left.at(&*predLevel.first), succ);
            ++predLevel.first;
            predStack.push(predLevel);
            continue;
        }
    }

    if(mapping->left.count(&origGraph.getEntryBlock())){
        resGraph.setEntryBlock(*mapping->left.at(&origGraph.getEntryBlock()));
    }

    return resGraph;
}

/// Convert llvm function to Cfg<BasicBlock*> or Cfg<const BasicBlock*>
template<bool isConstant>
struct FunctionToCfgConverter{
    using BlockPtr = std::conditional_t<isConstant, const llvm::BasicBlock*, llvm::BasicBlock*>;
    using FunctionPtr = std::conditional_t<isConstant, const llvm::Function*, llvm::Function*>;
    using Mapping = std::map<BlockPtr, typename Cfg<BlockPtr>::Block*>;
    using CfgType = Cfg<BlockPtr>;

    Cfg<BlockPtr> convert(FunctionPtr func, Mapping* mapping){
        Cfg<BlockPtr> cfg;

        std::unique_ptr<Mapping> mappingManager;
        if(mapping == nullptr){
            mapping = new Mapping();
            mappingManager.reset(mapping);
        }

        BlockPtr entryOrig = &func->getEntryBlock();

        addBlock(entryOrig, cfg, *mapping);

        return cfg;
    }

private:
    void addBlock(BlockPtr blockOrig, Cfg<BlockPtr>& cfg, std::map<BlockPtr, typename Cfg<BlockPtr>::Block*>& mapping){
        if(mapping.find(blockOrig) == mapping.end()){
            auto block = &cfg.addBlock(blockOrig);
            mapping[blockOrig] = block;
            for(auto succ : llvm::successors(blockOrig)){
                addBlock(succ, cfg, mapping);
                cfg.addEdge(*block, *mapping[succ]);
            }
        }
    }
};

/// Find nodes that have successors
template<class CfgType>
void findExitingNodes(const CfgType& subgraph,
                      CfgMapping<typename CfgType::Block>& g2sg,
                      std::set<const typename CfgType::Block*>& exitingBlocks){
    for(auto& bb : subgraph.blocks()){
        auto gbb = g2sg.right.at(&bb); // The same node in parental graph
        if(bb.succCount() != gbb->succCount()){
            exitingBlocks.insert(&bb);
        }
    }
}

template<class BlockType>
void findExitNodes(const BlockType* node, CfgMapping<BlockType>& g2sg,
                   std::set<const BlockType*>& exitBlocks){
    for(auto& succBb : node->successors()){
        if(g2sg.left.count(&succBb) == 0){
            exitBlocks.insert(&succBb);
        }
    }
}

template <class BlockType>
void findPathDecisionValues(const BlockType* from, const BlockType* to, std::set<llvm::Value*>& cps){

    // Algorithm:
    // 1. Build control flow SUBgraph (rfcfg), that include all edges and nodes
    // reachable from "from" node. Entry node in this CFG is the "from".
    // 2. Based on built subgraph, build another subgraph (rtcfg) that include
    // nodes and edges from which we can reach "to" node.
    // 3. Find all "exiting" nodes. Exiting nodes that exit in rtcfg and in
    // rfcfg have more successors.
    // 4. Conditions of "exiting" nodes are decision values.

    using CfgType = typename BlockType::CfgType;

    CfgMapping<BlockType> orig2rf;
    CfgType rfcfg = getReachableFromSubgraph(*from, {to}, &orig2rf);

    //There is no path from "from" to "to"
    if(orig2rf.left.count(to) == 0){
        return;
    }

    CfgMapping<BlockType> rf2rt;
    CfgType rtcfg = getReachesToSubgraph(*orig2rf.left.at(to), &rf2rt);

    auto fromRes = rf2rt.left.at(orig2rf.left.at(from));

    std::set<const BlockType*> exitingBlocks;
    findExitingNodes(rtcfg, rf2rt, exitingBlocks);

    auto getTermCondition = [](const llvm::TerminatorInst* ti) -> llvm::Value*{
        if(auto bi = llvm::dyn_cast<llvm::BranchInst>(ti)){
            if(bi->isConditional()){
                return bi->getCondition();
            }
            return nullptr;
        }
        if(auto ri = llvm::dyn_cast<llvm::ReturnInst>(ti)){
            return nullptr;
        }
        std::string tiStr; llvm::raw_string_ostream rso(tiStr); ti->print(rso);
        LOG(WARNING)<<"Can't get condition from instruction "<<tiStr;
        return nullptr;
    };

    for(auto en : exitingBlocks){
        // Foresee more complex exiting conditions
        auto cond = getTermCondition(en->getData()->getTerminator());
        if(cond != nullptr){
            cps.insert(cond);
        }
        findPathDecisionValues(fromRes, en, cps);
    }
}

template<>
void findPathDecisionValues<llvm::BasicBlock>(const llvm::BasicBlock* from, const llvm::BasicBlock* to, std::set<llvm::Value*>& cps);


#endif // CFG_UTILS_H
