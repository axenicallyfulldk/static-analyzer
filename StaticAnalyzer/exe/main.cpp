#include <iostream>
#include <memory>
#include <sstream>

#include <llvm/IR/LLVMContext.h> //LLVMContext
#include <llvm/IR/Module.h> //Module
#include <llvm/Support/SourceMgr.h> //SMDiagnostic
#include <llvm/IRReader/IRReader.h> //parseIRFile
#include <llvm/IR/PassManager.h> //AnalysisInfoMixin
#include <llvm/Passes/PassBuilder.h> //PassBuilder

#include "funcstatisticanalyzer.h"

#include "criticalparamanalyzer.h"

#include "easylogging++.h"

#define ELPP_DISABLE_LOGS
INITIALIZE_EASYLOGGINGPP


using namespace std;
using namespace llvm;

static LLVMContext context;

std::string getParamString2(std::vector<std::shared_ptr<AST::ValueNode>>& params){
    std::stringstream ss;

    for(auto p:params){
        ss << p->to_string()+", ";
    }

    auto str = ss.str();
    str = str.substr(0, str.size()-2) + (params.size() ? ", **kwargs" : "**kwargs");
    return str;
}

void printPythonModule(std::shared_ptr<Module> module){
    for(Function &func: *module){
        if(func.isDeclaration()) continue;
        std::cout<<"class "<<func.getName().data()<<":"<<std::endl;
        FuncStatisticAnalyzer fsa(&func);
        auto expr = fsa.getFlopExpr();
        auto params = FuncStatisticAnalyzer::extractSortedCriticalParams(expr);
        std::cout<<"\n\t@staticmethod\n\tdef flops("<<getParamString2(params)<<"):\n\t\treturn "<<expr->to_string()<<std::endl;
        expr = fsa.getFlop4Expr();
        params = FuncStatisticAnalyzer::extractSortedCriticalParams(expr);
        std::cout<<"\n\t@staticmethod\n\tdef flops4("<<getParamString2(params)<<"):\n\t\treturn "<<expr->to_string()<<std::endl;
        expr = fsa.getFlop8Expr();
        params = FuncStatisticAnalyzer::extractSortedCriticalParams(expr);
        std::cout<<"\n\t@staticmethod\n\tdef flops8("<<getParamString2(params)<<"):\n\t\treturn "<<expr->to_string()<<std::endl;
        expr = fsa.getReadAccessExpr();
        params = FuncStatisticAnalyzer::extractSortedCriticalParams(expr);
        std::cout<<"\n\t@staticmethod\n\tdef read_accesses("<<getParamString2(params)<<"):\n\t\treturn "<<expr->to_string()<<std::endl;
        expr = fsa.getWriteAccessExpr();
        params = FuncStatisticAnalyzer::extractSortedCriticalParams(expr);
        std::cout<<"\n\t@staticmethod\n\tdef write_accesses("<<getParamString2(params)<<"):\n\t\treturn "<<expr->to_string()<<std::endl;

        expr = fsa.getLoadSizeExpr();
        params = FuncStatisticAnalyzer::extractSortedCriticalParams(expr);
        std::cout<<"\n\t@staticmethod\n\tdef loads("<<getParamString2(params)<<"):\n\t\treturn "<<expr->to_string()<<std::endl;
        expr = fsa.getStoreSizeExpr();
        params = FuncStatisticAnalyzer::extractSortedCriticalParams(expr);
        std::cout<<"\n\t@staticmethod\n\tdef stores("<<getParamString2(params)<<"):\n\t\treturn "<<expr->to_string()<<std::endl;
        for(auto i:fsa.getUsedMathFuncs()){
            expr = fsa.getMathFuncExpr(i);
            params = FuncStatisticAnalyzer::extractSortedCriticalParams(expr);
            std::cout<<"\n\t@staticmethod\n\tdef ";
            std::cout<<getMathFuncName(i)<<"(";
            std::cout<<getParamString2(params)<<"):\n\t\t";
            std::cout<<"return "<<expr->to_string()<<std::endl;
        }

        for(auto i:fsa.getUsedMpiFuncs()){
            expr = fsa.getMpiFuncExpr(i);
            params = FuncStatisticAnalyzer::extractSortedCriticalParams(expr);
            std::cout<<"\n\t@staticmethod\n\tdef ";
            std::cout<<getMpiFuncName(i)<<"(";
            std::cout<<getParamString2(params)<<"):\n\t\t";
            std::cout<<"return "<<expr->to_string()<<std::endl;
        }
        if(fsa.doesSendData()){
            expr = fsa.getSendedByteExpr();
            params = FuncStatisticAnalyzer::extractSortedCriticalParams(expr);
            std::cout<<"\n\t@staticmethod\n\tdef ";
            std::cout<<"sendBytes"<<"(";
            std::cout<<getParamString2(params)<<"):\n\t\t";
            std::cout<<"return "<<expr->to_string()<<std::endl;
        }
        if(fsa.doesRecvData()){
            expr = fsa.getRecievedByteExpr();
            params = FuncStatisticAnalyzer::extractSortedCriticalParams(expr);
            std::cout<<"\n\t@staticmethod\n\tdef ";
            std::cout<<"recvBytes"<<"(";
            std::cout<<getParamString2(params)<<"):\n\t\t";
            std::cout<<"return "<<expr->to_string()<<std::endl;
        }
        std::cout<< std::endl << std::endl;
    }
}

void printEquations(std::shared_ptr<Module> module){
    for(Function &func: *module){
        if(func.isDeclaration()) continue;
        std::cout<<func.getName().data()<<std::endl;
        FuncStatisticAnalyzer fsa(&func);
        std::cout<<"flops: "<<fsa.getFlopExpr()->to_string()<<std::endl;
        std::cout<<"flops4: "<<fsa.getFlop4Expr()->to_string()<<std::endl;
        std::cout<<"flops8: "<<fsa.getFlop8Expr()->to_string()<<std::endl;
        std::cout<<"loads: "<<fsa.getReadAccessExpr()->to_string()<<std::endl;
        std::cout<<"stores: "<<fsa.getWriteAccessExpr()->to_string()<<std::endl<<std::endl;
    }
}


int main(int argc, char** argv)
{

    el::Configurations defaultConf;
    defaultConf.setToDefault();
    // Values are always std::string
    defaultConf.set(el::Level::Debug,
                    el::ConfigurationType::Enabled, "false");
    defaultConf.set(el::Level::Warning,
                    el::ConfigurationType::Enabled, "false");
    // default logger uses default configurations
    el::Loggers::reconfigureLogger("default", defaultConf);

    SMDiagnostic err;
    std::shared_ptr<Module> module=parseIRFile(argv[1], err, context);

    printPythonModule(module);

    return 0;
}
