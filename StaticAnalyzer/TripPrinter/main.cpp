#include <iostream>
#include <memory>
#include <sstream>

#include <llvm/IR/LLVMContext.h> //LLVMContext
#include <llvm/IR/Module.h> //Module
#include <llvm/Support/SourceMgr.h> //SMDiagnostic
#include <llvm/IRReader/IRReader.h> //parseIRFile
#include <llvm/IR/PassManager.h> //AnalysisInfoMixin
#include <llvm/Passes/PassBuilder.h> //PassBuilder

#include "blocktripevaluater.h"

#include "easylogging++.h"
INITIALIZE_EASYLOGGINGPP

using namespace std;
using namespace llvm;

static LLVMContext context;

static std::string getSimpleNodeLabel(const llvm::BasicBlock *Node,
                                      const llvm::Function *) {
    if (!Node->getName().empty())
        return Node->getName().str();

    std::string Str;
    llvm::raw_string_ostream OS(Str);

    Node->printAsOperand(OS, false);
    return OS.str();
}

void printTripEquations(std::shared_ptr<Module> module){
    for(auto& func: *module){
        if(func.isDeclaration()) continue;
        std::cout<<"Function: "<<func.getName().data()<<std::endl;
        BlockTripEvaluater bte(&func);
        BlockTripClassifier btc(&func);
        for(auto& bb: func){
            std::cout<<"Basic block: "<<getSimpleNodeLabel(&bb, &func)<<"; ";
            auto groupId = btc.getBasicBlockGroup(&bb);
            auto head = btc.getGroupHead(groupId);
            auto top = btc.getGroupTop(groupId);
            std::cout<<"Top block: "<<getSimpleNodeLabel(top, &func)<<"; ";
            if (head)
                std::cout<<"Head block: "<<getSimpleNodeLabel(head, &func)<<std::endl;
            else
                std::cout<<"Head block: null"<<std::endl;
            std::cout<<bte.getBasicBlockTripCount(&bb)->to_string()<<std::endl;
        }
        std::cout<<std::endl;
    }
}


int main(int argc, char** argv)
{
    if(argc == 1) return 0;

    SMDiagnostic err;
    std::shared_ptr<Module> module=parseIRFile(argv[1], err, context);

    printTripEquations(module);

    return 0;
}
